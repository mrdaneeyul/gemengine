﻿using System;

namespace GemEngine
{
    /// <summary>
    /// The main class.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            using (var game = new Engine(320, 180, 640, 320, "GemEngine", false))
                game.Run();
        }
    }
}
