﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.IO;

namespace GemEngine.Graphics
{
    public class Texture
    {
        public Texture2D Texture2D { get; private set; }
        public Rectangle ClipRectangle { get; private set; }
        public string AtlasPath { get; private set; }
        public Vector2 DrawOffset { get; private set; }
        public int Width { get; private set; }
        public int Height { get; private set; }
        public Vector2 Center { get; private set; }
        public float LeftUV { get; private set; }
        public float RightUV { get; private set; }
        public float TopUV { get; private set; }
        public float BottomUV { get; private set; }

        #region Constructors

        public Texture() { }

        public Texture(Texture2D texture)
        {
            Texture2D = texture;
            AtlasPath = null;
            ClipRectangle = new Rectangle(0, 0, Texture2D.Width, Texture2D.Height);
            DrawOffset = Vector2.Zero;
            Width = ClipRectangle.Width;
            Height = ClipRectangle.Height;
            SetUVs();
        }

        public Texture(Texture parent, int x, int y, int width, int height)
        {
            Texture2D = parent.Texture2D;
            AtlasPath = null;
            ClipRectangle = parent.GetRelativeRectangle(x, y, width, height);
            DrawOffset = new Vector2(-Math.Min(x - parent.DrawOffset.X, 0), -Math.Min(y - parent.DrawOffset.Y, 0));
            Width = width;
            Height = height;
            SetUVs();
        }

        public Texture(Texture parent, Rectangle clipRectangle) : this(parent, clipRectangle.X, clipRectangle.Y, clipRectangle.Width, clipRectangle.Height) { }

        public Texture(Texture parent, string atlasPath, Rectangle clipRectangle, Vector2 drawOffset, int width, int height)
        {
            Texture2D = parent.Texture2D;
            AtlasPath = atlasPath;
            ClipRectangle = parent.GetRelativeRectangle(clipRectangle);
            DrawOffset = drawOffset;
            Width = width;
            Height = height;
            SetUVs();
        }

        public Texture(Texture parent, string atlasPath, Rectangle clipRectangle) : this(parent, clipRectangle)
        {
            AtlasPath = atlasPath;
        }

        public Texture(Texture2D texture, Vector2 drawOffset, int frameWidth, int frameHeight)
        {
            Texture2D = texture;
            ClipRectangle = new Rectangle(0, 0, texture.Width, texture.Height);
            DrawOffset = drawOffset;
            Width = frameWidth;
            Height = frameHeight;
            SetUVs();
        }

        public Texture(int width, int height, Color color)
        {
            Texture2D = new Texture2D(Engine.Instance.GraphicsDevice, width, height);
            var colors = new Color[width * height];
            for (var i = 0; i < width * height; i++)
                colors[i] = color;
            Texture2D.SetData(colors);
            ClipRectangle = new Rectangle(0, 0, width, height);
            DrawOffset = Vector2.Zero;
            Width = width;
            Height = height;
            SetUVs();
        }

        #endregion Constructors

        public void Unload()
        {
            Texture2D.Dispose();
            Texture2D = null;
        }

        public void Dispose()
        {
            Texture2D.Dispose();
        }

        public Texture GetSubtexture(int x, int y, int width, int height, Texture applyTo = null)
        {
            if (applyTo == null)
                return new Texture(this, x, y, width, height);

            applyTo.Texture2D = Texture2D;
            applyTo.AtlasPath = null;
            applyTo.ClipRectangle = GetRelativeRectangle(x, y, width, height);
            applyTo.DrawOffset = new Vector2(-Math.Min(x - DrawOffset.X, 0), -Math.Min(y - DrawOffset.Y, 0));
            applyTo.Width = width;
            applyTo.Height = height;
            applyTo.SetUVs();
            return applyTo;
        }

        public Texture GetSubTexture(Rectangle rectangle)
        {
            return new Texture(this, rectangle);
        }

        static public Texture FromFile(string filename)
        {
            var fileStream = new FileStream(filename, FileMode.Open, FileAccess.Read);
            var texture = Texture2D.FromStream(Engine.Instance.GraphicsDevice, fileStream);
            fileStream.Close();

            return new Texture(texture);
        }

        private void SetUVs()
        {
            Center = new Vector2(Width, Height) * 0.5f;
            LeftUV = ClipRectangle.Left / (float)Texture2D.Width;
            RightUV = ClipRectangle.Right / (float)Texture2D.Width;
            TopUV = ClipRectangle.Top / (float)Texture2D.Height;
            BottomUV = ClipRectangle.Bottom / (float)Texture2D.Height;
        }

        #region Helpers
        public override string ToString()
        {
            if (AtlasPath != null)
                return AtlasPath;
            else
                return $"Texture [{Texture2D.Width} x {Texture2D.Height}]";
        }

        public Rectangle GetRelativeRectangle(Rectangle rectangle)
        {
            return GetRelativeRectangle(rectangle.X, rectangle.Y, rectangle.Width, rectangle.Height);
        }

        public Rectangle GetRelativeRectangle(int x, int y, int width, int height)
        {
            var atX = (int)(ClipRectangle.X - DrawOffset.X + x);
            var atY = (int)(ClipRectangle.Y - DrawOffset.Y + y);
            var rectangleX = MathHelper.Clamp(atX, ClipRectangle.Left, ClipRectangle.Right);
            var rectangleY = MathHelper.Clamp(atY, ClipRectangle.Top, ClipRectangle.Bottom);
            var rectangleWidth = Math.Max(0, Math.Min(atX + width, ClipRectangle.Right) - rectangleX);
            var rectangleHeight = Math.Max(0, Math.Min(atY + height, ClipRectangle.Bottom) - rectangleY);

            return new Rectangle(rectangleX, rectangleY, rectangleWidth, rectangleHeight);
        }

        public int TotalPixels => Width * Height;
        #endregion Helpers

        #region Draw
        public void Draw(Vector2 position)
        {
#if DEBUG
            if (Texture2D.IsDisposed)
                throw new Exception("Texture2D is disposed");
#endif
            Graphics.Draw.SpriteBatch.Draw(Texture2D, position, ClipRectangle, Color.White, 0, -DrawOffset, 1f, SpriteEffects.None, 0);
        }

        public void Draw(Vector2 position, Vector2 origin)
        {
#if DEBUG
            if (Texture2D.IsDisposed)
                throw new Exception("Texture2D is disposed");
#endif
            Graphics.Draw.SpriteBatch.Draw(Texture2D, position, ClipRectangle, Color.White, 0, origin - DrawOffset, 1f, SpriteEffects.None, 0);
        }

        public void Draw(Vector2 position, Vector2 origin, Color color)
        {
#if DEBUG
            if (Texture2D.IsDisposed)
                throw new Exception("Texture2D is disposed");
#endif
            Graphics.Draw.SpriteBatch.Draw(Texture2D, position, ClipRectangle, color, 0, origin - DrawOffset, 1f, SpriteEffects.None, 0);
        }

        public void Draw(Vector2 position, Vector2 origin, Color color, float scale)
        {
#if DEBUG
            if (Texture2D.IsDisposed)
                throw new Exception("Texture2D is disposed");
#endif
            Graphics.Draw.SpriteBatch.Draw(Texture2D, position, ClipRectangle, color, 0, origin - DrawOffset, scale, SpriteEffects.None, 0);
        }

        public void Draw(Vector2 position, Vector2 origin, Color color, float scale, float rotation)
        {
#if DEBUG
            if (Texture2D.IsDisposed)
                throw new Exception("Texture2D Is Disposed");
#endif
            Graphics.Draw.SpriteBatch.Draw(Texture2D, position, ClipRectangle, color, rotation, origin - DrawOffset, scale, SpriteEffects.None, 0);
        }

        public void Draw(Vector2 position, Vector2 origin, Color color, float scale, float rotation, SpriteEffects flip)
        {
#if DEBUG
            if (Texture2D.IsDisposed)
                throw new Exception("Texture2D Is Disposed");
#endif
            Graphics.Draw.SpriteBatch.Draw(Texture2D, position, ClipRectangle, color, rotation, origin - DrawOffset, scale, flip, 0);
        }

        public void Draw(Vector2 position, Vector2 origin, Color color, Vector2 scale)
        {
#if DEBUG
            if (Texture2D.IsDisposed)
                throw new Exception("Texture2D Is Disposed");
#endif
            Graphics.Draw.SpriteBatch.Draw(Texture2D, position, ClipRectangle, color, 0, origin - DrawOffset, scale, SpriteEffects.None, 0);
        }

        public void Draw(Vector2 position, Vector2 origin, Color color, Vector2 scale, float rotation)
        {
#if DEBUG
            if (Texture2D.IsDisposed)
                throw new Exception("Texture2D Is Disposed");
#endif
            Graphics.Draw.SpriteBatch.Draw(Texture2D, position, ClipRectangle, color, rotation, origin - DrawOffset, scale, SpriteEffects.None, 0);
        }

        public void Draw(Vector2 position, Vector2 origin, Color color, Vector2 scale, float rotation, SpriteEffects flip)
        {
#if DEBUG
            if (Texture2D.IsDisposed)
                throw new Exception("Texture2D Is Disposed");
#endif
            Graphics.Draw.SpriteBatch.Draw(Texture2D, position, ClipRectangle, color, rotation, origin - DrawOffset, scale, flip, 0);
        }

        public void Draw(Vector2 position, Vector2 origin, Color color, Vector2 scale, float rotation, Rectangle clip)
        {
#if DEBUG
            if (Texture2D.IsDisposed)
                throw new Exception("Texture2D Is Disposed");
#endif
            Graphics.Draw.SpriteBatch.Draw(Texture2D, position, GetRelativeRectangle(clip), color, rotation, origin - DrawOffset, scale, SpriteEffects.None, 0);
        }

        #region Draw centered
        public void DrawCentered(Vector2 position)
        {
#if DEBUG
            if (Texture2D.IsDisposed)
                throw new Exception("Texture2D Is Disposed");
#endif
            Graphics.Draw.SpriteBatch.Draw(Texture2D, position, ClipRectangle, Color.White, 0, Center - DrawOffset, 1f, SpriteEffects.None, 0);
        }

        public void DrawCentered(Vector2 position, Color color)
        {
#if DEBUG
            if (Texture2D.IsDisposed)
                throw new Exception("Texture2D Is Disposed");
#endif
            Graphics.Draw.SpriteBatch.Draw(Texture2D, position, ClipRectangle, color, 0, Center - DrawOffset, 1f, SpriteEffects.None, 0);
        }

        public void DrawCentered(Vector2 position, Color color, float scale)
        {
#if DEBUG
            if (Texture2D.IsDisposed)
                throw new Exception("Texture2D Is Disposed");
#endif
            Graphics.Draw.SpriteBatch.Draw(Texture2D, position, ClipRectangle, color, 0, Center - DrawOffset, scale, SpriteEffects.None, 0);
        }

        public void DrawCentered(Vector2 position, Color color, float scale, float rotation)
        {
#if DEBUG
            if (Texture2D.IsDisposed)
                throw new Exception("Texture2D Is Disposed");
#endif
            Graphics.Draw.SpriteBatch.Draw(Texture2D, position, ClipRectangle, color, rotation, Center - DrawOffset, scale, SpriteEffects.None, 0);
        }

        public void DrawCentered(Vector2 position, Color color, float scale, float rotation, SpriteEffects flip)
        {
#if DEBUG
            if (Texture2D.IsDisposed)
                throw new Exception("Texture2D Is Disposed");
#endif
            Graphics.Draw.SpriteBatch.Draw(Texture2D, position, ClipRectangle, color, rotation, Center - DrawOffset, scale, flip, 0);
        }

        public void DrawCentered(Vector2 position, Color color, Vector2 scale)
        {
#if DEBUG
            if (Texture2D.IsDisposed)
                throw new Exception("Texture2D Is Disposed");
#endif
            Graphics.Draw.SpriteBatch.Draw(Texture2D, position, ClipRectangle, color, 0, Center - DrawOffset, scale, SpriteEffects.None, 0);
        }

        public void DrawCentered(Vector2 position, Color color, Vector2 scale, float rotation)
        {
#if DEBUG
            if (Texture2D.IsDisposed)
                throw new Exception("Texture2D Is Disposed");
#endif
            Graphics.Draw.SpriteBatch.Draw(Texture2D, position, ClipRectangle, color, rotation, Center - DrawOffset, scale, SpriteEffects.None, 0);
        }

        public void DrawCentered(Vector2 position, Color color, Vector2 scale, float rotation, SpriteEffects flip)
        {
#if DEBUG
            if (Texture2D.IsDisposed)
                throw new Exception("Texture2D Is Disposed");
#endif
            Graphics.Draw.SpriteBatch.Draw(Texture2D, position, ClipRectangle, color, rotation, Center - DrawOffset, scale, flip, 0);
        }
        #endregion Draw centered

        #region Draw justified
        public void DrawJustified(Vector2 position, Vector2 justify)
        {
#if DEBUG
            if (Texture2D.IsDisposed)
                throw new Exception("Texture2D Is Disposed");
#endif
            Graphics.Draw.SpriteBatch.Draw(Texture2D, position, ClipRectangle, Color.White, 0, new Vector2(Width * justify.X, Height * justify.Y) - DrawOffset, 1f, SpriteEffects.None, 0);
        }

        public void DrawJustified(Vector2 position, Vector2 justify, Color color)
        {
#if DEBUG
            if (Texture2D.IsDisposed)
                throw new Exception("Texture2D Is Disposed");
#endif
            Graphics.Draw.SpriteBatch.Draw(Texture2D, position, ClipRectangle, color, 0, new Vector2(Width * justify.X, Height * justify.Y) - DrawOffset, 1f, SpriteEffects.None, 0);
        }

        public void DrawJustified(Vector2 position, Vector2 justify, Color color, float scale)
        {
#if DEBUG
            if (Texture2D.IsDisposed)
                throw new Exception("Texture2D Is Disposed");
#endif
            Graphics.Draw.SpriteBatch.Draw(Texture2D, position, ClipRectangle, color, 0, new Vector2(Width * justify.X, Height * justify.Y) - DrawOffset, scale, SpriteEffects.None, 0);
        }

        public void DrawJustified(Vector2 position, Vector2 justify, Color color, float scale, float rotation)
        {
#if DEBUG
            if (Texture2D.IsDisposed)
                throw new Exception("Texture2D Is Disposed");
#endif
            Graphics.Draw.SpriteBatch.Draw(Texture2D, position, ClipRectangle, color, rotation, new Vector2(Width * justify.X, Height * justify.Y) - DrawOffset, scale, SpriteEffects.None, 0);
        }

        public void DrawJustified(Vector2 position, Vector2 justify, Color color, float scale, float rotation, SpriteEffects flip)
        {
#if DEBUG
            if (Texture2D.IsDisposed)
                throw new Exception("Texture2D Is Disposed");
#endif
            Graphics.Draw.SpriteBatch.Draw(Texture2D, position, ClipRectangle, color, rotation, new Vector2(Width * justify.X, Height * justify.Y) - DrawOffset, scale, flip, 0);
        }

        public void DrawJustified(Vector2 position, Vector2 justify, Color color, Vector2 scale)
        {
#if DEBUG
            if (Texture2D.IsDisposed)
                throw new Exception("Texture2D Is Disposed");
#endif
            Graphics.Draw.SpriteBatch.Draw(Texture2D, position, ClipRectangle, color, 0, new Vector2(Width * justify.X, Height * justify.Y) - DrawOffset, scale, SpriteEffects.None, 0);
        }

        public void DrawJustified(Vector2 position, Vector2 justify, Color color, Vector2 scale, float rotation)
        {
#if DEBUG
            if (Texture2D.IsDisposed)
                throw new Exception("Texture2D Is Disposed");
#endif
            Graphics.Draw.SpriteBatch.Draw(Texture2D, position, ClipRectangle, color, rotation, new Vector2(Width * justify.X, Height * justify.Y) - DrawOffset, scale, SpriteEffects.None, 0);
        }

        public void DrawJustified(Vector2 position, Vector2 justify, Color color, Vector2 scale, float rotation, SpriteEffects flip)
        {
#if DEBUG
            if (Texture2D.IsDisposed)
                throw new Exception("Texture2D Is Disposed");
#endif
            Graphics.Draw.SpriteBatch.Draw(Texture2D, position, ClipRectangle, color, rotation, new Vector2(Width * justify.X, Height * justify.Y) - DrawOffset, scale, flip, 0);
        }
        #endregion Draw justified

        #region Draw outline
        public void DrawOutline(Vector2 position)
        {
#if DEBUG
            if (Texture2D.IsDisposed)
                throw new Exception("Texture2D Is Disposed");
#endif

            for (var i = -1; i <= 1; i++)
                for (var j = -1; j <= 1; j++)
                    if (i != 0 || j != 0)
                        Graphics.Draw.SpriteBatch.Draw(Texture2D, position + new Vector2(i, j), ClipRectangle, Color.Black, 0, -DrawOffset, 1f, SpriteEffects.None, 0);

            Graphics.Draw.SpriteBatch.Draw(Texture2D, position, ClipRectangle, Color.White, 0, -DrawOffset, 1f, SpriteEffects.None, 0);
        }

        public void DrawOutline(Vector2 position, Vector2 origin)
        {
#if DEBUG
            if (Texture2D.IsDisposed)
                throw new Exception("Texture2D Is Disposed");
#endif

            for (var i = -1; i <= 1; i++)
                for (var j = -1; j <= 1; j++)
                    if (i != 0 || j != 0)
                        Graphics.Draw.SpriteBatch.Draw(Texture2D, position + new Vector2(i, j), ClipRectangle, Color.Black, 0, origin - DrawOffset, 1f, SpriteEffects.None, 0);

            Graphics.Draw.SpriteBatch.Draw(Texture2D, position, ClipRectangle, Color.White, 0, origin - DrawOffset, 1f, SpriteEffects.None, 0);
        }

        public void DrawOutline(Vector2 position, Vector2 origin, Color color)
        {
#if DEBUG
            if (Texture2D.IsDisposed)
                throw new Exception("Texture2D Is Disposed");
#endif

            for (var i = -1; i <= 1; i++)
                for (var j = -1; j <= 1; j++)
                    if (i != 0 || j != 0)
                        Graphics.Draw.SpriteBatch.Draw(Texture2D, position + new Vector2(i, j), ClipRectangle, Color.Black, 0, origin - DrawOffset, 1f, SpriteEffects.None, 0);

            Graphics.Draw.SpriteBatch.Draw(Texture2D, position, ClipRectangle, color, 0, origin - DrawOffset, 1f, SpriteEffects.None, 0);
        }

        public void DrawOutline(Vector2 position, Vector2 origin, Color color, float scale)
        {
#if DEBUG
            if (Texture2D.IsDisposed)
                throw new Exception("Texture2D Is Disposed");
#endif

            for (var i = -1; i <= 1; i++)
                for (var j = -1; j <= 1; j++)
                    if (i != 0 || j != 0)
                        Graphics.Draw.SpriteBatch.Draw(Texture2D, position + new Vector2(i, j), ClipRectangle, Color.Black, 0, origin - DrawOffset, scale, SpriteEffects.None, 0);

            Graphics.Draw.SpriteBatch.Draw(Texture2D, position, ClipRectangle, color, 0, origin - DrawOffset, scale, SpriteEffects.None, 0);
        }

        public void DrawOutline(Vector2 position, Vector2 origin, Color color, float scale, float rotation)
        {
#if DEBUG
            if (Texture2D.IsDisposed)
                throw new Exception("Texture2D Is Disposed");
#endif

            for (var i = -1; i <= 1; i++)
                for (var j = -1; j <= 1; j++)
                    if (i != 0 || j != 0)
                        Graphics.Draw.SpriteBatch.Draw(Texture2D, position + new Vector2(i, j), ClipRectangle, Color.Black, rotation, origin - DrawOffset, scale, SpriteEffects.None, 0);

            Graphics.Draw.SpriteBatch.Draw(Texture2D, position, ClipRectangle, color, rotation, origin - DrawOffset, scale, SpriteEffects.None, 0);
        }

        public void DrawOutline(Vector2 position, Vector2 origin, Color color, float scale, float rotation, SpriteEffects flip)
        {
#if DEBUG
            if (Texture2D.IsDisposed)
                throw new Exception("Texture2D Is Disposed");
#endif

            for (var i = -1; i <= 1; i++)
                for (var j = -1; j <= 1; j++)
                    if (i != 0 || j != 0)
                        Graphics.Draw.SpriteBatch.Draw(Texture2D, position + new Vector2(i, j), ClipRectangle, Color.Black, rotation, origin - DrawOffset, scale, flip, 0);

            Graphics.Draw.SpriteBatch.Draw(Texture2D, position, ClipRectangle, color, rotation, origin - DrawOffset, scale, flip, 0);
        }

        public void DrawOutline(Vector2 position, Vector2 origin, Color color, Vector2 scale)
        {
#if DEBUG
            if (Texture2D.IsDisposed)
                throw new Exception("Texture2D Is Disposed");
#endif

            for (var i = -1; i <= 1; i++)
                for (var j = -1; j <= 1; j++)
                    if (i != 0 || j != 0)
                        Graphics.Draw.SpriteBatch.Draw(Texture2D, position + new Vector2(i, j), ClipRectangle, Color.Black, 0, origin - DrawOffset, scale, SpriteEffects.None, 0);

            Graphics.Draw.SpriteBatch.Draw(Texture2D, position, ClipRectangle, color, 0, origin - DrawOffset, scale, SpriteEffects.None, 0);
        }

        public void DrawOutline(Vector2 position, Vector2 origin, Color color, Vector2 scale, float rotation)
        {
#if DEBUG
            if (Texture2D.IsDisposed)
                throw new Exception("Texture2D Is Disposed");
#endif

            for (var i = -1; i <= 1; i++)
                for (var j = -1; j <= 1; j++)
                    if (i != 0 || j != 0)
                        Graphics.Draw.SpriteBatch.Draw(Texture2D, position + new Vector2(i, j), ClipRectangle, Color.Black, rotation, origin - DrawOffset, scale, SpriteEffects.None, 0);

            Graphics.Draw.SpriteBatch.Draw(Texture2D, position, ClipRectangle, color, rotation, origin - DrawOffset, scale, SpriteEffects.None, 0);
        }

        public void DrawOutline(Vector2 position, Vector2 origin, Color color, Vector2 scale, float rotation, SpriteEffects flip)
        {
#if DEBUG
            if (Texture2D.IsDisposed)
                throw new Exception("Texture2D Is Disposed");
#endif

            for (var i = -1; i <= 1; i++)
                for (var j = -1; j <= 1; j++)
                    if (i != 0 || j != 0)
                        Graphics.Draw.SpriteBatch.Draw(Texture2D, position + new Vector2(i, j), ClipRectangle, Color.Black, rotation, origin - DrawOffset, scale, flip, 0);

            Graphics.Draw.SpriteBatch.Draw(Texture2D, position, ClipRectangle, color, rotation, origin - DrawOffset, scale, flip, 0);
        }
        #endregion Draw outline

        #region Draw outline centered
        public void DrawOutlineCentered(Vector2 position)
        {
#if DEBUG
            if (Texture2D.IsDisposed)
                throw new Exception("Texture2D Is Disposed");
#endif

            for (var i = -1; i <= 1; i++)
                for (var j = -1; j <= 1; j++)
                    if (i != 0 || j != 0)
                        Graphics.Draw.SpriteBatch.Draw(Texture2D, position + new Vector2(i, j), ClipRectangle, Color.Black, 0, Center - DrawOffset, 1f, SpriteEffects.None, 0);

            Graphics.Draw.SpriteBatch.Draw(Texture2D, position, ClipRectangle, Color.White, 0, Center - DrawOffset, 1f, SpriteEffects.None, 0);
        }

        public void DrawOutlineCentered(Vector2 position, Color color)
        {
#if DEBUG
            if (Texture2D.IsDisposed)
                throw new Exception("Texture2D Is Disposed");
#endif

            for (var i = -1; i <= 1; i++)
                for (var j = -1; j <= 1; j++)
                    if (i != 0 || j != 0)
                        Graphics.Draw.SpriteBatch.Draw(Texture2D, position + new Vector2(i, j), ClipRectangle, Color.Black, 0, Center - DrawOffset, 1f, SpriteEffects.None, 0);

            Graphics.Draw.SpriteBatch.Draw(Texture2D, position, ClipRectangle, color, 0, Center - DrawOffset, 1f, SpriteEffects.None, 0);
        }

        public void DrawOutlineCentered(Vector2 position, Color color, float scale)
        {
#if DEBUG
            if (Texture2D.IsDisposed)
                throw new Exception("Texture2D Is Disposed");
#endif

            for (var i = -1; i <= 1; i++)
                for (var j = -1; j <= 1; j++)
                    if (i != 0 || j != 0)
                        Graphics.Draw.SpriteBatch.Draw(Texture2D, position + new Vector2(i, j), ClipRectangle, Color.Black, 0, Center - DrawOffset, scale, SpriteEffects.None, 0);

            Graphics.Draw.SpriteBatch.Draw(Texture2D, position, ClipRectangle, color, 0, Center - DrawOffset, scale, SpriteEffects.None, 0);
        }

        public void DrawOutlineCentered(Vector2 position, Color color, float scale, float rotation)
        {
#if DEBUG
            if (Texture2D.IsDisposed)
                throw new Exception("Texture2D Is Disposed");
#endif

            for (var i = -1; i <= 1; i++)
                for (var j = -1; j <= 1; j++)
                    if (i != 0 || j != 0)
                        Graphics.Draw.SpriteBatch.Draw(Texture2D, position + new Vector2(i, j), ClipRectangle, Color.Black, rotation, Center - DrawOffset, scale, SpriteEffects.None, 0);

            Graphics.Draw.SpriteBatch.Draw(Texture2D, position, ClipRectangle, color, rotation, Center - DrawOffset, scale, SpriteEffects.None, 0);
        }

        public void DrawOutlineCentered(Vector2 position, Color color, float scale, float rotation, SpriteEffects flip)
        {
#if DEBUG
            if (Texture2D.IsDisposed)
                throw new Exception("Texture2D Is Disposed");
#endif

            for (var i = -1; i <= 1; i++)
                for (var j = -1; j <= 1; j++)
                    if (i != 0 || j != 0)
                        Graphics.Draw.SpriteBatch.Draw(Texture2D, position + new Vector2(i, j), ClipRectangle, Color.Black, rotation, Center - DrawOffset, scale, flip, 0);

            Graphics.Draw.SpriteBatch.Draw(Texture2D, position, ClipRectangle, color, rotation, Center - DrawOffset, scale, flip, 0);
        }

        public void DrawOutlineCentered(Vector2 position, Color color, Vector2 scale)
        {
#if DEBUG
            if (Texture2D.IsDisposed)
                throw new Exception("Texture2D Is Disposed");
#endif

            for (var i = -1; i <= 1; i++)
                for (var j = -1; j <= 1; j++)
                    if (i != 0 || j != 0)
                        Graphics.Draw.SpriteBatch.Draw(Texture2D, position + new Vector2(i, j), ClipRectangle, Color.Black, 0, Center - DrawOffset, scale, SpriteEffects.None, 0);

            Graphics.Draw.SpriteBatch.Draw(Texture2D, position, ClipRectangle, color, 0, Center - DrawOffset, scale, SpriteEffects.None, 0);
        }

        public void DrawOutlineCentered(Vector2 position, Color color, Vector2 scale, float rotation)
        {
#if DEBUG
            if (Texture2D.IsDisposed)
                throw new Exception("Texture2D Is Disposed");
#endif

            for (var i = -1; i <= 1; i++)
                for (var j = -1; j <= 1; j++)
                    if (i != 0 || j != 0)
                        Graphics.Draw.SpriteBatch.Draw(Texture2D, position + new Vector2(i, j), ClipRectangle, Color.Black, rotation, Center - DrawOffset, scale, SpriteEffects.None, 0);

            Graphics.Draw.SpriteBatch.Draw(Texture2D, position, ClipRectangle, color, rotation, Center - DrawOffset, scale, SpriteEffects.None, 0);
        }

        public void DrawOutlineCentered(Vector2 position, Color color, Vector2 scale, float rotation, SpriteEffects flip)
        {
#if DEBUG
            if (Texture2D.IsDisposed)
                throw new Exception("Texture2D Is Disposed");
#endif

            for (var i = -1; i <= 1; i++)
                for (var j = -1; j <= 1; j++)
                    if (i != 0 || j != 0)
                        Graphics.Draw.SpriteBatch.Draw(Texture2D, position + new Vector2(i, j), ClipRectangle, Color.Black, rotation, Center - DrawOffset, scale, flip, 0);

            Graphics.Draw.SpriteBatch.Draw(Texture2D, position, ClipRectangle, color, rotation, Center - DrawOffset, scale, flip, 0);
        }

        #endregion

        #region Draw Outline Justified

        public void DrawOutlineJustified(Vector2 position, Vector2 justify)
        {
#if DEBUG
            if (Texture2D.IsDisposed)
                throw new Exception("Texture2D Is Disposed");
#endif

            for (var i = -1; i <= 1; i++)
                for (var j = -1; j <= 1; j++)
                    if (i != 0 || j != 0)
                        Graphics.Draw.SpriteBatch.Draw(Texture2D, position + new Vector2(i, j), ClipRectangle, Color.Black, 0, new Vector2(Width * justify.X, Height * justify.Y) - DrawOffset, 1f, SpriteEffects.None, 0);

            Graphics.Draw.SpriteBatch.Draw(Texture2D, position, ClipRectangle, Color.White, 0, new Vector2(Width * justify.X, Height * justify.Y) - DrawOffset, 1f, SpriteEffects.None, 0);
        }

        public void DrawOutlineJustified(Vector2 position, Vector2 justify, Color color)
        {
#if DEBUG
            if (Texture2D.IsDisposed)
                throw new Exception("Texture2D Is Disposed");
#endif

            for (var i = -1; i <= 1; i++)
                for (var j = -1; j <= 1; j++)
                    if (i != 0 || j != 0)
                        Graphics.Draw.SpriteBatch.Draw(Texture2D, position + new Vector2(i, j), ClipRectangle, Color.Black, 0, new Vector2(Width * justify.X, Height * justify.Y) - DrawOffset, 1f, SpriteEffects.None, 0);

            Graphics.Draw.SpriteBatch.Draw(Texture2D, position, ClipRectangle, color, 0, new Vector2(Width * justify.X, Height * justify.Y) - DrawOffset, 1f, SpriteEffects.None, 0);
        }

        public void DrawOutlineJustified(Vector2 position, Vector2 justify, Color color, float scale)
        {
#if DEBUG
            if (Texture2D.IsDisposed)
                throw new Exception("Texture2D Is Disposed");
#endif

            for (var i = -1; i <= 1; i++)
                for (var j = -1; j <= 1; j++)
                    if (i != 0 || j != 0)
                        Graphics.Draw.SpriteBatch.Draw(Texture2D, position + new Vector2(i, j), ClipRectangle, Color.Black, 0, new Vector2(Width * justify.X, Height * justify.Y) - DrawOffset, scale, SpriteEffects.None, 0);

            Graphics.Draw.SpriteBatch.Draw(Texture2D, position, ClipRectangle, color, 0, new Vector2(Width * justify.X, Height * justify.Y) - DrawOffset, scale, SpriteEffects.None, 0);
        }

        public void DrawOutlineJustified(Vector2 position, Vector2 justify, Color color, float scale, float rotation)
        {
#if DEBUG
            if (Texture2D.IsDisposed)
                throw new Exception("Texture2D Is Disposed");
#endif

            for (var i = -1; i <= 1; i++)
                for (var j = -1; j <= 1; j++)
                    if (i != 0 || j != 0)
                        Graphics.Draw.SpriteBatch.Draw(Texture2D, position + new Vector2(i, j), ClipRectangle, Color.Black, rotation, new Vector2(Width * justify.X, Height * justify.Y) - DrawOffset, scale, SpriteEffects.None, 0);

            Graphics.Draw.SpriteBatch.Draw(Texture2D, position, ClipRectangle, color, rotation, new Vector2(Width * justify.X, Height * justify.Y) - DrawOffset, scale, SpriteEffects.None, 0);
        }

        public void DrawOutlineJustified(Vector2 position, Vector2 justify, Color color, float scale, float rotation, SpriteEffects flip)
        {
#if DEBUG
            if (Texture2D.IsDisposed)
                throw new Exception("Texture2D Is Disposed");
#endif

            for (var i = -1; i <= 1; i++)
                for (var j = -1; j <= 1; j++)
                    if (i != 0 || j != 0)
                        Graphics.Draw.SpriteBatch.Draw(Texture2D, position + new Vector2(i, j), ClipRectangle, Color.Black, rotation, new Vector2(Width * justify.X, Height * justify.Y) - DrawOffset, scale, flip, 0);

            Graphics.Draw.SpriteBatch.Draw(Texture2D, position, ClipRectangle, color, rotation, new Vector2(Width * justify.X, Height * justify.Y) - DrawOffset, scale, flip, 0);
        }

        public void DrawOutlineJustified(Vector2 position, Vector2 justify, Color color, Vector2 scale)
        {
#if DEBUG
            if (Texture2D.IsDisposed)
                throw new Exception("Texture2D Is Disposed");
#endif

            for (var i = -1; i <= 1; i++)
                for (var j = -1; j <= 1; j++)
                    if (i != 0 || j != 0)
                        Graphics.Draw.SpriteBatch.Draw(Texture2D, position + new Vector2(i, j), ClipRectangle, Color.Black, 0, new Vector2(Width * justify.X, Height * justify.Y) - DrawOffset, scale, SpriteEffects.None, 0);

            Graphics.Draw.SpriteBatch.Draw(Texture2D, position, ClipRectangle, color, 0, new Vector2(Width * justify.X, Height * justify.Y) - DrawOffset, scale, SpriteEffects.None, 0);
        }

        public void DrawOutlineJustified(Vector2 position, Vector2 justify, Color color, Vector2 scale, float rotation)
        {
#if DEBUG
            if (Texture2D.IsDisposed)
                throw new Exception("Texture2D Is Disposed");
#endif

            for (var i = -1; i <= 1; i++)
                for (var j = -1; j <= 1; j++)
                    if (i != 0 || j != 0)
                        Graphics.Draw.SpriteBatch.Draw(Texture2D, position + new Vector2(i, j), ClipRectangle, Color.Black, rotation, new Vector2(Width * justify.X, Height * justify.Y) - DrawOffset, scale, SpriteEffects.None, 0);

            Graphics.Draw.SpriteBatch.Draw(Texture2D, position, ClipRectangle, color, rotation, new Vector2(Width * justify.X, Height * justify.Y) - DrawOffset, scale, SpriteEffects.None, 0);
        }

        public void DrawOutlineJustified(Vector2 position, Vector2 justify, Color color, Vector2 scale, float rotation, SpriteEffects flip)
        {
#if DEBUG
            if (Texture2D.IsDisposed)
                throw new Exception("Texture2D Is Disposed");
#endif

            for (var i = -1; i <= 1; i++)
                for (var j = -1; j <= 1; j++)
                    if (i != 0 || j != 0)
                        Graphics.Draw.SpriteBatch.Draw(Texture2D, position + new Vector2(i, j), ClipRectangle, Color.Black, rotation, new Vector2(Width * justify.X, Height * justify.Y) - DrawOffset, scale, flip, 0);

            Graphics.Draw.SpriteBatch.Draw(Texture2D, position, ClipRectangle, color, rotation, new Vector2(Width * justify.X, Height * justify.Y) - DrawOffset, scale, flip, 0);
        }
        #endregion Draw outline centered
        #endregion Draw
    }
}
