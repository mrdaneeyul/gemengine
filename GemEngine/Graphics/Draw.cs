﻿using GemEngine.Collision;
using GemEngine.Graphics.Render;
using GemEngine.Utility;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace GemEngine.Graphics
{
    public static class Draw
    {
        /// <summary>
        /// The currently-rendering Renderer
        /// </summary>
        public static Renderer Renderer { get; set; }

        /// <summary>
        /// All 2D rendering is done through this SpriteBatch instance
        /// </summary>
        public static SpriteBatch SpriteBatch { get; private set; }

        /// <summary>
        /// The default Monocle font (Consolas 12). Loaded automatically by Monocle at startup
        /// </summary>
        public static SpriteFont DefaultFont { get; private set; }

        /// <summary>
        /// A subtexture used to draw particle systems.
        /// Will be generated at startup, but you can replace this with a subtexture from your Atlas to reduce texture swaps.
        /// Should be a 2x2 white pixel
        /// </summary>
        public static Texture Particle;

        /// <summary>
        /// A subtexture used to draw rectangles and lines. 
        /// Will be generated at startup, but you can replace this with a subtexture from your Atlas to reduce texture swaps.
        /// Use the top left pixel of your Particle Subtexture if you replace it!
        /// Should be a 1x1 white pixel
        /// </summary>
        public static Texture Pixel;

        private static Rectangle rectangle;

        internal static void Initialize(GraphicsDevice graphicsDevice)
        {
            SpriteBatch = new SpriteBatch(graphicsDevice);
            DefaultFont = Engine.Instance.Content.Load<SpriteFont>(@"Fonts\GemDefault");
            UseDebugPixelTexture();
        }

        public static void UseDebugPixelTexture()
        {
            var texture = new Texture(2, 2, Color.White);
            Pixel = new Texture(texture, 0, 0, 1, 1);
            Particle = new Texture(texture, 0, 0, 2, 2);
        }

        public static void Point(Vector2 at, Color color)
        {
            SpriteBatch.Draw(Pixel.Texture2D, at, Pixel.ClipRectangle, color, 0, Vector2.Zero, 1f, SpriteEffects.None, 0);
        }

        #region Line
        public static void Line(Vector2 start, Vector2 end, Color color)
        {
            LineAngle(start, Calculate.Angle(start, end), Vector2.Distance(start, end), color);
        }

        public static void Line(Vector2 start, Vector2 end, Color color, float thickness)
        {
            LineAngle(start, Calculate.Angle(start, end), Vector2.Distance(start, end), color, thickness);
        }

        public static void Line(float x1, float y1, float x2, float y2, Color color)
        {
            Line(new Vector2(x1, y1), new Vector2(x2, y2), color);
        }
        #endregion Line

        #region Line Angle
        public static void LineAngle(Vector2 start, float angle, float length, Color color)
        {
            SpriteBatch.Draw(Pixel.Texture2D, start, Pixel.ClipRectangle, color, angle, Vector2.Zero, new Vector2(length, 1), SpriteEffects.None, 0);
        }

        public static void LineAngle(Vector2 start, float angle, float length, Color color, float thickness)
        {
            SpriteBatch.Draw(Pixel.Texture2D, start, Pixel.ClipRectangle, color, angle, new Vector2(0, 0.5f), new Vector2(length, thickness), SpriteEffects.None, 0);
        }

        public static void LineAngle(float startX, float startY, float angle, float length, Color color)
        {
            LineAngle(new Vector2(startX, startY), angle, length, color);
        }

        #endregion Line Angle

        #region Circle
        public static void Circle(Vector2 position, float radius, Color color, int resolution)
        {
            var last = Vector2.UnitX * radius;
            var lastPerpendicular = last.Perpendicular();
            for (var i = 1; i <= resolution; i++)
            {
                var at = Calculate.AngleToVector(i * MathHelper.PiOver2 / resolution, radius);
                var atPerpendicular = at.Perpendicular();

                Line(position + last, position + at, color);
                Line(position - last, position - at, color);
                Line(position + lastPerpendicular, position + atPerpendicular, color);
                Line(position - lastPerpendicular, position - atPerpendicular, color);

                last = at;
                lastPerpendicular = atPerpendicular;
            }
        }

        public static void Circle(float x, float y, float radius, Color color, int resolution)
        {
            Circle(new Vector2(x, y), radius, color, resolution);
        }

        public static void Circle(Vector2 position, float radius, Color color, float thickness, int resolution)
        {
            var last = Vector2.UnitX * radius;
            var lastPerpendicular = last.Perpendicular();
            for (var i = 1; i <= resolution; i++)
            {
                var at = Calculate.AngleToVector(i * MathHelper.PiOver2 / resolution, radius);
                var atPerpendicular = at.Perpendicular();

                Line(position + last, position + at, color, thickness);
                Line(position - last, position - at, color, thickness);
                Line(position + lastPerpendicular, position + atPerpendicular, color, thickness);
                Line(position - lastPerpendicular, position - atPerpendicular, color, thickness);

                last = at;
                lastPerpendicular = atPerpendicular;
            }
        }

        public static void Circle(float x, float y, float radius, Color color, float thickness, int resolution)
        {
            Circle(new Vector2(x, y), radius, color, thickness, resolution);
        }
        #endregion Circle

        #region Rectangle
        public static void Rectangle(float x, float y, float width, float height, Color color)
        {
            rectangle.X = (int)x;
            rectangle.Y = (int)y;
            rectangle.Width = (int)width;
            rectangle.Height = (int)height;
            SpriteBatch.Draw(Pixel.Texture2D, rectangle, Pixel.ClipRectangle, color);
        }

        public static void Rectangle(Vector2 position, float width, float height, Color color)
        {
            Rectangle(position.X, position.Y, width, height, color);
        }

        public static void Rectangle(Rectangle rectangle, Color color)
        {
            Draw.rectangle = rectangle;
            SpriteBatch.Draw(Pixel.Texture2D, rectangle, Pixel.ClipRectangle, color);
        }

        public static void Rectangle(Collider collider, Color color)
        {
            Rectangle(collider.AbsoluteLeft, collider.AbsoluteTop, collider.Width, collider.Height, color);
        }
        #endregion Rectangle

        #region Hollow rectangle
        public static void HollowRectangle(float x, float y, float width, float height, Color color)
        {
            rectangle.X = (int)x;
            rectangle.Y = (int)y;
            rectangle.Width = (int)width;
            rectangle.Height = 1;

            SpriteBatch.Draw(Pixel.Texture2D, rectangle, Pixel.ClipRectangle, color);

            rectangle.Y += (int)height - 1;

            SpriteBatch.Draw(Pixel.Texture2D, rectangle, Pixel.ClipRectangle, color);

            rectangle.Y -= (int)height - 1;
            rectangle.Width = 1;
            rectangle.Height = (int)height;

            SpriteBatch.Draw(Pixel.Texture2D, rectangle, Pixel.ClipRectangle, color);

            rectangle.X += (int)width - 1;

            SpriteBatch.Draw(Pixel.Texture2D, rectangle, Pixel.ClipRectangle, color);
        }

        public static void HollowRectangle(Vector2 position, float width, float height, Color color)
        {
            HollowRectangle(position.X, position.Y, width, height, color);
        }

        public static void HollowRectangle(Rectangle rectangle, Color color)
        {
            HollowRectangle(rectangle.X, rectangle.Y, rectangle.Width, rectangle.Height, color);
        }

        public static void HollowRectangle(Collider collider, Color color)
        {
            HollowRectangle(collider.AbsoluteLeft, collider.AbsoluteTop, collider.Width, collider.Height, color);
        }
        #endregion Hollow rectangle

        #region Text
        public static void Text(SpriteFont font, string text, Vector2 position, Color color)
        {
            SpriteBatch.DrawString(font, text, Calculate.Floor(position), color);
        }

        public static void Text(SpriteFont font, string text, Vector2 position, Color color, Vector2 origin, Vector2 scale, float rotation)
        {
            SpriteBatch.DrawString(font, text, Calculate.Floor(position), color, rotation, origin, scale, SpriteEffects.None, 0);
        }

        public static void TextJustified(SpriteFont font, string text, Vector2 position, Color color, Vector2 justify)
        {
            var origin = font.MeasureString(text);
            origin.X *= justify.X;
            origin.Y *= justify.Y;
            SpriteBatch.DrawString(font, text, Calculate.Floor(position), color, 0, origin, 1, SpriteEffects.None, 0);
        }

        public static void TextJustified(SpriteFont font, string text, Vector2 position, Color color, float scale, Vector2 justify)
        {
            var origin = font.MeasureString(text);
            origin.X *= justify.X;
            origin.Y *= justify.Y;
            SpriteBatch.DrawString(font, text, Calculate.Floor(position), color, 0, origin, scale, SpriteEffects.None, 0);
        }

        public static void TextCentered(SpriteFont font, string text, Vector2 position)
        {
            Text(font, text, position - font.MeasureString(text) * .5f, Color.White);
        }

        public static void TextCentered(SpriteFont font, string text, Vector2 position, Color color)
        {
            Text(font, text, position - font.MeasureString(text) * .5f, color);
        }

        public static void TextCentered(SpriteFont font, string text, Vector2 position, Color color, float scale)
        {
            Text(font, text, position, color, font.MeasureString(text) * .5f, Vector2.One * scale, 0);
        }

        public static void TextCentered(SpriteFont font, string text, Vector2 position, Color color, float scale, float rotation)
        {
            Text(font, text, position, color, font.MeasureString(text) * .5f, Vector2.One * scale, rotation);
        }

        public static void OutlineTextCentered(SpriteFont font, string text, Vector2 position, Color color, float scale)
        {
            var origin = font.MeasureString(text) / 2;
            for (var i = -1; i < 2; i++)
                for (var j = -1; j < 2; j++)
                    if (i != 0 || j != 0)
                        SpriteBatch.DrawString(font, text, Calculate.Floor(position) + new Vector2(i, j), Color.Black, 0, origin, scale, SpriteEffects.None, 0);
            SpriteBatch.DrawString(font, text, Calculate.Floor(position), color, 0, origin, scale, SpriteEffects.None, 0);
        }

        public static void OutlineTextCentered(SpriteFont font, string text, Vector2 position, Color color, Color outlineColor)
        {
            var origin = font.MeasureString(text) / 2;
            for (var i = -1; i < 2; i++)
                for (var j = -1; j < 2; j++)
                    if (i != 0 || j != 0)
                        SpriteBatch.DrawString(font, text, Calculate.Floor(position) + new Vector2(i, j), outlineColor, 0, origin, 1, SpriteEffects.None, 0);
            SpriteBatch.DrawString(font, text, Calculate.Floor(position), color, 0, origin, 1, SpriteEffects.None, 0);
        }

        public static void OutlineTextCentered(SpriteFont font, string text, Vector2 position, Color color, Color outlineColor, float scale)
        {
            var origin = font.MeasureString(text) / 2;
            for (var i = -1; i < 2; i++)
                for (var j = -1; j < 2; j++)
                    if (i != 0 || j != 0)
                        SpriteBatch.DrawString(font, text, Calculate.Floor(position) + new Vector2(i, j), outlineColor, 0, origin, scale, SpriteEffects.None, 0);
            SpriteBatch.DrawString(font, text, Calculate.Floor(position), color, 0, origin, scale, SpriteEffects.None, 0);
        }

        public static void OutlineTextJustify(SpriteFont font, string text, Vector2 position, Color color, Color outlineColor, Vector2 justify)
        {
            var origin = font.MeasureString(text) * justify;
            for (var i = -1; i < 2; i++)
                for (var j = -1; j < 2; j++)
                    if (i != 0 || j != 0)
                        SpriteBatch.DrawString(font, text, Calculate.Floor(position) + new Vector2(i, j), outlineColor, 0, origin, 1, SpriteEffects.None, 0);
            SpriteBatch.DrawString(font, text, Calculate.Floor(position), color, 0, origin, 1, SpriteEffects.None, 0);
        }

        public static void OutlineTextJustify(SpriteFont font, string text, Vector2 position, Color color, Color outlineColor, Vector2 justify, float scale)
        {
            var origin = font.MeasureString(text) * justify;
            for (var i = -1; i < 2; i++)
                for (var j = -1; j < 2; j++)
                    if (i != 0 || j != 0)
                        SpriteBatch.DrawString(font, text, Calculate.Floor(position) + new Vector2(i, j), outlineColor, 0, origin, scale, SpriteEffects.None, 0);
            SpriteBatch.DrawString(font, text, Calculate.Floor(position), color, 0, origin, scale, SpriteEffects.None, 0);
        }
        #endregion Text

        #region Weird textures
        public static void SineTextureH(Texture texture, Vector2 position, Vector2 origin, Vector2 scale, float rotation, Color color, SpriteEffects effects, float sineCounter, float amplitude = 2, int sliceSize = 2, float sliceAdd = MathHelper.TwoPi / 8)
        {
            position = Calculate.Floor(position);
            var clip = texture.ClipRectangle;
            clip.Width = sliceSize;

            var num = 0;
            while (clip.X < texture.ClipRectangle.X + texture.ClipRectangle.Width)
            {
                var add = new Vector2(sliceSize * num, (float)Math.Round(Math.Sin(sineCounter + sliceAdd * num) * amplitude));
                SpriteBatch.Draw(texture.Texture2D, position, clip, color, rotation, origin - add, scale, effects, 0);

                num++;
                clip.X += sliceSize;
                clip.Width = Math.Min(sliceSize, texture.ClipRectangle.X + texture.ClipRectangle.Width - clip.X);
            }
        }

        public static void SineTextureV(Texture texture, Vector2 position, Vector2 origin, Vector2 scale, float rotation, Color color, SpriteEffects effects, float sineCounter, float amplitude = 2, int sliceSize = 2, float sliceAdd = MathHelper.TwoPi / 8)
        {
            position = Calculate.Floor(position);
            var clip = texture.ClipRectangle;
            clip.Height = sliceSize;

            var num = 0;
            while (clip.Y < texture.ClipRectangle.Y + texture.ClipRectangle.Height)
            {
                var add = new Vector2((float)Math.Round(Math.Sin(sineCounter + sliceAdd * num) * amplitude), sliceSize * num);
                SpriteBatch.Draw(texture.Texture2D, position, clip, color, rotation, origin - add, scale, effects, 0);

                num++;
                clip.Y += sliceSize;
                clip.Height = Math.Min(sliceSize, texture.ClipRectangle.Y + texture.ClipRectangle.Height - clip.Y);
            }
        }

        public static void TextureBannerV(Texture texture, Vector2 position, Vector2 origin, Vector2 scale, float rotation, Color color, SpriteEffects effects, float sineCounter, float amplitude = 2, int sliceSize = 2, float sliceAdd = MathHelper.TwoPi / 8)
        {
            position = Calculate.Floor(position);
            var clip = texture.ClipRectangle;
            clip.Height = sliceSize;

            var num = 0;
            while (clip.Y < texture.ClipRectangle.Y + texture.ClipRectangle.Height)
            {
                var fade = (clip.Y - texture.ClipRectangle.Y) / (float)texture.ClipRectangle.Height;
                clip.Height = (int)MathHelper.Lerp(sliceSize, 1, fade);
                clip.Height = Math.Min(sliceSize, texture.ClipRectangle.Y + texture.ClipRectangle.Height - clip.Y);

                var add = new Vector2((float)Math.Round(Math.Sin(sineCounter + sliceAdd * num) * amplitude * fade), clip.Y - texture.ClipRectangle.Y);
                SpriteBatch.Draw(texture.Texture2D, position, clip, color, rotation, origin - add, scale, effects, 0);

                num++;
                clip.Y += clip.Height;
            }
        }
        #endregion Weird textures
    }
}
