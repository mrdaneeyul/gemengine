﻿using GemEngine.Utility;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace GemEngine.ECS
{
    public class ComponentList : IEnumerable<Component>, IEnumerable
    {
        public enum LockModes { Open, Locked, Error };
        public Entity Entity { get; set; }
        public int Count => components.Count;

        private List<Component> components;
        private List<Component> toAdd;
        private List<Component> toRemove;
        private HashSet<Component> current;
        private HashSet<Component> adding;
        private HashSet<Component> removing;
        private LockModes lockMode;
        
        public LockModes LockMode
        {
            get { return lockMode; }
            set
            {
                lockMode = value;

                if (toAdd.Count > 0)
                {
                    foreach (var component in toAdd)
                    {
                        if (!current.Contains(component))
                        {
                            current.Add(component);
                            components.Add(component);
                            component.Added(Entity);
                        }
                    }
                    adding.Clear();
                    toAdd.Clear();
                }

                if (toRemove.Count > 0)
                {
                    foreach (var component in toRemove)
                    {
                        if (current.Contains(component))
                        {
                            current.Remove(component);
                            components.Remove(component);
                            component.Removed(Entity);
                        }
                        removing.Clear();
                        toRemove.Clear();
                    }
                }
            }
        }
        
        public ComponentList(Entity entity)
        {
            Entity = entity;

            components = new List<Component>();
            toAdd = new List<Component>();
            toRemove = new List<Component>();
            current = new HashSet<Component>();
            adding = new HashSet<Component>();
            removing = new HashSet<Component>();
        }

        public void Add(Component component)
        {
            switch (lockMode)
            {
                case LockModes.Open:
                    if (!current.Contains(component))
                    {
                        current.Add(component);
                        components.Add(component);
                        component.Added(Entity);
                    }
                    break;
                case LockModes.Locked:
                    if (!current.Contains(component) && !adding.Contains(component))
                    {
                        adding.Add(component);
                        toAdd.Add(component);
                    }
                    break;
                case LockModes.Error:
                    throw new Exception("Cannot add Entities at this time");
            }
        }

        public void Remove(Component component)
        {
            switch (lockMode)
            {
                case LockModes.Open:
                    if (current.Contains(component))
                    {
                        current.Remove(component);
                        components.Remove(component);
                        component.Removed(Entity);
                    }
                    break;
                case LockModes.Locked:
                    if (current.Contains(component) && !removing.Contains(component))
                    {
                        removing.Add(component);
                        toRemove.Add(component);
                    }
                    break;
                case LockModes.Error:
                    throw new Exception("Cannot remove Entities at this time");
            }
        }

        public void Add(IEnumerable<Component> components)
        {
            foreach (var component in components)
                Add(component);
        }

        public void Remove(IEnumerable<Component> components)
        {
            foreach (var component in components)
                Remove(component);
        }

        public void Add(params Component[] components)
        {
            foreach (var component in components)
                Add(component);
        }

        public void Remove(params Component[] components)
        {
            foreach (var component in components)
                Remove(component);
        }

        public void RemoveAll<T>() where T : Component
        {
            Remove(GetAll<T>());
        }
        
        public Component this[int index]
        {
            get
            {
                if (index < 0 || index >= components.Count)
                    throw new IndexOutOfRangeException();
                else
                    return components[index];
            }
        }

        public IEnumerator<Component> GetEnumerator()
        {
            return components.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public Component[] ToArray()
        {
            return components.ToArray<Component>();
        }

        public void Update()
        {
            LockMode = LockModes.Locked;
            foreach (var component in components.Where(c => c.IsActive))
                component.Update();
            LockMode = LockModes.Open;
        }

        public void Render()
        {
            LockMode = LockModes.Error;
            foreach (var component in components.Where(c => c.IsVisible))
                component.Update();
            LockMode = LockModes.Open;
        }

        public void DebugRender(Camera camera)
        {
            LockMode = LockModes.Error;
            foreach (var component in components)
                component.DebugRender(camera);
            LockMode = LockModes.Open;
        }

        public void HandleGraphicsReset()
        {
            LockMode = LockModes.Error;
            foreach (var component in components)
                component.HandleGraphicsReset();
            LockMode = LockModes.Open;
        }

        public void HandleGraphicsCreate()
        {
            LockMode = LockModes.Error;
            foreach (var component in components)
                component.HandleGraphicsCreate();
            LockMode = LockModes.Open;
        }

        public T Get<T>() where T : Component
        {
            foreach (var component in components.Where(c => c is T))
                return component as T;
            return null;
        }

        public IEnumerable<T> GetAll<T>() where T : Component
        {
            foreach (var component in components.Where(c => c is T))
                yield return component as T;
        }
    }
}