﻿using System.Collections;
using System.Collections.Generic;

namespace GemEngine.ECS
{
    public class Coroutine : Component
    {
        public bool IsFinished { get; private set; }
        public bool ShouldRemoveOnComplete = true;
        public bool ShouldUseRawDeltaTime = false;

        private Stack<IEnumerator> enumerators;
        private float waitTimer;
        private bool hasEnded;

        public Coroutine(IEnumerator functionCall, bool removeOnComplete = true) : base(true, false)
        {
            enumerators = new Stack<IEnumerator>();
            enumerators.Push(functionCall);
            ShouldRemoveOnComplete = removeOnComplete;
        }

        public Coroutine(bool removeOnComplete = true) : base(false, false)
        {
            ShouldRemoveOnComplete = removeOnComplete;
            enumerators = new Stack<IEnumerator>();
        }

        public override void Update()
        {
            hasEnded = false;

            if (waitTimer > 0)
                waitTimer -= (ShouldUseRawDeltaTime ? Engine.RawDeltaTime : Engine.DeltaTime);
            else if (enumerators.Count > 0)
            {
                var now = enumerators.Peek();
                if (now.MoveNext() && !hasEnded)
                {
                    if (now.Current is int)
                        waitTimer = (int)now.Current;
                    if (now.Current is float)
                        waitTimer = (float)now.Current;
                    else if (now.Current is IEnumerator)
                        enumerators.Push(now.Current as IEnumerator);
                }
                else if (!hasEnded)
                {
                    enumerators.Pop();
                    if (enumerators.Count == 0)
                    {
                        IsFinished = true;
                        IsActive = false;
                        if (ShouldRemoveOnComplete)
                            RemoveSelf();
                    }
                }
            }
        }

        public void Cancel()
        {
            IsActive = false;
            IsFinished = true;
            waitTimer = 0;
            enumerators.Clear();

            hasEnded = true;
        }

        public void Replace(IEnumerator functionCall)
        {
            IsActive = true;
            IsFinished = false;
            waitTimer = 0;
            enumerators.Clear();
            enumerators.Push(functionCall);

            hasEnded = true;
        }
    }
}
