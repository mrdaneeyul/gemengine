﻿using GemEngine.Utility;
using System;
using System.Collections;

namespace GemEngine.ECS.Components
{
    /// <summary>
    /// Represents a state machine for an entity. In essence, this class can hold multiple "states," each of which
    /// has an update method that runs each update cycle of the game. A state can also have a coroutine (which can
    /// run alongside the update method), as well as a begin or end state. The coroutine, begin, and end methods
    /// are optional. Here is an example of adding a StateMachine within the code of an entity:
    ///     StateMachine = new StateMachine(3);
    ///                            //stateID,      Update(),     Coroutine(),   Begin(),     End()
    ///     StateMachine.CreateState(STATE_NORMAL, NormalUpdate, null,          NormalBegin, NormalEnd);
    ///     StateMachine.CreateState(STATE_CLIMB,  ClimbUpdate,  null,          ClimbBegin,  ClimbEnd);
    ///     StateMachine.CreateState(STATE DASH,   DashUpdate,   DashCoroutine, DashBegin,   DashEnd);
    ///     Add(StateMachine);
    /// </summary>
    public class StateMachine : Component
    {
        private int stateID;
        private Action[] beginMethods;
        private Func<int>[] updateMethods;
        private Action[] endMethods;
        private Func<IEnumerator>[] coroutineMethods;
        private Coroutine currentCoroutine;

        public bool HasJustChangedStates;
        public bool ShouldLog;
        public int PreviousState { get; private set; }
        public bool IsLocked;
        /// <summary>
        /// Gets or sets the current state. When setting the state, this will also run the end method of the previous
        /// state and then the begin method of the new state.
        /// </summary>
        public int StateID
        {
            get { return stateID; }
            set
            {
#if DEBUG
                if (value >= updateMethods.Length || value < 0)
                    throw new Exception($"StateMachine state {value} is out of range");
#endif
                if (IsLocked || stateID == value) return;

                if (ShouldLog)
                    Calculate.Log($"Entering state {value} (leaving {stateID})");

                HasJustChangedStates = true;
                PreviousState = stateID;
                stateID = value;

                if (PreviousState != -1 && endMethods[PreviousState] != null)
                {
                    if (ShouldLog)
                        Calculate.Log($"Calling End method on {PreviousState}");
                    endMethods[PreviousState]();
                }

                if (beginMethods[stateID] != null)
                {
                    if (ShouldLog)
                        Calculate.Log($"Calling Begin method on {stateID}");
                    beginMethods[stateID]();
                }

                if (coroutineMethods[stateID] != null)
                {
                    if (ShouldLog)
                        Calculate.Log($"Starting Coroutine {stateID}");
                    currentCoroutine.Replace(coroutineMethods[stateID]());
                }
                else
                    currentCoroutine.Cancel();
            }
        }

        /// <summary>
        /// Constructor for the state machine which creates the arrays to hold the begin, update, end, and coroutine methods.
        /// </summary>
        /// <param name="maxStates">The maximum amount of states (default 10) which determines how big each array
        /// for the begin/update/end/coroutine methods is. Ideally, this number should equal exactly the largest
        /// number (so if there are 3 end methods and 1 update method, maxStates should equal 3. More simply,
        /// this should just equal the number of states you're adding to the entity.</param>
        public StateMachine(int maxStates = 10) : base(true, false)
        {
            PreviousState = stateID = -1;

            beginMethods = new Action[maxStates];
            updateMethods = new Func<int>[maxStates];
            endMethods = new Action[maxStates];
            coroutineMethods = new Func<IEnumerator>[maxStates];

            currentCoroutine = new Coroutine { ShouldRemoveOnComplete = false };
        }

        public override void Added(Entity entity)
        {
            base.Added(entity);
            if (Entity.Scene != null && stateID == -1)
                StateID = 0;
        }

        public override void EntityAdded(Scene scene)
        {
            base.EntityAdded(scene);
            if (stateID == -1)
                StateID = 0;
        }

        /// <summary>
        /// Forces the specified state to begin. If it's a different state than the current state, then it switches to that state
        /// normally (and this method is useless). But if it's the same state, it will force the state to restart itself.
        /// </summary>
        /// <param name="toState">The state to switch to</param>
        public void ForceState(int toState)
        {
            if (stateID != toState)
            {
                StateID = toState;
                return;
            }

            if (ShouldLog)
                Calculate.Log($"Entering state {toState} (leaving {stateID})");

            HasJustChangedStates = true;
            PreviousState = stateID;
            stateID = toState;

            if (PreviousState != -1 && endMethods[PreviousState] != null)
            {
                if (ShouldLog)
                    Calculate.Log($"Calling End method on {stateID}");
                endMethods[PreviousState]();
            }

            if (beginMethods[stateID] != null)
            {
                if (ShouldLog)
                    Calculate.Log($"Calling Begin method on {stateID}");
                beginMethods[stateID]();
            }

            if (coroutineMethods[stateID] != null)
            {
                if (ShouldLog)
                    Calculate.Log($"Starting Coroutine {stateID}");
                currentCoroutine.Replace(coroutineMethods[stateID]());
            }
            else
                currentCoroutine.Cancel();
        }

        /// <summary>
        /// Creates a state by setting its callback methods: state Update methods, coroutines, a begin state method, and 
        /// an end state method. All callbacks are optional except the Update callback.
        /// </summary>
        /// <param name="state">The state to set the callbacks for</param>
        /// <param name="updateMethod">The update method. This is the only required method for a state.</param>
        /// <param name="coroutineMethod">An optional coroutine method.</param>
        /// <param name="beginMethod">An optional begin method.</param>
        /// <param name="endMethod">An optional end method.</param>
        public void CreateState(int state, Func<int> updateMethod, Func<IEnumerator> coroutineMethod = null, Action beginMethod = null, Action endMethod = null)
        {
            updateMethods[state] = updateMethod;
            beginMethods[state] = beginMethod;
            endMethods[state] = endMethod;
            coroutineMethods[state] = coroutineMethod;
        }

        /// <summary>
        /// Clones a state from another entity
        /// </summary>
        /// <param name="fromEntity">The entity to clone the state from</param>
        /// <param name="stateID">The ID of the new state</param>
        /// <param name="name">The name of the state to copy</param>
        public void CloneState(Entity fromEntity, int stateID, string name)
        {
            updateMethods[stateID] = (Func<int>)Calculate.GetMethod<Func<int>>(fromEntity, $"{name}Update");
            beginMethods[stateID] = (Action)Calculate.GetMethod<Action>(fromEntity, $"{name}Begin");
            endMethods[stateID] = (Action)Calculate.GetMethod<Action>(fromEntity, $"{name}End");
            coroutineMethods[stateID] = (Func<IEnumerator>)Calculate.GetMethod<Func<IEnumerator>>(fromEntity, $"{name}Coroutine");
        }

        /// <summary>
        /// Run the update method for the currently selected state, as well as the coroutine (if it exists and is active).
        /// </summary>
        public override void Update()
        {
            HasJustChangedStates = false;

            if (updateMethods[stateID] != null)
                StateID = updateMethods[stateID]();
            if (currentCoroutine.IsActive)
            {
                currentCoroutine.Update();
                if (!HasJustChangedStates && ShouldLog && currentCoroutine.IsFinished)
                    Calculate.Log($"Finished Coroutine {stateID}");
            }
        }

        public static implicit operator int(StateMachine s)
        {
            return s.stateID;
        }


        /// <summary>
        /// Logs all states along with a letter to indicate if each statehas an update, begin, end, or coroutine method
        /// ("U", "B", "E", and "C", respectively).
        /// </summary>
        public void LogAllStates()
        {
            for (var i = 0; i < updateMethods.Length; i++)
                LogState(i);
        }

        /// <summary>
        /// Logs the specified state along with a letter to indicate if it has an update, begin, end, or coroutine method
        /// ("U", "B", "E", and "C", respectively). 
        /// </summary>
        /// <param name="stateID">The ID of the state to log</param>
        public void LogState(int stateID)
        {
            Calculate.Log($@"State {stateID}: 
                {(updateMethods[stateID]    != null ? "U" : string.Empty)}
                {(beginMethods[stateID]     != null ? "B" : string.Empty)}
                {(endMethods[stateID]       != null ? "E" : string.Empty)}
                {(coroutineMethods[stateID] != null ? "C" : string.Empty)}");
        }
    }
}
