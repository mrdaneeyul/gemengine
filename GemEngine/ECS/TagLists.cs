﻿using GemEngine.Utility;
using System.Collections.Generic;

namespace GemEngine.ECS
{
    public class TagLists
    {
        private List<Entity>[] lists;
        private bool[] isUnsorted;
        private bool areAnyUnsorted;

        internal TagLists()
        {
            lists = new List<Entity>[BitTag.TotalTags];
            isUnsorted = new bool[BitTag.TotalTags];
            for (var i = 0; i < lists.Length; i++)
                lists[i] = new List<Entity>();
        }

        public List<Entity> this[int index] => lists[index];

        internal void MarkUnsorted(int index)
        {
            areAnyUnsorted = true;
            isUnsorted[index] = true;
        }

        internal void UpdateLists()
        {
            if (!areAnyUnsorted) return;

            for (var i = 0; i < lists.Length; i++)
            {
                if (!isUnsorted[i]) continue;
                lists[i].Sort(EntityList.CompareDepth);
                isUnsorted[i] = false;
            }
            areAnyUnsorted = false;
        }

        internal void EntityAdded(Entity entity)
        {
            for (var i = 0; i < BitTag.TotalTags; i++)
            {
                if (!entity.TagCheck(1 << i)) continue;
                this[i].Add(entity);
                areAnyUnsorted = true;
                isUnsorted[i] = true;
            }
        }

        internal void EntityRemoved(Entity entity)
        {
            for (var i = 0; i < BitTag.TotalTags; i++)
                if (entity.TagCheck(1 << i))
                    lists[i].Remove(entity);
        }
    }
}
