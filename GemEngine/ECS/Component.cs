﻿using GemEngine.Utility;

namespace GemEngine.ECS
{
    public class Component
    {
        public Entity Entity { get; private set; }
        public bool IsActive;
        public bool IsVisible;
        public Scene Scene => Entity?.Scene;

        public Component(bool isActive, bool isVisible)
        {
            IsActive = isActive;
            IsVisible = isVisible;
        }

        public virtual void Added(Entity entity)
        {
            Entity = entity;
            Scene?.Tracker.ComponentAdded(this);
        }

        public virtual void Removed(Entity entity)
        {
            Scene?.Tracker.ComponentRemoved(this);
            Entity = null;
        }

        public virtual void EntityAdded(Scene scene)
        {
            scene.Tracker.ComponentAdded(this);
        }

        public virtual void EntityRemoved(Scene scene)
        {
            scene.Tracker.ComponentRemoved(this);
        }

        public virtual void SceneEnd(Scene scene) { }

        public virtual void EntityAwake() { }

        public virtual void Update() { }

        public virtual void Render() { }

        public virtual void DebugRender(Camera camera) { }

        public virtual void HandleGraphicsReset() { }

        public virtual void HandleGraphicsCreate() { }

        public void RemoveSelf()
        {
            Entity?.Remove(this);
        }

        public T SceneAs<T>() where T : Scene
        {
            return Scene as T;
        }

        public T EntityAs<T>() where T : Entity
        {
            return Entity as T;
        }
    }
}
