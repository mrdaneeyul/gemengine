﻿using GemEngine.Collision;
using GemEngine.Utility;
using Microsoft.Xna.Framework;
using System;
using System.Collections;
using System.Collections.Generic;

namespace GemEngine.ECS
{
    public class Entity : IEnumerable<Component>, IEnumerable
    {
        public bool IsActive = true;
        public bool IsVisible = true;
        public bool IsCollidable = true;
        public Vector2 Position;

        public Scene Scene { get; private set; }
        public ComponentList Components { get; private set; }

        private int tag;
        private Collider collider;
        internal int depth = 0;
        internal double actualDepth = 0;

        public Entity(Vector2 position)
        {
            Position = position;
            Components = new ComponentList(this);
        }

        public Entity() : this(Vector2.Zero) { }

        /// <summary>
        /// Called when the containing scene begins
        /// </summary>
        /// <param name="scene">The scene that is beginning</param>
        public virtual void SceneBegin(Scene scene) { }

        /// <summary>
        /// Called when the containing scene ends
        /// </summary>
        /// <param name="scene">The scene that is ending</param>
        public virtual void SceneEnd(Scene scene)
        {
            if (Components != null)
                foreach (var component in Components)
                    component.EntityAwake();
        }

        /// <summary>
        /// Called before the frame starts, after Entities are added and removed, on the frame that the Entity was added
        /// Useful if you added two Entities in the same frame, and need them to detect each other before they start Updating
        /// </summary>
        /// <param name="scene">The scene the entity is awakening in</param>
        public virtual void Awake(Scene scene)
        {
            if (Components != null)
                foreach (var component in Components)
                    component.EntityAwake();
        }

        /// <summary>
        /// Called when this Entity is added to a Scene, which only occurs immediately before each Update. 
        /// Keep in mind, other Entities to be added this frame may be added after this Entity. 
        /// See Awake() for after all Entities are added, but still before the frame Updates.
        /// </summary>
        /// <param name="scene">The scene the entity has been added to</param>
        public virtual void Added(Scene scene)
        {
            Scene = scene;
            if (Components != null)
                foreach (var component in Components)
                    component.EntityAdded(scene);
            Scene.SetActualDepth(this);
        }

        /// <summary>
        /// Called when the Entity is removed from a Scene
        /// </summary>
        /// <param name="scene">The scene the entity has been removed from</param>
        public virtual void Removed(Scene scene)
        {
            if (Components != null)
                foreach (var component in Components)
                    component.EntityRemoved(scene);
            Scene = null;
        }

        /// <summary>
        /// Do game logic here, but not rendering. Not called if the entity is not active.
        /// </summary>
        public virtual void Update()
        {
            Components.Update();
        }

        /// <summary>
        /// Draw the entity here. Not called if the entity is not active.
        /// </summary>
        public virtual void Render()
        {
            Components.Render();
        }

        /// <summary>
        /// Draw any debug visuals here. Only called if the console is open, but still called even if the Entity is not Visible
        /// </summary>
        public virtual void DebugRender(Camera camera)
        {
            collider?.Render(camera, IsCollidable ? Color.Red : Color.DarkRed);
            Components.DebugRender(camera);
        }

        /// <summary>
        /// Called when the graphics device resets. When this happens, any RenderTargets or other contents of VRAM will be wiped and need to be regenerated
        /// </summary>
        public virtual void HandleGraphicsReset()
        {
            Components.HandleGraphicsReset();
        }

        public virtual void HandleGraphicsCreate()
        {
            Components.HandleGraphicsCreate();
        }

        public void RemoveSelf()
        {
            Scene?.Entities.Remove(this);
        }

        public int Depth
        {
            get { return depth; }
            set
            {
                if (depth == value) return;
                depth = value;
                Scene?.SetActualDepth(this);
            }
        }

        public float X
        {
            get { return Position.X; }
            set { Position.X = value; }
        }

        public float Y
        {
            get { return Position.Y; }
            set { Position.Y = value; }
        }

        #region Collider
        public Collider Collider
        {
            get { return collider; }
            set
            {
                if (value == collider) return;
#if DEBUG
                if (value.Entity != null)
                    throw new Exception("Cannot set an Entity's Collider to a Collider already in use by another Entity");
#endif
                if (collider != null)
                    collider.Removed();
                collider = value;
                if (collider != null)
                    collider.Added(this);
            }
        }

        public float Width
        {
            get
            {
                if (Collider == null)
                    return 0;
                else
                    return Collider.Width;
            }
        }

        public float Height
        {
            get
            {
                if (Collider == null)
                    return 0;
                else
                    return Collider.Height;
            }
        }

        public float Left
        {
            get
            {
                if (Collider == null)
                    return X;
                else
                    return Position.X + Collider.Left;
            }
            set
            {
                if (Collider == null)
                    Position.X = value;
                else
                    Position.X = value - Collider.Left;
            }
        }

        public float Right
        {
            get
            {
                if (Collider == null)
                    return Position.X;
                else
                    return Position.X + Collider.Right;
            }
            set
            {
                if (Collider == null)
                    Position.X = value;
                else
                    Position.X = value - Collider.Right;
            }
        }

        public float Top
        {
            get
            {
                if (Collider == null)
                    return Position.Y;
                else
                    return Position.Y + Collider.Top;
            }
            set
            {
                if (Collider == null)
                    Position.Y = value;
                else
                    Position.Y = value - Collider.Top;
            }
        }

        public float Bottom
        {
            get
            {
                if (Collider == null)
                    return Position.Y;
                else
                    return Position.Y + Collider.Bottom;
            }
            set
            {
                if (Collider == null)
                    Position.Y = value;
                else
                    Position.Y = value - Collider.Bottom;
            }
        }

        public float CenterX
        {
            get
            {
                if (Collider == null)
                    return Position.X;
                else
                    return Position.X + Collider.CenterX;
            }
            set
            {
                if (Collider == null)
                    Position.X = value;
                else
                    Position.X = value - Collider.CenterX;
            }
        }

        public float CenterY
        {
            get
            {
                if (Collider == null)
                    return Position.Y;
                else
                    return Position.Y + Collider.CenterY;
            }
            set
            {
                if (Collider == null)
                    Position.Y = value;
                else
                    Position.Y = value - Collider.CenterY;
            }
        }

        public Vector2 TopLeft
        {
            get { return new Vector2(Left, Top); }
            set
            {
                Left = value.X;
                Top = value.Y;
            }
        }

        public Vector2 TopRight
        {
            get { return new Vector2(Right, Top); }
            set
            {
                Right = value.X;
                Top = value.Y;
            }
        }

        public Vector2 BottomLeft
        {
            get { return new Vector2(Left, Bottom); }
            set
            {
                Left = value.X;
                Bottom = value.Y;
            }
        }

        public Vector2 BottomRight
        {
            get { return new Vector2(Right, Bottom); }
            set
            {
                Right = value.X;
                Bottom = value.Y;
            }
        }

        public Vector2 Center
        {
            get { return new Vector2(CenterX, CenterY); }
            set
            {
                CenterX = value.X;
                CenterY = value.Y;
            }
        }

        public Vector2 CenterLeft
        {
            get { return new Vector2(Left, CenterY); }
            set
            {
                Left = value.X;
                CenterY = value.Y;
            }
        }

        public Vector2 CenterRight
        {
            get { return new Vector2(Right, CenterY); }
            set
            {
                Right = value.X;
                CenterY = value.Y;
            }
        }

        public Vector2 TopCenter
        {
            get { return new Vector2(CenterX, Top); }
            set
            {
                CenterX = value.X;
                Top = value.Y;
            }
        }

        public Vector2 BottomCenter
        {
            get { return new Vector2(CenterX, Bottom); }
            set
            {
                CenterX = value.X;
                Bottom = value.Y;
            }
        }
        #endregion Collider

        #region Tag
        public int Tag
        {
            get { return tag; }
            set
            {
                if (tag == value) return;
                if (Scene != null)
                {
                    for (var i = 0; i < BitTag.TotalTags; i++)
                    {
                        var check = 1 << i;
                        var shouldAdd = (value & check) != 0;
                        var hasTag = (Tag & check) != 0;

                        if (hasTag != shouldAdd)
                        {
                            if (shouldAdd)
                                Scene.TagLists[i].Add(this);
                            else
                                Scene.TagLists[i].Remove(this);
                        }
                    }
                }
                tag = value;
            }
        }

        public bool TagFullCheck(int tag)
        {
            return (this.tag & tag) == tag;
        }

        public bool TagCheck(int tag)
        {
            return (this.tag & tag) != 0;
        }

        public void AddTag(int tag)
        {
            Tag |= tag;
        }

        public void RemoveTag(int tag)
        {
            Tag &= ~tag;
        }
        #endregion Tag

        #region Collision shortcuts
        #region Collide check
        public bool CollideCheck(Entity otherEntity)
        {
            return Collide.Check(this, otherEntity);
        }

        public bool CollideCheck(Entity otherEntity, Vector2 atPoint)
        {
            return Collide.Check(this, otherEntity, atPoint);
        }

        public bool CollideCheck(BitTag tag)
        {
#if DEBUG
            if (Scene == null)
                throw new Exception("Can't collide check an Entity against a tag list when it is not a member of a Scene");
#endif
            return Collide.Check(this, Scene[tag]);
        }

        public bool CollideCheck(BitTag tag, Vector2 atPoint)
        {
#if DEBUG
            if (Scene == null)
                throw new Exception("Can't collide check an Entity against a tag list when it is not a member of a Scene");
#endif
            return Collide.Check(this, Scene[tag], atPoint);
        }

        public bool CollideCheck<T>() where T : Entity
        {
#if DEBUG
            if (Scene == null)
                throw new Exception("Can't collide check an Entity against a tag list when it is not a member of a Scene");
            else if (!Scene.Tracker.Entities.ContainsKey(typeof(T)))
                throw new Exception("Can't collide check an Entity against an untracked Entity type");
#endif
            return Collide.Check(this, Scene.Tracker.Entities[typeof(T)]);
        }

        public bool CollideCheck<T>(Vector2 atPoint) where T : Entity
        {
            return Collide.Check(this, Scene.Tracker.Entities[typeof(T)], atPoint);
        }

        public bool CollideCheck<T, Exclude>() where T : Entity where Exclude : Entity
        {
#if DEBUG
            if (Scene == null)
                throw new Exception("Can't collide check an Entity against a tag list when it is not a member of a Scene");
            else if (!Scene.Tracker.Entities.ContainsKey(typeof(T)))
                throw new Exception("Can't collide check an Entity against an untracked Entity type");
            else if (!Scene.Tracker.Entities.ContainsKey(typeof(Exclude)))
                throw new Exception("Excluded type is an untracked Entity type");
#endif
            var exclude = Scene.Tracker.Entities[typeof(Exclude)];
            foreach (var entity in Scene.Tracker.Entities[typeof(T)])
                if (!exclude.Contains(entity) && Collide.Check(this, entity))
                    return true;
            return false;
        }

        public bool CollideCheck<T, Exclude>(Vector2 atPoint) where T : Entity where Exclude : Entity
        {
            var old = Position;
            Position = atPoint;
            var ret = CollideCheck<T, Exclude>();
            Position = old;
            return ret;
        }

        public bool CollideCheckByComponent<T>() where T : Component
        {
#if DEBUG
            if (Scene == null)
                throw new Exception("Can't collide check an Entity against a tag list when it is not a member of a Scene");
            else if (!Scene.Tracker.Components.ContainsKey(typeof(T)))
                throw new Exception("Can't collide check an Entity against an untracked Component type");
#endif
            foreach (var component in Scene.Tracker.Components[typeof(T)])
                if (Collide.Check(this, component.Entity))
                    return true;
            return false;
        }

        public bool CollideCheckByComponent<T>(Vector2 atPoint) where T : Component
        {
            var old = Position;
            Position = atPoint;
            var ret = CollideCheckByComponent<T>();
            Position = old;
            return ret;
        }
        #endregion Collide check

        #region Collide check outside
        public bool CollideCheckOutside(Entity otherEntity, Vector2 atPoint)
        {
            return !Collide.Check(this, otherEntity) && Collide.Check(this, otherEntity, atPoint);
        }

        public bool CollideCheckOutside(BitTag tag, Vector2 atPoint)
        {
#if DEBUG
            if (Scene == null)
                throw new Exception("Can't collide check an Entity against a tag list when it is not a member of a Scene");
#endif
            foreach (var entity in Scene[tag])
                if (!Collide.Check(this, entity) && Collide.Check(this, entity, atPoint))
                    return true;
            return false;
        }

        public bool CollideCheckOutside<T>(Vector2 atPoint) where T : Entity
        {
#if DEBUG
            if (Scene == null)
                throw new Exception("Can't collide check an Entity against tracked Entities when it is not a member of a Scene");
            else if (!Scene.Tracker.Entities.ContainsKey(typeof(T)))
                throw new Exception("Can't collide check an Entity against an untracked Entity type");
#endif
            foreach (var entity in Scene.Tracker.Entities[typeof(T)])
                if (!Collide.Check(this, entity) && Collide.Check(this, entity, atPoint))
                    return true;
            return false;
        }

        public bool CollideCheckOutsideByComponent<T>(Vector2 atPoint) where T : Entity
        {
#if DEBUG
            if (Scene == null)
                throw new Exception("Can't collide check an Entity against tracked Entities when it is not a member of a Scene");
            else if (!Scene.Tracker.Components.ContainsKey(typeof(T)))
                throw new Exception("Can't collide check an Entity against an untracked Component type");
#endif
            foreach (var component in Scene.Tracker.Components[typeof(T)])
                if (!Collide.Check(this, component.Entity) && Collide.Check(this, component.Entity, atPoint))
                    return true;
            return false;
        }
        #endregion Collide check outside

        #region Collide first
        public Entity CollideFirst(BitTag tag)
        {
#if DEBUG
            if (Scene == null)
                throw new Exception("Can't collide check an Entity against a tag list when it is not a member of a Scene");
#endif
            return Collide.First(this, Scene[tag]);
        }

        public Entity CollideFirst(BitTag tag, Vector2 atPoint)
        {
#if DEBUG
            if (Scene == null)
                throw new Exception("Can't collide check an Entity against a tag list when it is not a member of a Scene");
#endif
            return Collide.First(this, Scene[tag], atPoint);
        }

        public T CollideFirst<T>() where T : Entity
        {
#if DEBUG
            if (Scene == null)
                throw new Exception("Can't collide check an Entity against a tag list when it is not a member of a Scene");
            else if (!Scene.Tracker.Entities.ContainsKey(typeof(T)))
                throw new Exception("Can't collide check an Entity against an untracked Entity type");
#endif
            return Collide.First(this, Scene.Tracker.Entities[typeof(T)]) as T;
        }

        public T CollideFirst<T>(Vector2 atPoint) where T : Entity
        {
#if DEBUG
            if (Scene == null)
                throw new Exception("Can't collide check an Entity against a tag list when it is not a member of a Scene");
            else if (!Scene.Tracker.Entities.ContainsKey(typeof(T)))
                throw new Exception("Can't collide check an Entity against an untracked Entity type");
#endif
            return Collide.First(this, Scene.Tracker.Entities[typeof(T)], atPoint) as T;
        }

        public T CollideFirstByComponent<T>() where T : Component
        {
#if DEBUG
            if (Scene == null)
                throw new Exception("Can't collide check an Entity against tracked Entities when it is not a member of a Scene");
            else if (!Scene.Tracker.Components.ContainsKey(typeof(T)))
                throw new Exception("Can't collide check an Entity against an untracked Component type");
#endif

            foreach (var component in Scene.Tracker.Components[typeof(T)])
                if (Collide.Check(this, component.Entity))
                    return component as T;
            return null;
        }

        public T CollideFirstByComponent<T>(Vector2 at) where T : Component
        {
#if DEBUG
            if (Scene == null)
                throw new Exception("Can't collide check an Entity against tracked Entities when it is not a member of a Scene");
            else if (!Scene.Tracker.Components.ContainsKey(typeof(T)))
                throw new Exception("Can't collide check an Entity against an untracked Component type");
#endif

            foreach (var component in Scene.Tracker.Components[typeof(T)])
                if (Collide.Check(this, component.Entity, at))
                    return component as T;
            return null;
        }
        #endregion Collide first

        #region Collide first outside

        public Entity CollideFirstOutside(BitTag tag, Vector2 atPoint)
        {
#if DEBUG
            if (Scene == null)
                throw new Exception("Can't collide check an Entity against a tag list when it is not a member of a Scene");
#endif

            foreach (var entity in Scene[tag])
                if (!Collide.Check(this, entity) && Collide.Check(this, entity, atPoint))
                    return entity;
            return null;
        }

        public T CollideFirstOutside<T>(Vector2 atPoint) where T : Entity
        {
#if DEBUG
            if (Scene == null)
                throw new Exception("Can't collide check an Entity against tracked Entities when it is not a member of a Scene");
            else if (!Scene.Tracker.Entities.ContainsKey(typeof(T)))
                throw new Exception("Can't collide check an Entity against an untracked Entity type");
#endif

            foreach (var entity in Scene.Tracker.Entities[typeof(T)])
                if (!Collide.Check(this, entity) && Collide.Check(this, entity, atPoint))
                    return entity as T;
            return null;
        }

        public T CollideFirstOutsideByComponent<T>(Vector2 atPoint) where T : Component
        {
#if DEBUG
            if (Scene == null)
                throw new Exception("Can't collide check an Entity against tracked Entities when it is not a member of a Scene");
            else if (!Scene.Tracker.Components.ContainsKey(typeof(T)))
                throw new Exception("Can't collide check an Entity against an untracked Component type");
#endif

            foreach (var component in Scene.Tracker.Components[typeof(T)])
                if (!Collide.Check(this, component.Entity) && Collide.Check(this, component.Entity, atPoint))
                    return component as T;
            return null;
        }
        #endregion Collide first outside

        #region Collide all
        public List<Entity> CollideAll(BitTag tag)
        {
#if DEBUG
            if (Scene == null)
                throw new Exception("Can't collide check an Entity against a tag list when it is not a member of a Scene");
#endif
            return Collide.All(this, Scene[tag]);
        }

        public List<Entity> CollideAll(BitTag tag, Vector2 atPoint)
        {
#if DEBUG
            if (Scene == null)
                throw new Exception("Can't collide check an Entity against a tag list when it is not a member of a Scene");
#endif
            return Collide.All(this, Scene[tag], atPoint);
        }

        public List<Entity> CollideAll<T>() where T : Entity
        {
#if DEBUG
            if (Scene == null)
                throw new Exception("Can't collide check an Entity against tracked Entities when it is not a member of a Scene");
            else if (!Scene.Tracker.Entities.ContainsKey(typeof(T)))
                throw new Exception("Can't collide check an Entity against an untracked Entity type");
#endif

            return Collide.All(this, Scene.Tracker.Entities[typeof(T)]);
        }

        public List<Entity> CollideAll<T>(Vector2 atPoint) where T : Entity
        {
#if DEBUG
            if (Scene == null)
                throw new Exception("Can't collide check an Entity against tracked Entities when it is not a member of a Scene");
            else if (!Scene.Tracker.Entities.ContainsKey(typeof(T)))
                throw new Exception("Can't collide check an Entity against an untracked Entity type");
#endif

            return Collide.All(this, Scene.Tracker.Entities[typeof(T)], atPoint);
        }

        public List<Entity> CollideAll<T>(Vector2 atPoint, List<Entity> into) where T : Entity
        {
#if DEBUG
            if (Scene == null)
                throw new Exception("Can't collide check an Entity against tracked Entities when it is not a member of a Scene");
            else if (!Scene.Tracker.Entities.ContainsKey(typeof(T)))
                throw new Exception("Can't collide check an Entity against an untracked Entity type");
#endif

            into.Clear();
            return Collide.All(this, Scene.Tracker.Entities[typeof(T)], into, atPoint);
        }

        public List<T> CollideAllByComponent<T>() where T : Component
        {
#if DEBUG
            if (Scene == null)
                throw new Exception("Can't collide check an Entity against tracked Entities when it is not a member of a Scene");
            else if (!Scene.Tracker.Components.ContainsKey(typeof(T)))
                throw new Exception("Can't collide check an Entity against an untracked Component type");
#endif

            var list = new List<T>();
            foreach (var component in Scene.Tracker.Components[typeof(T)])
                if (Collide.Check(this, component.Entity))
                    list.Add(component as T);
            return list;
        }

        public List<T> CollideAllByComponent<T>(Vector2 atPoint) where T : Component
        {
            var old = Position;
            Position = atPoint;
            var ret = CollideAllByComponent<T>();
            Position = old;
            return ret;
        }
        #endregion Collide all

        #region Collide do
        public bool CollideDo(BitTag tag, Action<Entity> action)
        {
#if DEBUG
            if (Scene == null)
                throw new Exception("Can't collide check an Entity against a tag list when it is not a member of a Scene");
#endif

            var hit = false;
            foreach (var other in Scene[tag])
            {
                if (CollideCheck(other))
                {
                    action(other);
                    hit = true;
                }
            }
            return hit;
        }

        public bool CollideDo(BitTag tag, Action<Entity> action, Vector2 atPoint)
        {
#if DEBUG
            if (Scene == null)
                throw new Exception("Can't collide check an Entity against a tag list when it is not a member of a Scene");
#endif
            var hit = false;
            var was = Position;
            Position = atPoint;

            foreach (var other in Scene[tag])
            {
                if (CollideCheck(other))
                {
                    action(other);
                    hit = true;
                }
            }

            Position = was;
            return hit;
        }

        public bool CollideDo<T>(Action<T> action) where T : Entity
        {
#if DEBUG
            if (Scene == null)
                throw new Exception("Can't collide check an Entity against tracked Entities when it is not a member of a Scene");
            else if (!Scene.Tracker.Entities.ContainsKey(typeof(T)))
                throw new Exception("Can't collide check an Entity against an untracked Entity type");
#endif
            var hit = false;
            foreach (var other in Scene.Tracker.Entities[typeof(T)])
            {
                if (CollideCheck(other))
                {
                    action(other as T);
                    hit = true;
                }
            }
            return hit;
        }

        public bool CollideDo<T>(Action<T> action, Vector2 atPoint) where T : Entity
        {
#if DEBUG
            if (Scene == null)
                throw new Exception("Can't collide check an Entity against tracked Entities when it is not a member of a Scene");
            else if (!Scene.Tracker.Entities.ContainsKey(typeof(T)))
                throw new Exception("Can't collide check an Entity against an untracked Entity type");
#endif
            var hit = false;
            var was = Position;
            Position = atPoint;

            foreach (var other in Scene.Tracker.Entities[typeof(T)])
            {
                if (CollideCheck(other))
                {
                    action(other as T);
                    hit = true;
                }
            }

            Position = was;
            return hit;
        }

        public bool CollideDoByComponent<T>(Action<T> action) where T : Component
        {
#if DEBUG
            if (Scene == null)
                throw new Exception("Can't collide check an Entity against tracked Entities when it is not a member of a Scene");
            else if (!Scene.Tracker.Components.ContainsKey(typeof(T)))
                throw new Exception("Can't collide check an Entity against an untracked Component type");
#endif

            var hit = false;
            foreach (var component in Scene.Tracker.Components[typeof(T)])
            {
                if (CollideCheck(component.Entity))
                {
                    action(component as T);
                    hit = true;
                }
            }
            return hit;
        }

        public bool CollideDoByComponent<T>(Action<T> action, Vector2 atPoint) where T : Component
        {
#if DEBUG
            if (Scene == null)
                throw new Exception("Can't collide check an Entity against tracked Entities when it is not a member of a Scene");
            else if (!Scene.Tracker.Components.ContainsKey(typeof(T)))
                throw new Exception("Can't collide check an Entity against an untracked Component type");
#endif

            var hit = false;
            var was = Position;
            Position = atPoint;

            foreach (var component in Scene.Tracker.Components[typeof(T)])
            {
                if (CollideCheck(component.Entity))
                {
                    action(component as T);
                    hit = true;
                }
            }

            Position = was;
            return hit;
        }
        #endregion Collide do

        #region Collide geometry
        public bool CollidePoint(Vector2 point)
        {
            return Collide.CheckPoint(this, point);
        }

        public bool CollidePoint(Vector2 point, Vector2 atPoint)
        {
            return Collide.CheckPoint(this, point, atPoint);
        }

        public bool CollideLine(Vector2 fromPoint, Vector2 toPoint)
        {
            return Collide.CheckLine(this, fromPoint, toPoint);
        }

        public bool CollideLine(Vector2 fromPoint, Vector2 toPoint, Vector2 atPoint)
        {
            return Collide.CheckLine(this, fromPoint, toPoint, atPoint);
        }

        public bool CollideRectangle(Rectangle rectangle)
        {
            return Collide.CheckRect(this, rectangle);
        }

        public bool CollideRectangle(Rectangle rectangle, Vector2 atPoint)
        {
            return Collide.CheckRect(this, rectangle, atPoint);
        }
        #endregion Collide geometry
        #endregion Collision shortcuts

        #region Components shortcuts
        /// <summary>
        /// Shortcut function for adding a Component to the Entity's Components list
        /// </summary>
        /// <param name="component">The Component to add</param>
        public void Add(Component component)
        {
            Components.Add(component);
        }

        /// <summary>
        /// Shortcut function for removing an Component from the Entity's Components list
        /// </summary>
        /// <param name="component">The Component to remove</param>
        public void Remove(Component component)
        {
            Components.Remove(component);
        }

        /// <summary>
        /// Shortcut function for adding a set of Components from the Entity's Components list
        /// </summary>
        /// <param name="components">The Components to add</param>
        public void Add(params Component[] components)
        {
            Components.Add(components);
        }

        /// <summary>
        /// Shortcut function for removing a set of Components from the Entity's Components list
        /// </summary>
        /// <param name="components">The Components to remove</param>
        public void Remove(params Component[] components)
        {
            Components.Remove(components);
        }

        public T Get<T>() where T : Component
        {
            return Components.Get<T>();
        }

        /// <summary>
        /// Allows you to iterate through all Components in the Entity
        /// </summary>
        /// <returns></returns>
        public IEnumerator<Component> GetEnumerator()
        {
            return Components.GetEnumerator();
        }

        /// <summary>
        /// Allows you to iterate through all Components in the Entity
        /// </summary>
        /// <returns></returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        #endregion Components shortcuts

        #region Miscellaneous utilities
        public Entity Closest(params Entity[] entities)
        {
            var closest = entities[0];
            var distance = Vector2.DistanceSquared(Position, closest.Position);
            for (var i = 0; i < entities.Length; i++)
            {
                var currentDistance = Vector2.DistanceSquared(Position, entities[i].Position);
                if (currentDistance < distance)
                {
                    closest = entities[i];
                    distance = currentDistance;
                }
            }
            return closest;
        }

        public Entity Closest(BitTag tag)
        {
            var list = Scene[tag];
            Entity closest = null;
            float distance;
            if (list.Count >= 1)
            {
                closest = list[0];
                distance = Vector2.DistanceSquared(Position, closest.Position);
                for (var i = 1; i < list.Count; i++)
                {
                    var currentDistance = Vector2.DistanceSquared(Position, list[i].Position);
                    if (currentDistance < distance)
                    {
                        closest = list[i];
                        distance = currentDistance;
                    }
                }
            }
            return closest;
        }

        public T SceneAs<T>() where T : Scene
        {
            return Scene as T;
        }
        #endregion Miscellaneous utilities
    }
}
