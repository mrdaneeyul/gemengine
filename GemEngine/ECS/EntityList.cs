﻿using GemEngine.Utility;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace GemEngine.ECS
{
    public class EntityList : IEnumerable<Entity>, IEnumerable
    {
        public Scene Scene { get; private set; }

        private List<Entity> entities;
        private List<Entity> toAdd;
        private List<Entity> toAwaken;
        private List<Entity> toRemove;
        private HashSet<Entity> current;
        private HashSet<Entity> adding;
        private HashSet<Entity> removing;
        private bool isUnsorted;

        internal EntityList(Scene scene)
        {
            Scene = scene;
            entities = new List<Entity>();
            toAdd = new List<Entity>();
            toAwaken = new List<Entity>();
            toRemove = new List<Entity>();
            current = new HashSet<Entity>();
            adding = new HashSet<Entity>();
            removing = new HashSet<Entity>();
        }

        internal void MarkUnsorted()
        {
            isUnsorted = true;
        }

        public void UpdateLists()
        {
            for (var i = 0; i < toAdd.Count; i++)
            {
                var entity = toAdd[i];
                if(!current.Contains(entity))
                {
                    current.Add(entity);
                    entities.Add(entity);
                    if (Scene != null)
                    {
                        Scene.TagLists.EntityAdded(entity);
                        Scene.Tracker.EntityAdded(entity);
                        entity.Added(Scene);
                    }
                }
            }
            if (toAdd.Count > 0)
                isUnsorted = true;

            for (var i = 0; i < toRemove.Count; i++)
            {
                var entity = toRemove[i];
                if (entities.Contains(entity))
                {
                    current.Remove(entity);
                    entities.Remove(entity);
                    if (Scene != null)
                    {
                        entity.Removed(Scene);
                        Scene.TagLists.EntityRemoved(entity);
                        Scene.Tracker.EntityRemoved(entity);
                        Engine.Pooler.EntityRemoved(entity);
                    }
                }
            }
            if (toRemove.Count > 0)
            {
                toRemove.Clear();
                removing.Clear();
            }

            if (isUnsorted)
            {
                isUnsorted = false;
                entities.Sort(CompareDepth);
            }

            if (toAdd.Count > 0)
            {
                toAwaken.AddRange(toAdd);
                toAdd.Clear();
                adding.Clear();

                foreach (var entity in toAwaken)
                    if (entity.Scene == Scene)
                        entity.Awake(Scene);
                toAwaken.Clear();
            }
        }

        public void Add(Entity entity)
        {
            if (adding.Contains(entity) || current.Contains(entity)) return;
            adding.Add(entity);
            toAdd.Add(entity);
        }

        public void Remove(Entity entity)
        {
            if (removing.Contains(entity) || !current.Contains(entity)) return;
            removing.Add(entity);
            toRemove.Add(entity);
        }

        public void Add(IEnumerable<Entity> entities)
        {
            foreach (var entity in entities)
                Add(entity);
        }

        public void Remove(IEnumerable<Entity> entities)
        {
            foreach (var entity in entities)
                Remove(entity);
        }

        public void Add(params Entity[] entities)
        {
            for (var i = 0; i < entities.Length; i++)
                Add(entities[i]);
        }

        public void Remove(params Entity[] entities)
        {
            for (var i = 0; i < entities.Length; i++)
                Remove(entities[i]);
        }

        public int Count {  get { return entities.Count; } }

        public Entity this[int index]
        {
            get
            {
                if (index < 0 || index >= entities.Count)
                    throw new IndexOutOfRangeException();
                else
                    return entities[index];
            }
        }

        public int AmountOf<T>() where T : Entity
        {
            var count = 0;
            foreach (var entity in entities)
                if (entity is T)
                    count++;
            return count;
        }

        public T FindFirst<T>() where T : Entity
        {
            foreach (var entity in entities)
                if (entity is T)
                    return entity as T;
            return null;
        }

        public List<T> FindAll<T>() where T : Entity
        {
            var list = new List<T>();
            foreach (var entity in entities)
                if (entity is T)
                    list.Add(entity as T);
            return list;
        }

        public void With<T>(Action<T> action) where T : Entity
        {
            foreach (var entity in entities)
                if (entity is T)
                    action(entity as T);
        }

        public IEnumerator<Entity> GetEnumerator()
        {
            return entities.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public Entity[] ToArray()
        {
            return entities.ToArray<Entity>();
        }

        public bool HasVisibleEntities(int matchTags)
        {
            foreach (var entity in entities)
                if (entity.IsVisible && entity.TagCheck(matchTags))
                    return true;
            return false;
        }

        internal void Update()
        {
            foreach (var entity in entities)
                if (entity.IsActive)
                    entity.Update();
        }

        internal void Render()
        {
            foreach (var entity in entities)
                if (entity.IsActive)
                    entity.Render();
        }

        public void RenderOnly(int matchTags)
        {
            foreach (var entity in entities)
                if (entity.IsVisible && entity.TagCheck(matchTags))
                    entity.Render();
        }

        public void RenderOnlyFullMatch(int matchTags)
        {
            foreach (var entity in entities)
                if (entity.IsVisible && entity.TagFullCheck(matchTags))
                    entity.Render();
        }

        public void RenderExcept(int excludeTags)
        {
            foreach (var entity in entities)
                if (entity.IsVisible && !entity.TagCheck(excludeTags))
                    entity.Render();
        }

        public void DebugRender(Camera camera)
        {
            foreach (var entity in entities)
                entity.DebugRender(camera);
        }

        internal void HandleGraphicsReset()
        {
            foreach (var entity in entities)
                entity.HandleGraphicsReset();
        }

        internal void HandleGraphicsCreate()
        {
            foreach (var entity in entities)
                entity.HandleGraphicsCreate();
        }
        
        public static Comparison<Entity> CompareDepth = (a, b) => { return Math.Sign(b.actualDepth - a.actualDepth); };
    }
}
