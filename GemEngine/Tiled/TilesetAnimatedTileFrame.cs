﻿using Microsoft.Xna.Framework;

namespace GemEngine.Tiled
{
    public struct TilesetAnimatedTileFrame
    {
        public readonly int LocalTileID;
        public readonly float Duration;
        public readonly Vector2[] TextureCoordinates;

        internal TilesetAnimatedTileFrame(Tileset tileset, int localTileID, int durationInMilliseconds)
        {
            LocalTileID = localTileID;
            Duration = durationInMilliseconds;
            TextureCoordinates = new Vector2[4];
            CreateTextureCoordinates(tileset);
        }

        private void CreateTextureCoordinates(Tileset tileset)
        {
            var sourceRectangle = tileset.GetTileRegion(LocalTileID);
            var texture = tileset.Texture;
            var texelLeft = sourceRectangle.X / texture.Width;
            var texelTop = sourceRectangle.Y / texture.Height;
            var texelRight = (sourceRectangle.X + sourceRectangle.Width) / (float)texture.Width;
            var texelBottom = (sourceRectangle.Y + sourceRectangle.Height) / (float)texture.Height;

            TextureCoordinates[0].X = texelLeft;
            TextureCoordinates[0].Y = texelTop;

            TextureCoordinates[1].X = texelRight;
            TextureCoordinates[1].Y = texelTop;

            TextureCoordinates[2].X = texelLeft;
            TextureCoordinates[2].Y = texelBottom;

            TextureCoordinates[3].X = texelRight;
            TextureCoordinates[3].Y = texelBottom;
        }

        public override string ToString() => $"{LocalTileID}:{Duration}";
    }
}
