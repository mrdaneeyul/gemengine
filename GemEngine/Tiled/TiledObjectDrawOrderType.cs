﻿namespace GemEngine.Tiled
{
    public enum TiledObjectDrawOrderType : byte
    {
        TopDown,
        Index,
    }
}
