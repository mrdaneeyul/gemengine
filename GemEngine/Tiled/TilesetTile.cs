﻿using Microsoft.Xna.Framework.Content;
using System;

namespace GemEngine.Tiled
{
    public class TilesetTile
    {
        public int LocalTileID { get; set; }
        public TiledMapProperties Properties { get; private set; }
        public TiledObject[] Objects { get; set; }

        internal TilesetTile(int localTileID, ContentReader input)
        {
            var objectCount = input.ReadInt32();
            var objects = new TiledObject[objectCount];

            for (var i = 0; i < objectCount; i++)
            {
                TiledObject @object;
                var objectType = (TiledObjectType)input.ReadByte();
                switch (objectType)
                {
                    case TiledObjectType.Rectangle:
                        @object = new TiledMapRectangleObject(input);
                        break;
                    case TiledObjectType.Ellipse:
                        @object = new TiledMapEllipseObject(input);
                        break;
                    case TiledObjectType.Polygon:
                        @object = new TiledMapPolygonObject(input);
                        break;
                    case TiledObjectType.Polyline:
                        @object = new TiledMapPolylineObject(input);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
                objects[i] = @object;
            }

            Objects = objects;
            LocalTileID = localTileID;
            Properties = new TiledMapProperties();
        }
    }
}
