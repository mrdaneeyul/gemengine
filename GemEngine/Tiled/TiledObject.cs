﻿using GemEngine.Utility;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using System.Linq;

namespace GemEngine.Tiled
{
    public abstract class TiledObject
    {
        public int ID { get; }
        public string Name { get; set; }
        public string Type { get; }
        public TiledMapProperties Properties { get; }
        public bool IsVisible { get; set; }
        public float Opacity { get; set; }
        public float Rotation { get; set; }
        public Vector2 Position { get; set; }
        public Size2 Size { get; set; }

        internal TiledObject(ContentReader input)
        {
            ID = input.ReadInt32();
            Name = input.ReadString();
            Type = input.ReadString();
            Position = new Vector2(input.ReadSingle(), input.ReadSingle());
            var width = input.ReadSingle();
            var height = input.ReadSingle();
            Size = new Size2(width, height);
            Rotation = input.ReadSingle();
            IsVisible = input.ReadBoolean();

            Properties = new TiledMapProperties();
            input.ReadTiledMapProperties(Properties);
        }

        public override string ToString() => ID.ToString();
    }

    public sealed class TiledMapRectangleObject : TiledObject
    {
        internal TiledMapRectangleObject(ContentReader input) : base(input) { }
    }

    public sealed class TiledMapEllipseObject : TiledObject
    {
        public Vector2 Center { get; }
        public float RadiusX { get; }
        public float RadiusY { get; }

        internal TiledMapEllipseObject(ContentReader input) : base(input)
        {
            RadiusX = Size.Width / 2.0f;
            RadiusY = Size.Height / 2.0f;
            Center = new Vector2(Position.X + RadiusX, Position.Y);
        }
    }

    public sealed class TiledMapPolygonObject : TiledObject
    {
        public Point2[] Points { get; }

        internal TiledMapPolygonObject(ContentReader input) : base(input)
        {
            var pointCount = input.ReadInt32();
            Points = new Point2[pointCount];

            for (var i = 0; i < pointCount; i++)
            {
                var x = input.ReadSingle();
                var y = input.ReadSingle();
                Points[i] = new Point2(x, y);
            }
        }
    }

    public sealed class TiledMapPolylineObject : TiledObject
    {
        public Point2[] Points { get; }

        internal TiledMapPolylineObject(ContentReader input)
            : base(input)
        {
            var pointCount = input.ReadInt32();
            Points = new Point2[pointCount];

            for (var i = 0; i < pointCount; i++)
            {
                var x = input.ReadSingle();
                var y = input.ReadSingle();
                Points[i] = new Point2(x, y);
            }
        }
    }

    public sealed class TiledMapTileObject : TiledObject
    {
        public TilesetTile TilesetTile { get; }
        public Tileset Tileset { get; }

        public TiledMapTileObject(ContentReader input, TiledMap map) : base(input)
        {
            var globalTileIDWithFlags = input.ReadUInt32();
            var tile = new Tile(globalTileIDWithFlags);
            Tileset = map.GetTilesetByTileGlobalID(tile.GlobalID);

            var localTileID = tile.GlobalID - Tileset.FirstGlobalID;
            TilesetTile = Tileset.Tiles.FirstOrDefault(x => x.LocalTileID == localTileID);
        }
    }
}