﻿namespace GemEngine.Tiled
{
    public enum TileDrawOrderType : byte //TODO: may not need this since this is for isometric
    {
        RightDown,
        RightUp,
        LeftDown,
        LeftUp
    }
}
