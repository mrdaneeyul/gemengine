﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.IO;
using System.Reflection;

namespace GemEngine.Tiled
{
    public static class ContentReaderExtensions //TODO: Move this to Utilities folder
    {
        private static readonly FieldInfo contentReaderGraphicsDeviceFieldInfo = typeof(ContentReader).GetTypeInfo().GetDeclaredField("graphicsDevice");

        public static void ReadTiledMapProperties(this ContentReader reader, TiledMapProperties properties)
        {
            var count = reader.ReadInt32();
            for (var i = 0; i < count; i++)
            {
                var key = reader.ReadString();
                var value = reader.ReadString();
                properties[key] = value;
            }
        }

        public static GraphicsDevice GetGraphicsDevice(this ContentReader contentReader)
        {
            return (GraphicsDevice)contentReaderGraphicsDeviceFieldInfo.GetValue(contentReader);
        }

        public static string GetRelativeAssetName(this ContentReader contentReader, string relativeName)
        {
            var assetDirectory = Path.GetDirectoryName(contentReader.AssetName);
            var assetName = Path.Combine(assetDirectory, relativeName).Replace('\\', '/');

            var ellipseIndex = assetName.IndexOf("/../", StringComparison.Ordinal);
            while (ellipseIndex != -1)
            {
                var lastDirectoryIndex = assetName.LastIndexOf('/', ellipseIndex - 1);
                if (lastDirectoryIndex == -1)
                    lastDirectoryIndex = 0;
                assetName = assetName.Remove(lastDirectoryIndex, ellipseIndex + 4);
                ellipseIndex = assetName.IndexOf("/../", StringComparison.Ordinal);
            }

            return assetName;
        }
    }
}
