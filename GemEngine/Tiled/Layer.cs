﻿using GemEngine.Tiled.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.ObjectModel;
using System.Linq;

namespace GemEngine.Tiled
{
    /// <summary>
    /// A generic abstract class representing a layer in a Tiled map
    /// Based on the MonoGame.Extended class
    /// </summary>
    public abstract class Layer : IDisposable
    {
        internal LayerModel[] Models;
        internal LayerAnimatedModel[] AnimatedModels;

        public string Name { get; }
        public TiledMapProperties Properties { get; }
        public bool IsVisible { get; set; }
        public float Opacity { get; set; }
        public float OffsetX { get; }
        public float OffsetY { get; }

        internal Layer(ContentReader input)
        {
            Models = null;
            AnimatedModels = null;
            Name = input.ReadString();
            Properties = new TiledMapProperties();
            IsVisible = input.ReadBoolean();
            Opacity = input.ReadSingle();
            OffsetX = input.ReadSingle();
            OffsetY = input.ReadSingle();

            input.ReadTiledMapProperties(Properties);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool shouldDispose)
        {
            if (!shouldDispose) return;
            Models?.ToList().ForEach(m => Dispose());
        }
    }

    public class ImageLayer : Layer
    {
        public Vector2 Position { get; set; }
        public Texture2D Texture { get; }

        internal ImageLayer(ContentReader input)
            : base(input)
        {
            var textureAssetName = input.GetRelativeAssetName(input.ReadString());
            Texture = input.ContentManager.Load<Texture2D>(textureAssetName);
            var x = input.ReadSingle();
            var y = input.ReadSingle();
            Position = new Vector2(x, y);
        }
    }

    public class TileLayer : Layer
    {
        // immutable
        public int Width { get; }
        public int Height { get; }
        public int TileWidth { get; }
        public int TileHeight { get; }
        public ReadOnlyCollection<Tile> Tiles { get; }

        internal TileLayer(ContentReader input, TiledMap map)
            : base(input)
        {
            Width = input.ReadInt32();
            Height = input.ReadInt32();
            TileWidth = map.TileWidth;
            TileHeight = map.TileHeight;

            var tileCount = input.ReadInt32();
            var tiles = new Tile[Width * Height];
            Tiles = new ReadOnlyCollection<Tile>(tiles);

            for (var i = 0; i < tileCount; i++)
            {
                var globalTileIdentifierWithFlags = input.ReadUInt32();
                var x = input.ReadUInt16();
                var y = input.ReadUInt16();
                tiles[x + y * Width] = new Tile(globalTileIdentifierWithFlags);
            }
        }

        public bool TryGetTile(int x, int y, out Tile? tile)
        {
            var index = x + y * Width;
            if ((index < 0) || (index >= Tiles.Count))
            {
                tile = null;
                return false;
            }

            tile = Tiles[index];
            return true;
        }
    }

    public class ObjectLayer : Layer
    {
        public Color Color { get; }
        public TiledObjectDrawOrderType DrawOrder { get; }
        public TiledObject[] Objects { get; }

        internal ObjectLayer(ContentReader input, TiledMap map)
            : base(input)
        {
            Color = input.ReadColor();
            DrawOrder = (TiledObjectDrawOrderType)input.ReadByte();

            var objectCount = input.ReadInt32();

            Objects = new TiledObject[objectCount];

            for (var i = 0; i < objectCount; i++)
            {
                TiledObject @object;

                var objectType = (TiledObjectType)input.ReadByte();
                switch (objectType)
                {
                    case TiledObjectType.Rectangle:
                        @object = new TiledMapRectangleObject(input);
                        break;
                    case TiledObjectType.Tile:
                        @object = new TiledMapTileObject(input, map);
                        break;
                    case TiledObjectType.Ellipse:
                        @object = new TiledMapEllipseObject(input);
                        break;
                    case TiledObjectType.Polygon:
                        @object = new TiledMapPolygonObject(input);
                        break;
                    case TiledObjectType.Polyline:
                        @object = new TiledMapPolylineObject(input);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                Objects[i] = @object;
            }
        }
    }
}
