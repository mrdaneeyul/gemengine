﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace GemEngine.Tiled
{
    public class TilesetAnimatedTile : TilesetTile
    {
        public ReadOnlyCollection<TilesetAnimatedTileFrame> AnimationFrames { get; }
        public TilesetAnimatedTileFrame CurrentAnimationFrame { get; private set; }

        private readonly List<TilesetAnimatedTileFrame> animationFrames = new List<TilesetAnimatedTileFrame>();
        private int frameIndex;
        private float timer;

        internal TilesetAnimatedTile(Tileset tileset, ContentReader input, int localTileID, int animationFramesCount) : base(localTileID, input)
        {
            AnimationFrames = new ReadOnlyCollection<TilesetAnimatedTileFrame>(animationFrames);
            timer = 0;

            for (var i = 0; i < animationFramesCount; i++)
            {
                var localTileIDForFrame = input.ReadInt32();
                var frameDurationInMilliseconds = input.ReadInt32();
                var tilesetTileFrame = new TilesetAnimatedTileFrame(tileset, localTileIDForFrame, frameDurationInMilliseconds);
                animationFrames.Add(tilesetTileFrame);
                CurrentAnimationFrame = AnimationFrames[0];
            }
        }

        public void Update(GameTime gameTime)
        {
            if (animationFrames.Count == 0) return;

            timer += Engine.DeltaTime;
            if (timer <= CurrentAnimationFrame.Duration) return;

            timer -= CurrentAnimationFrame.Duration;
            frameIndex = (frameIndex + 1) % AnimationFrames.Count;
            CurrentAnimationFrame = AnimationFrames[frameIndex];
        }
    }
}
