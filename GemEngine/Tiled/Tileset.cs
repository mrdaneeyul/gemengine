﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace GemEngine.Tiled
{
    public class Tileset
    {
        public string Name => Texture.Name;
        public Texture2D Texture { get; }
        public int FirstGlobalID { get; }
        public int TileWidth { get; }
        public int TileHeight { get; }
        public int Spacing { get; }
        public int Margin { get; }
        public int TileCount { get; }
        public int Columns { get; }
        public ReadOnlyCollection<TilesetTile> Tiles { get; private set; }
        public ReadOnlyCollection<TilesetAnimatedTile> AnimatedTiles { get; private set; }
        public TiledMapProperties Properties { get; }

        private readonly Dictionary<int, TilesetAnimatedTile> animatedTilesByLocalID;

        internal Tileset(ContentReader input)
        {
            var textureAssetName = input.GetRelativeAssetName(input.ReadString());
            var animatedTiles = new List<TilesetAnimatedTile>();
            var tiles = new List<TilesetTile>();

            animatedTilesByLocalID = new Dictionary<int, TilesetAnimatedTile>();

            Texture = input.ContentManager.Load<Texture2D>(textureAssetName);
            FirstGlobalID = input.ReadInt32();
            TileWidth = input.ReadInt32();
            TileHeight = input.ReadInt32();
            TileCount = input.ReadInt32();
            Spacing = input.ReadInt32();
            Margin = input.ReadInt32();
            Columns = input.ReadInt32();
            Properties = new TiledMapProperties();
            
            var explicitTileCount = input.ReadInt32();

            for (var tileIndex = 0; tileIndex < explicitTileCount; tileIndex++)
            {
                var localTileID = input.ReadInt32();
                var animationFramesCount = input.ReadInt32();

                TilesetTile tilesetTile;
                if (animationFramesCount <= 0)
                    tilesetTile = new TilesetTile(localTileID, input);
                else
                {
                    var animatedTilesetTile = new TilesetAnimatedTile(this, input, localTileID, animationFramesCount);
                    animatedTiles.Add(animatedTilesetTile);
                    animatedTilesByLocalID.Add(localTileID, animatedTilesetTile);
                    tilesetTile = animatedTilesetTile;
                }
                tiles.Add(tilesetTile);

                input.ReadTiledMapProperties(tilesetTile.Properties);
            }
            input.ReadTiledMapProperties(Properties);
            Tiles = new ReadOnlyCollection<TilesetTile>(tiles);
            AnimatedTiles = new ReadOnlyCollection<TilesetAnimatedTile>(animatedTiles);
        }

        public Rectangle GetTileRegion(int localTileID)
        {
            return TiledMapHelper.GetTileSourceRectangle(localTileID, TileWidth, TileHeight, Columns, Margin, Spacing);
        }

        public TilesetAnimatedTile GetAnimatedTilesetTileByLocalID(int localTileID)
        {
            TilesetAnimatedTile animatedTile;
            animatedTilesByLocalID.TryGetValue(localTileID, out animatedTile);
            return animatedTile;
        }

        public bool ContainsGlobalID(int globalID) => globalID >= FirstGlobalID && globalID < FirstGlobalID + TileCount;
    }
}
