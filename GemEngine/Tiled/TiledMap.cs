﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace GemEngine.Tiled
{
    /// <summary>
    /// A class representing a Tiled map and all its associated layers
    /// Based on the Monogame.Extended class
    /// </summary>
    public sealed class TiledMap : IDisposable
    {
        private readonly List<Layer> layers;
        private readonly Dictionary<string, Layer> layersByName;
        private readonly List<Tileset> tilesets;
        private readonly List<ImageLayer> imageLayers;
        private readonly List<TileLayer> tileLayers;
        private readonly List<ObjectLayer> objectLayers;

        public string Name { get; }
        public int Width { get; }
        public int Height { get; }
        public int TileWidth { get; }
        public int TileHeight { get; }
        public TileDrawOrderType RenderOrder { get; } //TODO: May not need this since it looks like it's for isometric
        public OrientationType Orientation { get; } //TODO: May not need this either since we're only orthagonal
        public TiledMapProperties Properties { get; }
        public ReadOnlyCollection<Layer> Layers { get; }
        public ReadOnlyCollection<Tileset> Tilesets { get; }
        public ReadOnlyCollection<ImageLayer> ImageLayers { get; }
        public ReadOnlyCollection<TileLayer> TileLayers { get; }
        public ReadOnlyCollection<ObjectLayer> ObjectLayers { get; }

        public Color? BackgroundColor { get; set; }

        public int WidthInPixels => Width * TileWidth;
        public int HeightInPixels => Height * TileHeight;

        private TiledMap()
        {
            layers = new List<Layer>();
            Layers = new ReadOnlyCollection<Layer>(layers);
            imageLayers = new List<ImageLayer>();
            ImageLayers = new ReadOnlyCollection<ImageLayer>(imageLayers);
            tileLayers = new List<TileLayer>();
            TileLayers = new ReadOnlyCollection<TileLayer>(tileLayers);
            objectLayers = new List<ObjectLayer>();
            ObjectLayers = new ReadOnlyCollection<ObjectLayer>(objectLayers);
            tilesets = new List<Tileset>();
            Tilesets = new ReadOnlyCollection<Tileset>(tilesets);

            layersByName = new Dictionary<string, Layer>();
            Properties = new TiledMapProperties();
        }

        public TiledMap(string name, int width, int height, int tileWidth, int tileHeight, TileDrawOrderType renderOrder, OrientationType orientation) : this()
        {
            Name = name;
            Width = width;
            Height = height;
            TileWidth = tileWidth;
            TileHeight = tileHeight;
            RenderOrder = renderOrder;
            Orientation = orientation;
        }

        public void Dispose()
        {
            foreach (var layer in layers)
                layer.Dispose();
        }

        internal void AddTileset(Tileset tileset)
        {
            tilesets.Add(tileset);
        }

        public void AddLayer(Layer layer)
        {
            layers.Add(layer);
            layersByName.Add(layer.Name, layer);

            if (layer is ImageLayer)
                imageLayers.Add((ImageLayer)layer);

            if (layer is TileLayer)
                tileLayers.Add((TileLayer)layer);

            if (layer is ObjectLayer)
                objectLayers.Add((ObjectLayer)layer);
        }

        public Layer GetLayer(string layerName)
        {
            Layer layer;
            layersByName.TryGetValue(layerName, out layer);
            return layer;
        }

        public T GetLayer<T>(string layerName) where T : Layer
        {
            return GetLayer(layerName) as T;
        }

        public Tileset GetTilesetByTileGlobalID(int tileID)
        {
            return tilesets.FirstOrDefault(t => t.ContainsGlobalID(tileID)) ?? null;
        }
    }
}
