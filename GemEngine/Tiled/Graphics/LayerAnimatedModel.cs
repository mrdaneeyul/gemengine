﻿using Microsoft.Xna.Framework.Content;

namespace GemEngine.Tiled.Graphics
{
    public sealed class LayerAnimatedModel : LayerModel
    {
        public TilesetAnimatedTile[] AnimatedTilesetTiles { get; }

        internal LayerAnimatedModel(ContentReader reader, TiledMap map) : base(reader, true)
        {
            var tilesetFirstGlobalID = reader.ReadInt32();
            var tileset = map.GetTilesetByTileGlobalID(tilesetFirstGlobalID);
            var animatedTilesetTileCount = reader.ReadInt32();
            AnimatedTilesetTiles = new TilesetAnimatedTile[animatedTilesetTileCount];

            for (var i = 0; i < animatedTilesetTileCount; i++)
            {
                var tileLocalID = reader.ReadInt32();
                AnimatedTilesetTiles[i] = tileset.GetAnimatedTilesetTileByLocalID(tileLocalID);
            }
        }
    }
}
