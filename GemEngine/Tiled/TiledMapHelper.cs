﻿using GemEngine.Utility;
using Microsoft.Xna.Framework;

namespace GemEngine.Tiled
{
    public static class TiledMapHelper
    {
        //There are 4 vertices per tile...
        public const int VERTICES_PER_TILE = 4;
        //and 2 triangles per tile (mesh), with each triangle indexing 3 out of 4 vertices, so 6 vertices
        public const int INDICES_PER_TILE = 6;         
        //By using ushort type for indices we are limited to indexing vertices from 0 to 65535
        //this limits us on how many vertices can fit inside a single vertex buffer (65536 vertices) 
        public const int MAXIMUM_VERTICES_PER_MODEL = ushort.MaxValue + 1;
        //...and thus, we know how many tiles we can fit inside a vertex or index buffer (16384 tiles)
        public const int MAXIMUM_TILES_PER_GEOMETRY_CONTENT = MAXIMUM_VERTICES_PER_MODEL / VERTICES_PER_TILE;
        // and thus, we also know the maximum number of indices we can fit inside a single index buffer (98304 indices)
        public const int MAXIMUM_INDICES_PER_MODEL = MAXIMUM_TILES_PER_GEOMETRY_CONTENT * INDICES_PER_TILE;
        // these optimal maximum numbers of course are not considering texture bindings which would practically lower the actual number of tiles per vertex / index buffer
        // thus, the reason why it is a good to have ONE giant tileset (at least per layer)

        internal static Rectangle GetTileSourceRectangle(int localTileID, int tileWidth, int tileHeight, int columns, int margin, int spacing)
        {
            var x = margin + localTileID % columns * (tileWidth + spacing);
            var y = margin + localTileID / columns * (tileHeight + spacing);
            return new Rectangle(x, y, tileWidth, tileHeight);
        }

        internal static Point2 GetOrthogonalPosition(int tileX, int tileY, int tileWidth, int tileHeight)
        {
            var x = tileX * tileWidth;
            var y = tileY * tileHeight;
            return new Vector2(x, y);
        }
    }
}
