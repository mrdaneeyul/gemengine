﻿namespace GemEngine.Tiled
{
    public enum TiledObjectType : byte
    {
        Rectangle,
        Ellipse,
        Polygon,
        Polyline,
        Tile
    }
}
