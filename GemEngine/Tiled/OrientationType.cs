﻿namespace GemEngine.Tiled
{
    public enum OrientationType //TODO: May not need this since we're always orthagonal
    {
        Orthogonal,
        Isometric,
        Staggered
    }
}
