﻿using System.Linq;

namespace GemEngine.Tiled
{
    public struct Tile
    {
        internal readonly uint GlobalTileIDWithFlags;

        public int GlobalID => (int)(GlobalTileIDWithFlags & ~(uint)TileFlipFlags.All);
        public bool IsFlippedHorizontally => (GlobalTileIDWithFlags & (uint)TileFlipFlags.FlipHorizontally) != 0;
        public bool IsFlippedVertically => (GlobalTileIDWithFlags & (uint)TileFlipFlags.FlipVertically) != 0;
        public bool IsFlippedDiagonally => (GlobalTileIDWithFlags & (uint)TileFlipFlags.FlipDiagonally) != 0;
        public bool IsBlank => GlobalID == 0;
        public TileFlipFlags Flags => (TileFlipFlags)(GlobalTileIDWithFlags & (uint)TileFlipFlags.All);

        internal Tile(uint globalTileIDWithFlags)
        {
            GlobalTileIDWithFlags = globalTileIDWithFlags;
        }

        public override string ToString() => $"GlobalIdentifier: {GlobalID}, Flags: {Flags}";
    }
}
