﻿using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;
using System.Linq;

namespace GemEngine.Input
{
    /// <summary>
    /// A virtual input that is represented as a boolean. As well as simply checking the current button state, you can ask whether it was just pressed or released this frame. You can also keep the button press stored in a buffer for a limited time, or until it is consumed by calling ConsumeBuffer()
    /// </summary>
    public class VirtualButton : VirtualInput
    {
        public List<Node> Nodes;
        public float BufferTime;
        public bool IsRepeating { get; private set; }

        private float firstRepeatTime;
        private float multiRepeatTime;
        private float bufferCounter;
        private float repeatCounter;
        private bool canRepeat;
        private bool isConsumed;

        public VirtualButton() : this(0) { }

        public VirtualButton(params Node[] nodes) : this(0, nodes) { }

        public VirtualButton(float bufferTime) : base()
        {
            Nodes = new List<Node>();
            BufferTime = bufferTime;
        }

        public VirtualButton(float bufferTime, params Node[] nodes) : base()
        {
            Nodes = new List<Node>(nodes);
            BufferTime = bufferTime;
        }

        public void SetRepeat(float repeatTime)
        {
            SetRepeat(repeatTime, repeatTime);
        }

        public void SetRepeat(float firstRepeatTime, float multiRepeatTime)
        {
            this.firstRepeatTime = firstRepeatTime;
            this.multiRepeatTime = multiRepeatTime;
            canRepeat = (firstRepeatTime > 0);
            if (!canRepeat)
                IsRepeating = false;
        }

        public override void Update()
        {
            isConsumed = false;
            bufferCounter -= Engine.DeltaTime;

            var check = false;
            foreach (var node in Nodes)
            {
                node.Update();
                if (node.IsPressed)
                {
                    bufferCounter = BufferTime;
                    check = true;
                }
                else if (node.Check)
                    check = true;
            }

            if (!check)
            {
                IsRepeating = false;
                repeatCounter = 0;
                bufferCounter = 0;
            }
            else if (canRepeat)
            {
                IsRepeating = false;
                if (repeatCounter == 0)
                    repeatCounter = firstRepeatTime;
                else
                {
                    repeatCounter -= Engine.DeltaTime;
                    if (repeatCounter <= 0)
                    {
                        IsRepeating = true;
                        repeatCounter = multiRepeatTime;
                    }
                }
            }
        }

        public bool Check
        {
            get
            {
                if (InputManager.IsDisabled)
                    return false;

                if (Nodes.Any(n => n.Check))
                    return true;
                return false;
            }
        }

        public bool IsPressed
        {
            get
            {
                if (InputManager.IsDisabled || isConsumed)
                    return false;
                if (bufferCounter > 0 || IsRepeating || Nodes.Any(n => n.IsPressed))
                    return true;
                return false;
            }
        }

        public bool IsReleased
        {
            get
            {
                if (InputManager.IsDisabled)
                    return false;
                if (Nodes.Any(n => n.IsReleased))
                    return true;
                return false;
            }
        }

        /// <summary>
        /// Ends the Press buffer for this button
        /// </summary>
        public void ConsumeBuffer()
        {
            bufferCounter = 0;
        }

        /// <summary>
        /// This button will not register a Press for the rest of the current frame, but otherwise continues to function normally. If the player continues to hold the button, next frame will not count as a Press. Also ends the Press buffer for this button
        /// </summary>
        public void ConsumePress()
        {
            bufferCounter = 0;
            isConsumed = true;
        }

        public static implicit operator bool(VirtualButton button)
        {
            return button.Check;
        }

        public abstract class Node : VirtualInputNode
        {
            public abstract bool Check { get; }
            public abstract bool IsPressed { get; }
            public abstract bool IsReleased { get; }
        }

        public class KeyboardKey : Node
        {
            public Keys Key;

            public KeyboardKey(Keys key)
            {
                Key = key;
            }

            public override bool Check => InputManager.Keyboard.Check(Key);
            public override bool IsPressed => InputManager.Keyboard.IsPressed(Key);
            public override bool IsReleased => InputManager.Keyboard.IsReleased(Key);
        }

        public class PadButton : Node
        {
            public int GamePadIndex;
            public Buttons Button;

            public PadButton(int gamePadIndex, Buttons button)
            {
                GamePadIndex = gamePadIndex;
                Button = button;
            }

            public override bool Check => InputManager.GamePads[GamePadIndex].Check(Button);
            public override bool IsPressed => InputManager.GamePads[GamePadIndex].IsPressed(Button);
            public override bool IsReleased => InputManager.GamePads[GamePadIndex].IsReleased(Button);
        }

        #region Pad left stick
        public class PadLeftStickRight : Node
        {
            public int GamePadIndex;
            public float Deadzone;

            public PadLeftStickRight(int gamePadIndex, float deadzone)
            {
                GamePadIndex = gamePadIndex;
                Deadzone = deadzone;
            }

            public override bool Check => InputManager.GamePads[GamePadIndex].LeftStickRightCheck(Deadzone);
            public override bool IsPressed => InputManager.GamePads[GamePadIndex].LeftStickRightPressed(Deadzone);
            public override bool IsReleased => InputManager.GamePads[GamePadIndex].LeftStickRightReleased(Deadzone);
        }

        public class PadLeftStickLeft : Node
        {
            public int GamePadIndex;
            public float Deadzone;

            public PadLeftStickLeft(int gamePadIndex, float deadzone)
            {
                GamePadIndex = gamePadIndex;
                Deadzone = deadzone;
            }

            public override bool Check => InputManager.GamePads[GamePadIndex].LeftStickLeftCheck(Deadzone);
            public override bool IsPressed => InputManager.GamePads[GamePadIndex].LeftStickLeftPressed(Deadzone);
            public override bool IsReleased => InputManager.GamePads[GamePadIndex].LeftStickLeftReleased(Deadzone);
        }

        public class PadLeftStickUp : Node
        {
            public int GamePadIndex;
            public float Deadzone;

            public PadLeftStickUp(int gamePadIndex, float deadzone)
            {
                GamePadIndex = gamePadIndex;
                Deadzone = deadzone;
            }

            public override bool Check => InputManager.GamePads[GamePadIndex].LeftStickUpCheck(Deadzone);
            public override bool IsPressed => InputManager.GamePads[GamePadIndex].LeftStickUpPressed(Deadzone);
            public override bool IsReleased => InputManager.GamePads[GamePadIndex].LeftStickUpReleased(Deadzone);
        }

        public class PadLeftStickDown : Node
        {
            public int GamePadIndex;
            public float Deadzone;

            public PadLeftStickDown(int gamePadIndex, float deadzone)
            {
                GamePadIndex = gamePadIndex;
                Deadzone = deadzone;
            }

            public override bool Check => InputManager.GamePads[GamePadIndex].LeftStickDownCheck(Deadzone);
            public override bool IsPressed => InputManager.GamePads[GamePadIndex].LeftStickDownPressed(Deadzone);
            public override bool IsReleased => InputManager.GamePads[GamePadIndex].LeftStickDownReleased(Deadzone);
        }
        #endregion Pad left stick

        #region Pad right stick
        public class PadRightStickRight : Node
        {
            public int GamePadIndex;
            public float Deadzone;

            public PadRightStickRight(int gamePadIndex, float deadzone)
            {
                GamePadIndex = gamePadIndex;
                Deadzone = deadzone;
            }

            public override bool Check
            {
                get { return InputManager.GamePads[GamePadIndex].RightStickRightCheck(Deadzone); }
            }

            public override bool IsPressed
            {
                get { return InputManager.GamePads[GamePadIndex].RightStickRightPressed(Deadzone); }
            }

            public override bool IsReleased => InputManager.GamePads[GamePadIndex].RightStickRightReleased(Deadzone);
        }

        public class PadRightStickLeft : Node
        {
            public int GamePadIndex;
            public float Deadzone;

            public PadRightStickLeft(int gamePadIndex, float deadzone)
            {
                GamePadIndex = gamePadIndex;
                Deadzone = deadzone;
            }

            public override bool Check => InputManager.GamePads[GamePadIndex].RightStickLeftCheck(Deadzone);
            public override bool IsPressed => InputManager.GamePads[GamePadIndex].RightStickLeftPressed(Deadzone);
            public override bool IsReleased => InputManager.GamePads[GamePadIndex].RightStickLeftReleased(Deadzone);
        }

        public class PadRightStickUp : Node
        {
            public int GamePadIndex;
            public float Deadzone;

            public PadRightStickUp(int gamePadIndex, float deadzone)
            {
                GamePadIndex = gamePadIndex;
                Deadzone = deadzone;
            }

            public override bool Check => InputManager.GamePads[GamePadIndex].RightStickUpCheck(Deadzone);
            public override bool IsPressed => InputManager.GamePads[GamePadIndex].RightStickUpPressed(Deadzone);
            public override bool IsReleased => InputManager.GamePads[GamePadIndex].RightStickUpReleased(Deadzone);
        }

        public class PadRightStickDown : Node
        {
            public int GamePadIndex;
            public float Deadzone;

            public PadRightStickDown(int gamePadIndex, float deadzone)
            {
                GamePadIndex = gamePadIndex;
                Deadzone = deadzone;
            }

            public override bool Check => InputManager.GamePads[GamePadIndex].RightStickDownCheck(Deadzone);
            public override bool IsPressed => InputManager.GamePads[GamePadIndex].RightStickDownPressed(Deadzone);
            public override bool IsReleased => InputManager.GamePads[GamePadIndex].RightStickDownReleased(Deadzone);
        }
        #endregion Pad right stick

        #region Pad triggers
        public class PadLeftTrigger : Node
        {
            public int GamePadIndex;
            public float Threshold;

            public PadLeftTrigger(int gamePadIndex, float threshold)
            {
                GamePadIndex = gamePadIndex;
                Threshold = threshold;
            }

            public override bool Check => InputManager.GamePads[GamePadIndex].LeftTriggerCheck(Threshold);
            public override bool IsPressed => InputManager.GamePads[GamePadIndex].LeftTriggerPressed(Threshold);
            public override bool IsReleased => InputManager.GamePads[GamePadIndex].LeftTriggerReleased(Threshold);
        }

        public class PadRightTrigger : Node
        {
            public int GamePadIndex;
            public float Threshold;

            public PadRightTrigger(int gamePadIndex, float threshold)
            {
                GamePadIndex = gamePadIndex;
                Threshold = threshold;
            }

            public override bool Check => InputManager.GamePads[GamePadIndex].RightTriggerCheck(Threshold);
            public override bool IsPressed => InputManager.GamePads[GamePadIndex].RightTriggerPressed(Threshold);
            public override bool IsReleased => InputManager.GamePads[GamePadIndex].RightTriggerReleased(Threshold);
        }
        #endregion Pad triggers

        #region Pad dpad
        public class PadDPadRight : Node
        {
            public int GamePadIndex;

            public PadDPadRight(int gamePadIndex)
            {
                GamePadIndex = gamePadIndex;
            }

            public override bool Check => InputManager.GamePads[GamePadIndex].DPadRightCheck;
            public override bool IsPressed => InputManager.GamePads[GamePadIndex].DPadRightPressed;
            public override bool IsReleased => InputManager.GamePads[GamePadIndex].DPadRightReleased;
        }

        public class PadDPadLeft : Node
        {
            public int GamePadIndex;

            public PadDPadLeft(int gamePadIndex)
            {
                GamePadIndex = gamePadIndex;
            }

            public override bool Check => InputManager.GamePads[GamePadIndex].DPadLeftCheck;
            public override bool IsPressed => InputManager.GamePads[GamePadIndex].DPadLeftPressed;
            public override bool IsReleased => InputManager.GamePads[GamePadIndex].DPadLeftReleased;
        }

        public class PadDPadUp : Node
        {
            public int GamePadIndex;

            public PadDPadUp(int gamePadIndex)
            {
                GamePadIndex = gamePadIndex;
            }

            public override bool Check => InputManager.GamePads[GamePadIndex].DPadUpCheck;
            public override bool IsPressed => InputManager.GamePads[GamePadIndex].DPadUpPressed;
            public override bool IsReleased => InputManager.GamePads[GamePadIndex].DPadUpReleased;
        }

        public class PadDPadDown : Node
        {
            public int GamePadIndex;

            public PadDPadDown(int gamePadIndex)
            {
                GamePadIndex = gamePadIndex;
            }

            public override bool Check => InputManager.GamePads[GamePadIndex].DPadDownCheck;
            public override bool IsPressed => InputManager.GamePads[GamePadIndex].DPadDownPressed;
            public override bool IsReleased => InputManager.GamePads[GamePadIndex].DPadDownReleased;
        }
        #endregion Pad dpad

        #region Mouse
        public class MouseLeftButton : Node
        {
            public override bool Check => InputManager.Mouse.CheckLeftButton;
            public override bool IsPressed => InputManager.Mouse.PressedLeftButton;
            public override bool IsReleased => InputManager.Mouse.ReleasedLeftButton;
        }

        public class MouseRightButton : Node
        {
            public override bool Check => InputManager.Mouse.CheckRightButton;
            public override bool IsPressed => InputManager.Mouse.PressedRightButton;
            public override bool IsReleased => InputManager.Mouse.ReleasedRightButton;
        }

        public class MouseMiddleButton : Node
        {
            public override bool Check => InputManager.Mouse.CheckMiddleButton;
            public override bool IsPressed => InputManager.Mouse.PressedMiddleButton;
            public override bool IsReleased => InputManager.Mouse.ReleasedMiddleButton;
        }
        #endregion Mouse

        #region Other virtual inputs
        public class VirtualAxisTrigger : Node
        {
            public enum Modes { LargerThan, LessThan, Equals };

            public ThresholdModes Mode;
            public float Threshold;

            private VirtualAxis axis;

            public VirtualAxisTrigger(VirtualAxis axis, ThresholdModes mode, float threshold)
            {
                this.axis = axis;
                Mode = mode;
                Threshold = threshold;
            }

            public override bool Check
            {
                get
                {
                    if (Mode == ThresholdModes.LargerThan)
                        return axis.Value >= Threshold;
                    else if (Mode == ThresholdModes.LessThan)
                        return axis.Value <= Threshold;
                    else
                        return axis.Value == Threshold;
                }
            }

            public override bool IsPressed
            {
                get
                {
                    if (Mode == ThresholdModes.LargerThan)
                        return axis.Value >= Threshold && axis.PreviousValue < Threshold;
                    else if (Mode == ThresholdModes.LessThan)
                        return axis.Value <= Threshold && axis.PreviousValue > Threshold;
                    else
                        return axis.Value == Threshold && axis.PreviousValue != Threshold;
                }
            }

            public override bool IsReleased
            {
                get
                {
                    if (Mode == ThresholdModes.LargerThan)
                        return axis.Value < Threshold && axis.PreviousValue >= Threshold;
                    else if (Mode == ThresholdModes.LessThan)
                        return axis.Value > Threshold && axis.PreviousValue <= Threshold;
                    else
                        return axis.Value != Threshold && axis.PreviousValue == Threshold;
                }
            }
        }

        public class VirtualIntegerAxisTrigger : Node
        {
            public enum Modes { LargerThan, LessThan, Equals };

            public ThresholdModes Mode;
            public int Threshold;

            private VirtualIntegerAxis axis;

            public VirtualIntegerAxisTrigger(VirtualIntegerAxis axis, ThresholdModes mode, int threshold)
            {
                this.axis = axis;
                Mode = mode;
                Threshold = threshold;
            }

            public override bool Check
            {
                get
                {
                    if (Mode == ThresholdModes.LargerThan)
                        return axis.Value >= Threshold;
                    else if (Mode == ThresholdModes.LessThan)
                        return axis.Value <= Threshold;
                    else
                        return axis.Value == Threshold;
                }
            }

            public override bool IsPressed
            {
                get
                {
                    if (Mode == ThresholdModes.LargerThan)
                        return axis.Value >= Threshold && axis.PreviousValue < Threshold;
                    else if (Mode == ThresholdModes.LessThan)
                        return axis.Value <= Threshold && axis.PreviousValue > Threshold;
                    else
                        return axis.Value == Threshold && axis.PreviousValue != Threshold;
                }
            }

            public override bool IsReleased
            {
                get
                {
                    if (Mode == ThresholdModes.LargerThan)
                        return axis.Value < Threshold && axis.PreviousValue >= Threshold;
                    else if (Mode == ThresholdModes.LessThan)
                        return axis.Value > Threshold && axis.PreviousValue <= Threshold;
                    else
                        return axis.Value != Threshold && axis.PreviousValue == Threshold;
                }
            }
        }

        public class VirtualJoystickXTrigger : Node
        {
            public enum Modes { LargerThan, LessThan, Equals };

            public ThresholdModes Mode;
            public float Threshold;

            private VirtualJoystick joystick;

            public VirtualJoystickXTrigger(VirtualJoystick joystick, ThresholdModes mode, float threshold)
            {
                this.joystick = joystick;
                Mode = mode;
                Threshold = threshold;
            }

            public override bool Check
            {
                get
                {
                    if (Mode == ThresholdModes.LargerThan)
                        return joystick.Value.X >= Threshold;
                    else if (Mode == ThresholdModes.LessThan)
                        return joystick.Value.X <= Threshold;
                    else
                        return joystick.Value.X == Threshold;
                }
            }

            public override bool IsPressed
            {
                get
                {
                    if (Mode == ThresholdModes.LargerThan)
                        return joystick.Value.X >= Threshold && joystick.PreviousValue.X < Threshold;
                    else if (Mode == ThresholdModes.LessThan)
                        return joystick.Value.X <= Threshold && joystick.PreviousValue.X > Threshold;
                    else
                        return joystick.Value.X == Threshold && joystick.PreviousValue.X != Threshold;
                }
            }

            public override bool IsReleased
            {
                get
                {
                    if (Mode == ThresholdModes.LargerThan)
                        return joystick.Value.X < Threshold && joystick.PreviousValue.X >= Threshold;
                    else if (Mode == ThresholdModes.LessThan)
                        return joystick.Value.X > Threshold && joystick.PreviousValue.X <= Threshold;
                    else
                        return joystick.Value.X != Threshold && joystick.PreviousValue.X == Threshold;
                }
            }
        }

        public class VirtualJoystickYTrigger : Node
        {
            public ThresholdModes Mode;
            public float Threshold;

            private VirtualJoystick joystick;

            public VirtualJoystickYTrigger(VirtualJoystick joystick, ThresholdModes mode, float threshold)
            {
                this.joystick = joystick;
                Mode = mode;
                Threshold = threshold;
            }

            public override bool Check
            {
                get
                {
                    if (Mode == ThresholdModes.LargerThan)
                        return joystick.Value.X >= Threshold;
                    else if (Mode == ThresholdModes.LessThan)
                        return joystick.Value.X <= Threshold;
                    else
                        return joystick.Value.X == Threshold;
                }
            }

            public override bool IsPressed
            {
                get
                {
                    if (Mode == ThresholdModes.LargerThan)
                        return joystick.Value.X >= Threshold && joystick.PreviousValue.X < Threshold;
                    else if (Mode == ThresholdModes.LessThan)
                        return joystick.Value.X <= Threshold && joystick.PreviousValue.X > Threshold;
                    else
                        return joystick.Value.X == Threshold && joystick.PreviousValue.X != Threshold;
                }
            }

            public override bool IsReleased
            {
                get
                {
                    if (Mode == ThresholdModes.LargerThan)
                        return joystick.Value.X < Threshold && joystick.PreviousValue.X >= Threshold;
                    else if (Mode == ThresholdModes.LessThan)
                        return joystick.Value.X > Threshold && joystick.PreviousValue.X <= Threshold;
                    else
                        return joystick.Value.X != Threshold && joystick.PreviousValue.X == Threshold;
                }
            }
        }
        #endregion Other virtual inputs
    }
}
