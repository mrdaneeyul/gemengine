﻿using Microsoft.Xna.Framework.Input;

namespace GemEngine.Input
{

    public class KeyboardData
    {
        public KeyboardState PreviousState;
        public KeyboardState CurrentState;

        internal KeyboardData() { }

        internal void Update()
        {
            PreviousState = CurrentState;
            CurrentState = Keyboard.GetState();
        }

        internal void UpdateNull()
        {
            PreviousState = CurrentState;
            CurrentState = new KeyboardState();
        }

        #region Basic checks
        public bool Check(Keys key)
        {
            if (InputManager.IsDisabled) return false;
            return CurrentState.IsKeyDown(key);
        }

        public bool IsPressed(Keys key)
        {
            if (InputManager.IsDisabled) return false;
            return CurrentState.IsKeyDown(key) && !PreviousState.IsKeyDown(key);
        }

        public bool IsReleased(Keys key)
        {
            if (InputManager.IsDisabled) return false;
            return !CurrentState.IsKeyDown(key) && PreviousState.IsKeyDown(key);
        }
        #endregion Basic checks

        #region Convenience checks
        public bool Check(Keys keyA, Keys keyB)
        {
            return Check(keyA) || Check(keyB);
        }

        public bool Check(Keys keyA, Keys keyB, Keys keyC)
        {
            return Check(keyA) || Check(keyB) || Check(keyC);
        }

        public bool IsPressed(Keys keyA, Keys keyB)
        {
            return IsPressed(keyA) || IsPressed(keyB);
        }

        public bool IsPressed(Keys keyA, Keys keyB, Keys keyC)
        {
            return IsPressed(keyA) || IsPressed(keyB) || IsPressed(keyC);
        }

        public bool IsReleased(Keys keyA, Keys keyB)
        {
            return IsReleased(keyA) || IsReleased(keyB);
        }

        public bool IsReleased(Keys keyA, Keys keyB, Keys keyC)
        {
            return IsReleased(keyA) || IsReleased(keyB) || IsReleased(keyC);
        }
        #endregion Convenience checks

        #region Axis
        public int AxisCheck(Keys negative, Keys positive)
        {
            if (Check(negative))
            {
                if (Check(positive))
                    return 0;
                else
                    return -1;
            }
            else if (Check(positive))
                return 1;
            else
                return 0;
        }

        public int AxisCheck(Keys negative, Keys positive, int both)
        {
            if (Check(negative))
            {
                if (Check(positive))
                    return both;
                else
                    return -1;
            }
            else if (Check(positive))
                return 1;
            else
                return 0;
        }
        #endregion Axis
    }
}
