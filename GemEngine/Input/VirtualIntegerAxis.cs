﻿using System;
using System.Collections.Generic;

namespace GemEngine.Input
{
    /// <summary>
    /// A virtual input that is represented as a int that is either -1, 0, or 1
    /// </summary>
    public class VirtualIntegerAxis : VirtualAxis
    {
        public new List<Node> Nodes;

        public new int Value;
        public new int PreviousValue { get; private set; }

        public VirtualIntegerAxis() : base()
        {
            Nodes = new List<Node>();
        }

        public VirtualIntegerAxis(params Node[] nodes) : base()
        {
            Nodes = new List<Node>(nodes);
        }

        public override void Update()
        {
            foreach (var node in Nodes)
                node.Update();

            PreviousValue = Value;
            Value = 0;
            foreach (var node in Nodes)
            {
                var value = node.Value;
                if (value != 0)
                {
                    Value = Math.Sign(value);
                    break;
                }
            }
        }

        public static implicit operator int(VirtualIntegerAxis axis)
        {
            return axis.Value;
        }
    }
}
