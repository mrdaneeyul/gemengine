﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;

namespace GemEngine.Input
{
    public class MouseData
    {
        public MouseState PreviousState;
        public MouseState CurrentState;

        internal MouseData()
        {
            PreviousState = new MouseState();
            CurrentState = new MouseState();
        }

        internal void Update()
        {
            PreviousState = CurrentState;
            CurrentState = Mouse.GetState();
        }

        internal void UpdateNull()
        {
            PreviousState = CurrentState;
            CurrentState = new MouseState();
        }

        #region Buttons
        public bool CheckLeftButton => CurrentState.LeftButton == ButtonState.Pressed;
        public bool CheckRightButton => CurrentState.RightButton == ButtonState.Pressed;
        public bool CheckMiddleButton => CurrentState.MiddleButton == ButtonState.Pressed;
        public bool PressedLeftButton => CurrentState.LeftButton == ButtonState.Pressed && PreviousState.LeftButton == ButtonState.Released;
        public bool PressedRightButton => CurrentState.RightButton == ButtonState.Pressed && PreviousState.RightButton == ButtonState.Released;
        public bool PressedMiddleButton => CurrentState.MiddleButton == ButtonState.Pressed && PreviousState.MiddleButton == ButtonState.Released;
        public bool ReleasedLeftButton => CurrentState.LeftButton == ButtonState.Released && PreviousState.LeftButton == ButtonState.Pressed;
        public bool ReleasedRightButton => CurrentState.RightButton == ButtonState.Released && PreviousState.RightButton == ButtonState.Pressed;
        public bool ReleasedMiddleButton => CurrentState.MiddleButton == ButtonState.Released && PreviousState.MiddleButton == ButtonState.Pressed;
        #endregion Buttons

        #region Wheel
        public int Wheel => CurrentState.ScrollWheelValue;
        public int WheelDelta => CurrentState.ScrollWheelValue - PreviousState.ScrollWheelValue;
        #endregion Wheel

        #region Position
        public bool WasMoved => CurrentState.X != PreviousState.X || CurrentState.Y != PreviousState.Y;

        public float X
        {
            get { return Position.X; }
            set { Position = new Vector2(value, Position.Y); }
        }

        public float Y
        {
            get { return Position.Y; }
            set { Position = new Vector2(Position.X, value); }
        }

        public Vector2 Position
        {
            get { return Vector2.Transform(new Vector2(CurrentState.X, CurrentState.Y), Matrix.Invert(Engine.ScreenMatrix)); }
            set
            {
                var vector = Vector2.Transform(value, Engine.ScreenMatrix);
                Mouse.SetPosition((int)Math.Round(vector.X), (int)Math.Round(vector.Y));
            }
        }
        #endregion Position
    }
}
