﻿using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace GemEngine.Input
{
    public static class InputManager
    {
        public static KeyboardData Keyboard { get; private set; }
        public static MouseData Mouse { get; private set; }
        public static GamePadData[] GamePads { get; private set; } //TODO: May not need an array of gamepads
        public static bool IsActive = true;
        public static bool IsDisabled = false;

        internal static List<VirtualInput> VirtualInputs;

        internal static void Initialize()
        {
            Keyboard = new KeyboardData();
            Mouse = new MouseData();
            GamePads = new GamePadData[4];
            for (var i = 0; i < 4; i++)
                GamePads[i] = new GamePadData((PlayerIndex)i); //TODO: Probably don't need this for single player
            VirtualInputs = new List<VirtualInput>();
        }

        internal static void Shutdown()
        {
            foreach (var gamepad in GamePads)
                gamepad.StopRumble();
        }

        internal static void Update()
        {
            if (Engine.Instance.IsActive && IsActive)
            {
                if (Engine.DebugConsole.IsOpen)
                {
                    Keyboard.UpdateNull();
                    Mouse.UpdateNull();
                }
                else
                {
                    Keyboard.Update();
                    Mouse.Update();
                }
                for (var i = 0; i < 4; i++)
                    GamePads[i].Update();
            }
            else
            {
                Keyboard.UpdateNull();
                Mouse.UpdateNull();
                for (var i = 0; i < 4; i++)
                    GamePads[i].UpdateNull();
            }
            UpdateVirtualInputs();
        }

        public static void UpdateNull()
        {
            Keyboard.UpdateNull();
            Mouse.UpdateNull();
            for (var i = 0; i < 4; i++)
                GamePads[i].UpdateNull();
            UpdateVirtualInputs();
        }

        private static void UpdateVirtualInputs()
        {
            foreach (var virtualInput in VirtualInputs)
                virtualInput.Update();
        }
    }
}
