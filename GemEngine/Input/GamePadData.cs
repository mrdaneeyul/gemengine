﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;

namespace GemEngine.Input
{
    public class GamePadData
    {
        public PlayerIndex PlayerIndex { get; private set; }
        public GamePadState PreviousState;
        public GamePadState CurrentState;
        public bool IsAttached;

        private float rumbleStrength;
        private float rumbleTime;

        internal GamePadData(PlayerIndex playerIndex)
        {
            PlayerIndex = playerIndex;
        }

        public void Update()
        {
            PreviousState = CurrentState;
            CurrentState = GamePad.GetState(PlayerIndex);
            IsAttached = CurrentState.IsConnected;

            if (rumbleTime > 0)
            {
                rumbleTime -= Engine.DeltaTime;
                if (rumbleTime <= 0)
                    GamePad.SetVibration(PlayerIndex, 0, 0);
            }
        }

        public void UpdateNull()
        {
            PreviousState = CurrentState;
            CurrentState = new GamePadState();
            IsAttached = GamePad.GetState(PlayerIndex).IsConnected;

            if (rumbleTime > 0)
                rumbleTime -= Engine.DeltaTime;
            GamePad.SetVibration(PlayerIndex, 0, 0);
        }

        public void Rumble(float strength, float time)
        {
            if (rumbleTime <= 0 || strength > rumbleStrength || (strength == rumbleStrength && time > rumbleTime))
            {
                GamePad.SetVibration(PlayerIndex, strength, strength);
                rumbleStrength = strength;
                rumbleTime = time;
            }
        }

        public void StopRumble()
        {
            GamePad.SetVibration(PlayerIndex, 0, 0);
            rumbleTime = 0;
        }

        #region Buttons
        public bool Check(Buttons button)
        {
            if (InputManager.IsDisabled) return false;
            return CurrentState.IsButtonDown(button) && PreviousState.IsButtonUp(button);
        }

        public bool Check(Buttons buttonA, Buttons buttonB)
        {
            return Check(buttonA) || Check(buttonB);
        }

        public bool Check(Buttons buttonA, Buttons buttonB, Buttons buttonC)
        {
            return Check(buttonA) || Check(buttonB) || Check(buttonC);
        }

        public bool IsPressed(Buttons button)
        {
            if (InputManager.IsDisabled)
                return false;
            return CurrentState.IsButtonDown(button) && PreviousState.IsButtonUp(button);
        }

        public bool IsPressed(Buttons buttonA, Buttons buttonB)
        {
            return IsPressed(buttonA) || IsPressed(buttonB);
        }

        public bool IsPressed(Buttons buttonA, Buttons buttonB, Buttons buttonC)
        {
            return IsPressed(buttonA) || IsPressed(buttonB) || Check(buttonC);
        }

        public bool IsReleased(Buttons button)
        {
            if (InputManager.IsDisabled)
                return false;
            return CurrentState.IsButtonUp(button) && PreviousState.IsButtonDown(button);
        }

        public bool IsReleased(Buttons buttonA, Buttons buttonB)
        {
            return IsReleased(buttonA) || IsReleased(buttonB);
        }

        public bool IsReleased(Buttons buttonA, Buttons buttonB, Buttons buttonC)
        {
            return IsReleased(buttonA) || IsReleased(buttonB) || Check(buttonC);
        }

        #endregion Buttons

        #region Sticks
        public Vector2 GetLeftStick()
        {
            var ret = CurrentState.ThumbSticks.Left;
            ret.Y = -ret.Y;
            return ret;
        }

        public Vector2 GetLeftStick(float deadzone)
        {
            var ret = CurrentState.ThumbSticks.Left;
            if (ret.LengthSquared() < deadzone * deadzone)
                ret = Vector2.Zero;
            else
                ret.Y = -ret.Y;
            return ret;
        }

        public Vector2 GetRightStick()
        {
            var ret = CurrentState.ThumbSticks.Right;
            ret.Y = -ret.Y;
            return ret;
        }

        public Vector2 GetRightStick(float deadzone)
        {
            var ret = CurrentState.ThumbSticks.Right;
            if (ret.LengthSquared() < deadzone * deadzone)
                ret = Vector2.Zero;
            else
                ret.Y = -ret.Y;
            return ret;
        }

        #region Left stick directions
        public bool LeftStickLeftCheck(float deadzone)
        {
            return CurrentState.ThumbSticks.Left.X <= -deadzone;
        }

        public bool LeftStickLeftPressed(float deadzone)
        {
            return CurrentState.ThumbSticks.Left.X <= -deadzone && PreviousState.ThumbSticks.Left.X > -deadzone;
        }

        public bool LeftStickLeftReleased(float deadzone)
        {
            return CurrentState.ThumbSticks.Left.X > -deadzone && PreviousState.ThumbSticks.Left.X <= -deadzone;
        }

        public bool LeftStickRightCheck(float deadzone)
        {
            return CurrentState.ThumbSticks.Left.X >= deadzone;
        }

        public bool LeftStickRightPressed(float deadzone)
        {
            return CurrentState.ThumbSticks.Left.X >= deadzone && PreviousState.ThumbSticks.Left.X < deadzone;
        }

        public bool LeftStickRightReleased(float deadzone)
        {
            return CurrentState.ThumbSticks.Left.X < deadzone && PreviousState.ThumbSticks.Left.X >= deadzone;
        }

        public bool LeftStickDownCheck(float deadzone)
        {
            return CurrentState.ThumbSticks.Left.Y <= -deadzone;
        }

        public bool LeftStickDownPressed(float deadzone)
        {
            return CurrentState.ThumbSticks.Left.Y <= -deadzone && PreviousState.ThumbSticks.Left.Y > -deadzone;
        }

        public bool LeftStickDownReleased(float deadzone)
        {
            return CurrentState.ThumbSticks.Left.Y > -deadzone && PreviousState.ThumbSticks.Left.Y <= -deadzone;
        }

        public bool LeftStickUpCheck(float deadzone)
        {
            return CurrentState.ThumbSticks.Left.Y >= deadzone;
        }

        public bool LeftStickUpPressed(float deadzone)
        {
            return CurrentState.ThumbSticks.Left.Y >= deadzone && PreviousState.ThumbSticks.Left.Y < deadzone;
        }

        public bool LeftStickUpReleased(float deadzone)
        {
            return CurrentState.ThumbSticks.Left.Y < deadzone && PreviousState.ThumbSticks.Left.Y >= deadzone;
        }

        public float LeftStickHorizontal(float deadzone)
        {
            var h = CurrentState.ThumbSticks.Left.X;
            if (Math.Abs(h) < deadzone)
                return 0;
            else
                return h;
        }

        public float LeftStickVertical(float deadzone)
        {
            var v = CurrentState.ThumbSticks.Left.Y;
            if (Math.Abs(v) < deadzone)
                return 0;
            else
                return -v;
        }
        #endregion Left stick directions

        #region Right stick directions
        public bool RightStickLeftCheck(float deadzone)
        {
            return CurrentState.ThumbSticks.Right.X <= -deadzone;
        }

        public bool RightStickLeftPressed(float deadzone)
        {
            return CurrentState.ThumbSticks.Right.X <= -deadzone && PreviousState.ThumbSticks.Right.X > -deadzone;
        }

        public bool RightStickLeftReleased(float deadzone)
        {
            return CurrentState.ThumbSticks.Right.X > -deadzone && PreviousState.ThumbSticks.Right.X <= -deadzone;
        }

        public bool RightStickRightCheck(float deadzone)
        {
            return CurrentState.ThumbSticks.Right.X >= deadzone;
        }

        public bool RightStickRightPressed(float deadzone)
        {
            return CurrentState.ThumbSticks.Right.X >= deadzone && PreviousState.ThumbSticks.Right.X < deadzone;
        }

        public bool RightStickRightReleased(float deadzone)
        {
            return CurrentState.ThumbSticks.Right.X < deadzone && PreviousState.ThumbSticks.Right.X >= deadzone;
        }

        public bool RightStickUpCheck(float deadzone)
        {
            return CurrentState.ThumbSticks.Right.Y <= -deadzone;
        }

        public bool RightStickUpPressed(float deadzone)
        {
            return CurrentState.ThumbSticks.Right.Y <= -deadzone && PreviousState.ThumbSticks.Right.Y > -deadzone;
        }

        public bool RightStickUpReleased(float deadzone)
        {
            return CurrentState.ThumbSticks.Right.Y > -deadzone && PreviousState.ThumbSticks.Right.Y <= -deadzone;
        }

        public bool RightStickDownCheck(float deadzone)
        {
            return CurrentState.ThumbSticks.Right.Y >= deadzone;
        }

        public bool RightStickDownPressed(float deadzone)
        {
            return CurrentState.ThumbSticks.Right.Y >= deadzone && PreviousState.ThumbSticks.Right.Y < deadzone;
        }

        public bool RightStickDownReleased(float deadzone)
        {
            return CurrentState.ThumbSticks.Right.Y < deadzone && PreviousState.ThumbSticks.Right.Y >= deadzone;
        }

        public float RightStickHorizontal(float deadzone)
        {
            var h = CurrentState.ThumbSticks.Right.X;
            if (Math.Abs(h) < deadzone)
                return 0;
            else
                return h;
        }

        public float RightStickVertical(float deadzone)
        {
            var v = CurrentState.ThumbSticks.Right.Y;
            if (Math.Abs(v) < deadzone)
                return 0;
            else
                return -v;
        }
        #endregion Right stick directions
        #endregion Sticks

        #region DPad
        public int DPadHorizontal => CurrentState.DPad.Right == ButtonState.Pressed ? 1 : (CurrentState.DPad.Left == ButtonState.Pressed ? -1 : 0);
        public int DPadVertical => CurrentState.DPad.Down == ButtonState.Pressed ? 1 : (CurrentState.DPad.Up == ButtonState.Pressed ? -1 : 0);
        public Vector2 DPad => new Vector2(DPadHorizontal, DPadVertical);

        public bool DPadLeftCheck => CurrentState.DPad.Left == ButtonState.Pressed;
        public bool DPadLeftPressed => CurrentState.DPad.Left == ButtonState.Pressed && PreviousState.DPad.Left == ButtonState.Released;
        public bool DPadLeftReleased => CurrentState.DPad.Left == ButtonState.Released && PreviousState.DPad.Left == ButtonState.Pressed;

        public bool DPadRightCheck => CurrentState.DPad.Right == ButtonState.Pressed;
        public bool DPadRightPressed => CurrentState.DPad.Right == ButtonState.Pressed && PreviousState.DPad.Right == ButtonState.Released;
        public bool DPadRightReleased => CurrentState.DPad.Right == ButtonState.Released && PreviousState.DPad.Right == ButtonState.Pressed;

        public bool DPadUpCheck => CurrentState.DPad.Up == ButtonState.Pressed;
        public bool DPadUpPressed => CurrentState.DPad.Up == ButtonState.Pressed && PreviousState.DPad.Up == ButtonState.Released;
        public bool DPadUpReleased => CurrentState.DPad.Up == ButtonState.Released && PreviousState.DPad.Up == ButtonState.Pressed;

        public bool DPadDownCheck => CurrentState.DPad.Down == ButtonState.Pressed;
        public bool DPadDownPressed => CurrentState.DPad.Down == ButtonState.Pressed && PreviousState.DPad.Down == ButtonState.Released;
        public bool DPadDownReleased => CurrentState.DPad.Down == ButtonState.Released && PreviousState.DPad.Down == ButtonState.Pressed;
        #endregion DPad

        #region Trackers
        public bool LeftTriggerCheck(float threshold)
        {
            if (InputManager.IsDisabled)
                return false;
            return CurrentState.Triggers.Left >= threshold;
        }

        public bool LeftTriggerPressed(float threshold)
        {
            if (InputManager.IsDisabled)
                return false;
            return CurrentState.Triggers.Left >= threshold && PreviousState.Triggers.Left < threshold;
        }

        public bool LeftTriggerReleased(float threshold)
        {
            if (InputManager.IsDisabled)
                return false;
            return CurrentState.Triggers.Left < threshold && PreviousState.Triggers.Left >= threshold;
        }

        public bool RightTriggerCheck(float threshold)
        {
            if (InputManager.IsDisabled)
                return false;
            return CurrentState.Triggers.Right >= threshold;
        }

        public bool RightTriggerPressed(float threshold)
        {
            if (InputManager.IsDisabled)
                return false;
            return CurrentState.Triggers.Right >= threshold && PreviousState.Triggers.Right < threshold;
        }

        public bool RightTriggerReleased(float threshold)
        {
            if (InputManager.IsDisabled)
                return false;
            return CurrentState.Triggers.Right < threshold && PreviousState.Triggers.Right >= threshold;
        }
        #endregion Trackers

        #region Helpers
        public static void RumbleFirst(float strength, float time)
        {
            InputManager.GamePads[0].Rumble(strength, time);
        }

        public static int Axis(bool negative, bool positive, int bothValue)
        {
            if (negative)
            {
                if (positive)
                    return bothValue;
                else
                    return -1;
            }
            else if (positive)
                return 1;
            else
                return 0;
        }

        public static int Axis(float axisValue, float deadzone)
        {
            if (Math.Abs(axisValue) >= deadzone)
                return Math.Sign(axisValue);
            else
                return 0;
        }

        public static int Axis(bool negative, bool positive, int bothValue, float axisValue, float deadzone)
        {
            var ret = Axis(axisValue, deadzone);
            if (ret == 0)
                ret = Axis(negative, positive, bothValue);
            return ret;
        }
        #endregion Helpers
    }
}
