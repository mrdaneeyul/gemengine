﻿using GemEngine.Graphics;
using GemEngine.Input;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace GemEngine.Utility
{
    /// <summary>
    /// Handles commands in the debug console. Taken from Monocle Engine.
    /// </summary>
    public class DebugConsole
    {
        private const float UNDERSCORE_TIME = 0.5f;
        private const float REPEAT_DELAY = 0.5f;
        private const float REPEAT_EVERY = 1 / 30f;
        private const float OPACITY = 0.8f;

        public bool IsEnabled = true;
        public bool IsOpen;
        public Action[] FunctionKeyActions { get; private set; }

        private Dictionary<string, CommandInfo> commands;
        private List<string> sorted;

        private KeyboardState oldKeyboardState;
        private KeyboardState currentKeyboardState;
        private string currentText = string.Empty;
        private List<Line> drawCommands;
        private bool shouldShowUnderscore;
        private float underscoreCounter;
        private List<string> commandHistory;
        private int seekIndex = -1;
        private int tabIndex = -1;
        private string tabSearch;
        private float repeatCounter = 0;
        private Keys? repeatKey = null;
        private bool canOpen;

        public DebugConsole()
        {
            commandHistory = new List<string>();
            drawCommands = new List<Line>();
            commands = new Dictionary<string, CommandInfo>();
            sorted = new List<string>();
            FunctionKeyActions = new Action[12];

            BuildCommandsList();
        }

        public void Log(object obj, Color color)
        {
            var str = obj.ToString();

            //Newline splits
            if (str.Contains("\n"))
            {
                var all = str.Split('\n');
                foreach (var line in all)
                    Log(line, color);
                return;
            }

            //Split the string if you overflow horizontally
            var maxWidth = Engine.Instance.Window.ClientBounds.Width - 40;
            while (Draw.DefaultFont.MeasureString(str).X > maxWidth)
            {
                var split = -1;
                for (var i = 0; i < str.Length; i++)
                {
                    if (str[i] != ' ') continue;
                    if (Draw.DefaultFont.MeasureString(str.Substring(0, i)).X <= maxWidth)
                        split = i;
                    else
                        break;
                }
                if (split == -1) break;

                drawCommands.Insert(0, new Line(str.Substring(0, split), color));
                str = str.Substring(split + 1);
            }

            drawCommands.Insert(0, new Line(str, color));

            //Don't overflow on top of window
            var maxCommands = (Engine.Instance.Window.ClientBounds.Height - 100) / 30;
            while (drawCommands.Count > maxCommands)
                drawCommands.RemoveAt(drawCommands.Count - 1);
        }

        public void Log(object obj)
        {
            Log(obj, Color.White);
        }

        #region Updating and rendering
        internal void UpdateClosed()
        {
            if (!canOpen)
                canOpen = true;
            else if (InputManager.Keyboard.IsPressed(Keys.OemTilde, Keys.Oem8))
            {
                IsOpen = true;
                currentKeyboardState = Keyboard.GetState();
            }

            for (var i = 0; i < FunctionKeyActions.Length; i++)
                if (InputManager.Keyboard.IsPressed(Keys.F1 + i))
                    ExecuteFunctionKeyAction(i);
        }

        internal void UpdateOpen()
        {
            oldKeyboardState = currentKeyboardState;
            currentKeyboardState = Keyboard.GetState();

            underscoreCounter += Engine.DeltaTime;
            while (underscoreCounter >= UNDERSCORE_TIME)
            {
                underscoreCounter -= UNDERSCORE_TIME;
                shouldShowUnderscore = !shouldShowUnderscore;
            }

            if (repeatKey.HasValue)
            {
                if (currentKeyboardState[repeatKey.Value] == KeyState.Down)
                {
                    repeatCounter += Engine.DeltaTime;
                    while (repeatCounter >= REPEAT_DELAY)
                    {
                        HandleKey(repeatKey.Value);
                        repeatCounter -= REPEAT_EVERY;
                    }
                }
                else
                    repeatKey = null;
            }

            foreach (var key in currentKeyboardState.GetPressedKeys())
            {
                if (oldKeyboardState[key] != KeyState.Up) continue;
                HandleKey(key);
                break;
            }
        }

        private void HandleKey(Keys key)
        {
            if (key != Keys.Tab && key != Keys.LeftShift && key != Keys.RightShift && key != Keys.RightAlt && key != Keys.LeftAlt && key != Keys.RightControl && key != Keys.LeftControl)
                tabIndex = -1;

            if (key != Keys.OemTilde && key != Keys.Oem8 && key != Keys.Enter && repeatKey != key)
            {
                repeatKey = key;
                repeatCounter = 0;
            }

            var isShiftHeld = currentKeyboardState[Keys.LeftShift] == KeyState.Down || currentKeyboardState[Keys.RightShift] == KeyState.Down;
            switch (key)
            {
                default:
                    if (key.ToString().Length == 1)
                    {
                        if (isShiftHeld)
                            currentText += key.ToString();
                        else
                            currentText += key.ToString().ToLower();
                    }
                    break;
                case (Keys.D1):
                    if (isShiftHeld)
                        currentText += '!';
                    else
                        currentText += '1';
                    break;
                case (Keys.D2):
                    if (isShiftHeld)
                        currentText += '@';
                    else
                        currentText += '2';
                    break;
                case (Keys.D3):
                    if (isShiftHeld)
                        currentText += '#';
                    else
                        currentText += '3';
                    break;
                case (Keys.D4):
                    if (isShiftHeld)
                        currentText += '$';
                    else
                        currentText += '4';
                    break;
                case (Keys.D5):
                    if (isShiftHeld)
                        currentText += '%';
                    else
                        currentText += '5';
                    break;
                case (Keys.D6):
                    if (isShiftHeld)
                        currentText += '^';
                    else
                        currentText += '6';
                    break;
                case (Keys.D7):
                    if (isShiftHeld)
                        currentText += '&';
                    else
                        currentText += '7';
                    break;
                case (Keys.D8):
                    if (isShiftHeld)
                        currentText += '*';
                    else
                        currentText += '8';
                    break;
                case (Keys.D9):
                    if (isShiftHeld)
                        currentText += '(';
                    else
                        currentText += '9';
                    break;
                case (Keys.D0):
                    if (isShiftHeld)
                        currentText += ')';
                    else
                        currentText += '0';
                    break;
                case (Keys.OemComma):
                    if (isShiftHeld)
                        currentText += '<';
                    else
                        currentText += ',';
                    break;
                case (Keys.OemPeriod):
                    if (isShiftHeld)
                        currentText += '>';
                    else
                        currentText += '.';
                    break;
                case (Keys.OemQuestion):
                    if (isShiftHeld)
                        currentText += '?';
                    else
                        currentText += '/';
                    break;
                case (Keys.OemSemicolon):
                    if (isShiftHeld)
                        currentText += ':';
                    else
                        currentText += ';';
                    break;
                case (Keys.OemQuotes):
                    if (isShiftHeld)
                        currentText += '"';
                    else
                        currentText += '\'';
                    break;
                case (Keys.OemBackslash):
                    if (isShiftHeld)
                        currentText += '|';
                    else
                        currentText += '\\';
                    break;
                case (Keys.OemOpenBrackets):
                    if (isShiftHeld)
                        currentText += '{';
                    else
                        currentText += '[';
                    break;
                case (Keys.OemCloseBrackets):
                    if (isShiftHeld)
                        currentText += '}';
                    else
                        currentText += ']';
                    break;
                case (Keys.OemMinus):
                    if (isShiftHeld)
                        currentText += '_';
                    else
                        currentText += '-';
                    break;
                case (Keys.OemPlus):
                    if (isShiftHeld)
                        currentText += '+';
                    else
                        currentText += '=';
                    break;
                case (Keys.Space):
                    currentText += " ";
                    break;
                case (Keys.Back):
                    if (currentText.Length > 0)
                        currentText = currentText.Substring(0, currentText.Length - 1);
                    break;
                case (Keys.Delete):
                    currentText = string.Empty;
                    break;
                case (Keys.Up):
                    if (seekIndex < commandHistory.Count - 1)
                    {
                        seekIndex++;
                        currentText = string.Join(" ", commandHistory[seekIndex]);
                    }
                    break;
                case (Keys.Down):
                    if (seekIndex > -1)
                    {
                        seekIndex--;
                        if (seekIndex == -1)
                            currentText = string.Empty;
                        else
                            currentText = string.Join(" ", commandHistory[seekIndex]);
                    }
                    break;
                case (Keys.Tab):
                    if (isShiftHeld)
                    {
                        if (tabIndex == -1)
                        {
                            tabSearch = currentText;
                            FindLastTab();
                        }
                        else
                        {
                            tabIndex--;
                            if (tabIndex < 0 || (!string.IsNullOrEmpty(tabSearch) && sorted[tabIndex].IndexOf(tabSearch) != 0))
                                FindLastTab();
                        }
                    }
                    else
                    {
                        if (tabIndex == -1)
                        {
                            tabSearch = currentText;
                            FindFirstTab();
                        }
                        else
                        {
                            tabIndex++;
                            if (tabIndex >= sorted.Count || (!string.IsNullOrEmpty(tabSearch) && sorted[tabIndex].IndexOf(tabSearch) != 0))
                                FindFirstTab();
                        }
                    }
                    if (tabIndex != -1)
                        currentText = sorted[tabIndex];
                    break;
                case Keys.F1:
                case Keys.F2:
                case Keys.F3:
                case Keys.F4:
                case Keys.F5:
                case Keys.F6:
                case Keys.F7:
                case Keys.F8:
                case Keys.F9:
                case Keys.F10:
                case Keys.F11:
                case Keys.F12:
                    ExecuteFunctionKeyAction(key - Keys.F1);
                    break;
                case Keys.Enter:
                    if (currentText.Length > 0)
                        EnterCommand();
                    break;
                case Keys.Oem8:
                case Keys.OemTilde:
                    IsOpen = canOpen = false;
                    break;
            }
        }

        private void EnterCommand()
        {
            var data = currentText.Split(new char[] { ' ', ',' }, StringSplitOptions.RemoveEmptyEntries);
            if (commandHistory.Count == 0 || commandHistory[0] != currentText)
                commandHistory.Insert(0, currentText);
            drawCommands.Insert(0, new Line(currentText, Color.Aqua));
            currentText = string.Empty;
            seekIndex = -1;

            var args = new string[data.Length - 1];
            for (var i = 1; i < data.Length; i++)
                args[i - 1] = data[i];
            ExecuteCommand(data[0].ToLower(), args);
        }

        private void FindFirstTab()
        {
            for (var i = 0; i < sorted.Count; i++)
                if (string.IsNullOrEmpty(tabSearch) || sorted[i].IndexOf(tabSearch) == 0)
                {
                    tabIndex = i;
                    break;
                }
        }

        private void FindLastTab()
        {
            for (var i = 0; i < sorted.Count; i++)
                if (string.IsNullOrEmpty(tabSearch) || sorted[i].IndexOf(tabSearch) == 0)
                    tabIndex = i;
        }

        internal void Render()
        {
            var screenWidth = Engine.ViewWidth;
            var screenHeight = Engine.ViewHeight;

            Draw.SpriteBatch.Begin();

            Draw.Rectangle(10, screenHeight - 50, screenWidth - 20, 40, Color.Black * OPACITY);
            if (shouldShowUnderscore)
                Draw.SpriteBatch.DrawString(Draw.DefaultFont, ">" + currentText + "_", new Vector2(20, screenHeight - 42), Color.White);
            else
                Draw.SpriteBatch.DrawString(Draw.DefaultFont, ">" + currentText, new Vector2(20, screenHeight - 42), Color.White);

            if (drawCommands.Count > 0)
            {
                var height = 10 + (30 * drawCommands.Count);
                Draw.Rectangle(10, screenHeight - height - 60, screenWidth - 20, height, Color.Black * OPACITY);
                for (var i = 0; i < drawCommands.Count; i++)
                    Draw.SpriteBatch.DrawString(Draw.DefaultFont, drawCommands[i].Text, new Vector2(20, screenHeight - 92 - (30 * i)), drawCommands[i].Color);

            }

            Draw.SpriteBatch.End();

        }
        #endregion Updating and rendering

        #region Execute
        public void ExecuteCommand(string command, string[] args)
        {
            if (commands.ContainsKey(command))
                commands[command].Action(args);
            else
                Log($"Argle-bargle glop-glyf. Command '{command}' not found! Type 'help' for a list of commands", Color.Yellow);
        }

        public void ExecuteFunctionKeyAction(int num)
        {
            FunctionKeyActions[num]?.Invoke();
        }
        #endregion Execute

        #region Parse commands
        private void BuildCommandsList()
        {
#if !CONSOLE
            //Check GemEngine for commands
            foreach (var type in Assembly.GetCallingAssembly().GetTypes())
                foreach (var method in type.GetMethods(BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic))
                    ProcessMethod(method);

            //Check the calling assembly for commands
            foreach (var type in Assembly.GetEntryAssembly().GetTypes())
                foreach (var method in type.GetMethods(BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic))
                    ProcessMethod(method);

            //Maintain the sorted command list
            foreach (var command in commands)
                sorted.Add(command.Key);
            sorted.Sort();
#endif
        }

        private void ProcessMethod(MethodInfo method)
        {
            Command attribute = null;
            {
                var attributes = method.GetCustomAttributes(typeof(Command), false);
                if (attributes.Length > 0)
                    attribute = attributes[0] as Command;
            }

            if (attribute != null)
            {
                if (!method.IsStatic)
                    throw new Exception($"{method.DeclaringType.Name}.{method.Name} is marked as a command but isn't static.");
                else
                {
                    var info = new CommandInfo();
                    info.Help = attribute.Help;

                    var parameters = method.GetParameters();
                    var defaults = new object[parameters.Length];
                    var usages = new string[parameters.Length];

                    for (var i = 0; i < parameters.Length; i++)
                    {
                        var p = parameters[i];
                        usages[i] = $"{p.Name}:";

                        if (p.ParameterType == typeof(string))
                            usages[i] += "string";
                        else if (p.ParameterType == typeof(int))
                            usages[i] += "int";
                        else if (p.ParameterType == typeof(float))
                            usages[i] += "float";
                        else if (p.ParameterType == typeof(bool))
                            usages[i] += "bool";
                        else
                            throw new Exception($"{method.DeclaringType.Name}.{method.Name} is marked as a command but has an invalid parameter type. Allowed types are: string, int, float, and bool.");

                        if (p.DefaultValue == DBNull.Value)
                            defaults[i] = null;
                        else if (p.DefaultValue != null)
                        {
                            defaults[i] = p.DefaultValue;
                            if (p.ParameterType == typeof(string))
                                usages[i] += $"=\"{p.DefaultValue}\"";
                            else
                                usages[i] += $"={p.DefaultValue}";
                        }
                        else
                            defaults[i] = null;
                    }

                    if (usages.Length == 0)
                        info.Usage = string.Empty;
                    else
                        info.Usage = $"[{string.Join(@" ", usages)}]";

                    info.Action = (args) =>
                    {
                        if (parameters.Length == 0)
                            InvokeMethod(method);
                        else
                        {
                            var param = (object[])defaults.Clone();
                            for (var i = 0; i < param.Length && i < args.Length; i++)
                            {
                                if (parameters[i].ParameterType == typeof(string))
                                    param[i] = ArgString(args[i]);
                                else if (parameters[i].ParameterType == typeof(int))
                                    param[i] = ArgInt(args[i]);
                                else if (parameters[i].ParameterType == typeof(float))
                                    param[i] = ArgFloat(args[i]);
                                else if (parameters[i].ParameterType == typeof(bool))
                                    param[i] = ArgBool(args[i]);
                            }
                            InvokeMethod(method, param);
                        }
                    };
                    commands[attribute.Name] = info;
                }
            }
        }

        private void InvokeMethod(MethodInfo method, object[] parameters = null)
        {
            try
            {
                method.Invoke(null, parameters);
            }
            catch (Exception e)
            {
                Engine.DebugConsole.Log(e.InnerException.Message, Color.Yellow);
                LogStackTrace(e.InnerException.StackTrace);
            }
        }

        private void LogStackTrace(string stackTrace)
        {
            foreach (var call in stackTrace.Split('\n'))
            {
                var log = call;

                //Remove file path
                {
                    var from = log.LastIndexOf(" in " + 4);
                    var to = log.LastIndexOf('\\') + 1;
                    if (from != -1 && to != -1)
                        log = log.Substring(0, from) + log.Substring(to);
                }

                //Remove arguments list
                {
                    var from = log.IndexOf('(') + 1;
                    var to = log.IndexOf(')');
                    if (from != -1 && to != -1)
                        log = log.Substring(0, from) + log.Substring(to);
                }

                //Space out colon line number
                var colon = log.LastIndexOf(':');
                if (colon != -1)
                    log = log.Insert(colon + 1, " ").Insert(colon, " ");

                log = log.TrimStart();
                log = $"-> {log}";

                Engine.DebugConsole.Log(log, Color.White);
            }
        }

        private struct CommandInfo
        {
            public Action<string[]> Action;
            public string Help;
            public string Usage;
        }

        #region Parsing arguments
        private static string ArgString(string arg)
        {
            if (arg == null)
                return string.Empty;
            else
                return arg;
        }

        private static int ArgInt(string arg)
        {
            try
            {
                return Convert.ToInt32(arg);
            }
            catch
            {
                return 0;
            }
        }

        private static float ArgFloat(string arg)
        {
            try
            {
                return Convert.ToSingle(arg);
            }
            catch
            {
                return 0;
            }
        }

        private static bool ArgBool(string arg)
        {
            if (arg != null)
                return !(arg == "0" || arg.ToLower() == "false" || arg.ToLower() == "f");
            else
                return false;
        }
        #endregion Parsing arguments
        #endregion Parse commands

        #region Built-in commands
#if !CONSOLE
        [Command("clear", "Clears the terminal")]
        private static void Clear()
        {
            Engine.DebugConsole.drawCommands.Clear();
        }

        [Command("exit", "Exits the game")]
        private static void Exit()
        {
            Engine.Instance.Exit();
        }

        [Command("vsync", "Enables or disables vertical sync")]
        private static void Vsync(bool enabled = true)
        {
            Engine.Graphics.SynchronizeWithVerticalRetrace = enabled;
            Engine.Graphics.ApplyChanges();
            Engine.DebugConsole.Log($"Vertical sync {(enabled ? "enabled" : "disabled")}");
        }

        [Command("fixed", "Enables or disables fixed time step")]
        private static void Fixed(bool enabled = true)
        {
            Engine.Instance.IsFixedTimeStep = enabled;
            Engine.DebugConsole.Log($"Fixed time step {(enabled ? "enabled" : "disabled")}");
        }

        [Command("framerate", "Sets the target framerate")]
        private static void Framerate(float targetFramerate = 60)
        {
            Engine.Instance.TargetElapsedTime = TimeSpan.FromSeconds(1.0 / targetFramerate);
        }

        [Command("count", "Logs amount of entities in the scene. Pass a tagIndex to only count entities with that tag.")]
        private static void Count(int tagIndex = -1)
        {
            if (Engine.Scene == null)
            {
                Engine.DebugConsole.Log("Current scene is null");
                return;
            }

            if (tagIndex < 0)
                Engine.DebugConsole.Log(Engine.Scene.Entities.Count.ToString());
            else
                Engine.DebugConsole.Log(Engine.Scene.TagLists[tagIndex].Count.ToString());
        }

        [Command("tracker", "Logs all tracked objects in the scene. Set mode to 'e' for just entities, or 'c' for just components")]
        private static void Tracker(string mode)
        {
            if (Engine.Scene == null)
            {
                Engine.DebugConsole.Log("Current scene is null");
                return;
            }

            switch(mode)
            {
                default:
                    Engine.DebugConsole.Log("-- Entities --");
                    Engine.Scene.Tracker.LogEntities();
                    Engine.DebugConsole.Log("-- Components --");
                    Engine.Scene.Tracker.LogComponents();
                    break;
                case "e":
                    Engine.Scene.Tracker.LogEntities();
                    break;
                case "c":
                    Engine.Scene.Tracker.LogComponents();
                    break;
            }
        }

        [Command("pooler", "Logs the pooled entity counts")]
        private static void Pooler()
        {
            Engine.Pooler.Log();
        }

        [Command("fullscreen", "Switches to fullscreen mode")]
        private static void Fullscreen()
        {
            Engine.SetFullscreen();
        }

        [Command("windowed", "Switches to windowed mode")]
        private static void Window(int scale = 1)
        {
            Engine.SetWindowed(Engine.Width * scale, Engine.Height * scale);
        }

        [Command("help", "Shows usage help for a given command")]
        private static void Help(string command)
        {
            if (Engine.DebugConsole.sorted.Contains(command))
            {
                var cmd = Engine.DebugConsole.commands[command];
                var str = new StringBuilder();

                //Title
                str.Append(":: ");
                str.Append(command);

                //Usage
                if (!string.IsNullOrEmpty(cmd.Usage))
                {
                    str.Append(" ");
                    str.Append(cmd.Usage);
                }
                Engine.DebugConsole.Log(str.ToString());

                //Help
                if (string.IsNullOrEmpty(cmd.Help))
                    Engine.DebugConsole.Log("No help info set");
                else
                    Engine.DebugConsole.Log(cmd.Help);
            }
            else
            {
                var str = new StringBuilder();
                str.Append("Commands list: ");
                str.Append(string.Join(", ", Engine.DebugConsole.sorted));
                Engine.DebugConsole.Log(str.ToString());
                Engine.DebugConsole.Log("Type 'help command' for more info on that command!");
            }
        }
#endif
        #endregion Built-in commands

        private struct Line
        {
            public string Text;
            public Color Color;

            public Line(string text)
            {
                Text = text;
                Color = Color.White;
            }

            public Line(string text, Color color)
            {
                Text = text;
                Color = color;
            }
        }

        public class Command : Attribute
        {
            public string Name;
            public string Help;

            public Command(string name, string help)
            {
                Name = name;
                Help = help;
            }
        }
    }
}
