﻿using System;
using System.IO;
using System.Text;

namespace GemEngine.Utility
{
    public static class ErrorLog
    {
        public const string FILENAME = "error_log.txt";
        public const string MARKER = "==========================================";

        public static void Write(Exception e)
        {
            Write(e.ToString());
        }

        public static void Write(string str)
        {
            var s = new StringBuilder();

            //Get the previous contents
            var content = string.Empty;
            if (File.Exists(FILENAME))
            {
                TextReader tr = new StreamReader(FILENAME);
                content = tr.ReadToEnd();
                tr.Close();

                if (!content.Contains(MARKER))
                    content = string.Empty;
            }

            //Header
            if (Engine.Instance != null)
                s.Append(Engine.Instance.Title);
            else
                s.Append("GemEngine");
            s.AppendLine(" Error Log");
            s.AppendLine(MARKER);
            s.AppendLine();

            //Version Number
            if (Engine.Instance.Version != null)
            {
                s.Append("Version ");
                s.AppendLine(Engine.Instance.Version.ToString());
            }

            //Datetime
            s.AppendLine(DateTime.Now.ToString());

            //String
            s.AppendLine(str);

            //If the file wasn't empty, preserve the old errors
            if (!string.IsNullOrEmpty(content))
            {
                var at = content.IndexOf(MARKER) + MARKER.Length;
                var after = content.Substring(at);
                s.AppendLine(after);
            }

            TextWriter tw = new StreamWriter(FILENAME, false);
            tw.Write(s.ToString());
            tw.Close();
        }

        public static void Open()
        {
            if (File.Exists(FILENAME))
                System.Diagnostics.Process.Start(FILENAME);
        }
    }
}
