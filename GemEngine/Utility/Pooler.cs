﻿using GemEngine.ECS;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace GemEngine.Utility
{
    public class Pooler
    {
        internal Dictionary<Type, Queue<Entity>> Pools { get; private set; }

        public Pooler()
        {
            Pools = new Dictionary<Type, Queue<Entity>>();
            foreach (var type in Assembly.GetEntryAssembly().GetTypes())
            {
                if (type.GetCustomAttributes(typeof(Pooled), false).Length <= 0)
                    return;

                if (!typeof(Entity).IsAssignableFrom(type))
                    throw new Exception($"Type '{type.Name}' cannot be pooled because it doesn't derive from Entity");
                else if (type.GetConstructor(Type.EmptyTypes) == null)
                    throw new Exception($"Type '{type.Name}' cannot be pooled because it doesn't have a parameterless constructor");
                else
                    Pools.Add(type, new Queue<Entity>());
            }
        }

        public T Create<T>() where T : Entity, new()
        {
            if (!Pools.ContainsKey(typeof(T)))
                return new T();

            var queue = Pools[typeof(T)];
            if (queue.Count == 0)
                return new T();
            else
                return queue.Dequeue() as T;
        }

        internal void EntityRemoved(Entity entity)
        {
            var type = entity.GetType();
            if (Pools.ContainsKey(type))
                Pools[type].Enqueue(entity);
        }

        public void Log()
        {
            if (Pools.Count == 0)
                Engine.DebugConsole.Log("No entity types are marked as pooled!");

            foreach (var kv in Pools)
            {
                var output = $"{kv.Key.Name} : {kv.Value.Count}";
                Engine.DebugConsole.Log(output);
            }
        }
    }

    public class Pooled : Attribute { }
}
