﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace GemEngine.Utility
{
    public class Camera
    {
        private Matrix matrix = Matrix.Identity;
        private Matrix inverse = Matrix.Identity;
        private bool hasChanged;
        private Vector2 position = Vector2.Zero;
        private Vector2 zoom = Vector2.One;
        private Vector2 origin = Vector2.Zero;
        private float angle = 0;

        public Viewport Viewport;
        public Matrix Matrix
        {
            get
            {
                if (hasChanged)
                    UpdateMatrices();
                return matrix;
            }
        }
        public Matrix Inverse
        {
            get
            {
                if (hasChanged)
                    UpdateMatrices();
                return inverse;
            }
        }
        public Vector2 Position
        {
            get { return position; }
            set
            {
                hasChanged = true;
                position = value;
            }
        }
        public Vector2 Origin
        {
            get { return origin; }
            set
            {
                hasChanged = true;
                origin = value;
            }
        }

        public float X
        {
            get { return position.X; }
            set
            {
                hasChanged = true;
                position.X = value;
            }
        }

        public float Y
        {
            get { return position.Y; }
            set
            {
                hasChanged = true;
                position.X = value;
            }
        }

        public float Zoom
        {
            get { return zoom.X; }
            set
            {
                hasChanged = true;
                zoom.X = zoom.Y = value;
            }
        }

        public float Angle
        {
            get { return angle; }
            set
            {
                hasChanged = true;
                angle = value;
            }
        }

        public float Left
        {
            get
            {
                if (hasChanged)
                    UpdateMatrices();
                return Vector2.Transform(Vector2.Zero, Inverse).X;
            }
            set
            {
                if (hasChanged)
                    UpdateMatrices();
                X = Vector2.Transform(Vector2.UnitX * value, Matrix).X;
            }
        }

        public float Right
        {
            get
            {
                if (hasChanged)
                    UpdateMatrices();
                return Vector2.Transform(Vector2.UnitX * Viewport.Width, Inverse).X;
            }
            set { throw new NotImplementedException(); }
        }

        public float Top
        {
            get
            {
                if (hasChanged)
                    UpdateMatrices();
                return Vector2.Transform(Vector2.Zero, Inverse).Y;
            }
            set
            {
                if (hasChanged)
                    UpdateMatrices();
                Y = Vector2.Transform(Vector2.UnitY * value, Matrix).Y;
            }
        }

        public float Bottom
        {
            get
            {
                if (hasChanged)
                    UpdateMatrices();
                return Vector2.Transform(Vector2.UnitY * Viewport.Height, Inverse).Y;
            }
            set { throw new NotImplementedException(); }
        }

        public Camera()
        {
            Viewport = new Viewport
            {
                Width = Engine.Width,
                Height = Engine.Height
            };
            UpdateMatrices();
        }

        public Camera(int width, int height)
        {
            Viewport = new Viewport
            {
                Width = width,
                Height = height
            };
            UpdateMatrices();
        }

        public override string ToString()
        {
            return "Camera:\n\tViewport: { " + Viewport.X + ", " + Viewport.Y + ", " + Viewport.Width + ", " + Viewport.Height +
                " }\n\tPosition: { " + position.X + ", " + position.Y +
                " }\n\tOrigin: { " + origin.X + ", " + origin.Y +
                " }\n\tZoom: { " + zoom.X + ", " + zoom.Y +
                " }\n\tAngle: " + angle;
        }

        private void UpdateMatrices()
        {
            matrix = Matrix.Identity *
                     Matrix.CreateTranslation(new Vector3(-new Vector2((int)Math.Floor(position.X), (int)Math.Floor(position.Y)), 0)) *
                     Matrix.CreateRotationZ(angle) *
                     Matrix.CreateScale(new Vector3(zoom, 1)) *
                     Matrix.CreateTranslation(new Vector3(new Vector2((int)Math.Floor(origin.X), (int)Math.Floor(origin.Y)), 0));
            inverse = Matrix.Invert(matrix);
            hasChanged = false;
        }

        public void CopyFrom(Camera otherCamera)
        {
            position = otherCamera.position;
            origin = otherCamera.origin;
            angle = otherCamera.angle;
            zoom = otherCamera.zoom;
            hasChanged = true;
        }

        #region Utilities
        public void CenterOrigin()
        {
            origin = new Vector2((float)Viewport.Width / 2, (float)Viewport.Height / 2);
            hasChanged = true;
        }

        public void RoundPosition()
        {
            position.X = (float)Math.Round(position.X);
            position.Y = (float)Math.Round(position.Y);
        }

        public Vector2 ScreenToCamera(Vector2 position)
        {
            return Vector2.Transform(position, Inverse);
        }

        public Vector2 CameraToScreen(Vector2 position)
        {
            return Vector2.Transform(position, Matrix);
        }

        public void Approach(Vector2 position, float ease)
        {
            Position += (position - Position) * ease;
        }

        public void Approach(Vector2 position, float ease, float maxDistance)
        {
            var move = (position - Position) * ease;
            if (move.Length() > maxDistance)
                Position += Vector2.Normalize(move) * maxDistance;
            else
                Position += move;
        }
        #endregion Utilities
    }
}
