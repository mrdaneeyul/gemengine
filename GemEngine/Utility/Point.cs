﻿namespace GemEngine.Utility
{
    public struct Point
    {
        public static readonly Point Zero = new Point(0, 0);
        public static readonly Point UnitX = new Point(1, 0);
        public static readonly Point UnitY = new Point(0, 1);
        public static readonly Point One = new Point(1, 1);

        public int X;
        public int Y;

        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }

        #region Point operators

        public static bool operator ==(Point a, Point b)
        {
            return a.X == b.X && a.Y == b.Y;
        }

        public static bool operator !=(Point a, Point b)
        {
            return a.X != b.X || a.Y != b.Y;
        }

        public static Point operator +(Point a, Point b)
        {
            return new Point(a.X + b.X, a.Y + b.Y);
        }

        public static Point operator -(Point a, Point b)
        {
            return new Point(a.X - b.X, a.Y - b.Y);
        }

        public static Point operator *(Point a, Point b)
        {
            return new Point(a.X * b.X, a.Y * b.Y);
        }

        public static Point operator /(Point a, Point b)
        {
            return new Point(a.X / b.X, a.Y / b.Y);
        }

        public static Point operator %(Point a, Point b)
        {
            return new Point(a.X % b.X, a.Y % b.Y);
        }

        #endregion Point operators

        #region int operators

        public static bool operator ==(Point a, int b)
        {
            return a.X == b && a.Y == b;
        }

        public static bool operator !=(Point a, int b)
        {
            return a.X != b || a.Y != b;
        }

        public static Point operator +(Point a, int b)
        {
            return new Point(a.X + b, a.Y + b);
        }

        public static Point operator -(Point a, int b)
        {
            return new Point(a.X - b, a.Y - b);
        }

        public static Point operator *(Point a, int b)
        {
            return new Point(a.X * b, a.Y * b);
        }

        public static Point operator /(Point a, int b)
        {
            return new Point(a.X / b, a.Y / b);
        }

        public static Point operator %(Point a, int b)
        {
            return new Point(a.X % b, a.Y % b);
        }

        #endregion int operators

        public override bool Equals(object obj) => false;

        public override int GetHashCode() => X * 10000 + Y;

        public override string ToString() => "{ X: " + X + ", Y: " + Y + " }";
    }
}
