﻿using GemEngine.Graphics;
using GemEngine.Utility;
using Microsoft.Xna.Framework;

namespace GemEngine.Collision
{
    public class Hitbox : Collider
    {
        private float width;
        private float height;

        public override float Width
        {
            get { return width; }
            set { width = value; }
        }

        public override float Height
        {
            get { return height; }
            set { height = value; }
        }

        public override float Left
        {
            get { return Position.X; }
            set { Position.X = value; }
        }

        public override float Top
        {
            get { return Position.Y; }
            set { Position.Y = value; }
        }

        public override float Right
        {
            get { return Position.X + width; }
            set { Position.X = value - Width; }
        }

        public override float Bottom
        {
            get { return Position.Y + height; }
            set { Position.Y = value - Height; }
        }

        public Hitbox(float width, float height, float x = 0, float y = 0)
        {
            this.width = width;
            this.height = height;
            Position.X = x;
            Position.Y = y;
        }

        public bool Intersects(Hitbox hitbox)
        {
            return AbsoluteLeft < hitbox.AbsoluteRight && AbsoluteRight > hitbox.AbsoluteLeft &&
                   AbsoluteBottom > hitbox.AbsoluteTop && AbsoluteTop < hitbox.AbsoluteBottom;
        }

        public bool Intersects(float x, float y, float width, float height)
        {
            return AbsoluteRight > x && AbsoluteBottom > y && AbsoluteLeft < x + width && AbsoluteTop < y + height;
        }

        public override Collider Clone()
        {
            return new Hitbox(width, height, Position.X, Position.Y);
        }

        public override void Render(Camera camera, Color color)
        {
            Draw.HollowRectangle(AbsoluteX, AbsoluteY, Width, Height, color);
        }

        public void SetFromRectangle(Rectangle rectangle)
        {
            Position = new Vector2(rectangle.X, rectangle.Y);
            Width = rectangle.Width;
            Height = rectangle.Height;
        }

        public void Set(float x, float y, float width, float height)
        {
            Position = new Vector2(x, y);
            Width = width;
            Height = height;
        }

        #region Get edges
        public void GetTopEdge(out Vector2 fromPoint, out Vector2 toPoint)
        {
            fromPoint.X = AbsoluteLeft;
            toPoint.X = AbsoluteRight;
            fromPoint.Y = toPoint.Y = AbsoluteTop;
        }

        public void GetBottomEdge(out Vector2 fromPoint, out Vector2 toPoint)
        {
            fromPoint.X = AbsoluteLeft;
            toPoint.X = AbsoluteRight;
            fromPoint.Y = toPoint.Y = AbsoluteBottom;
        }

        public void GetLeftEdge(out Vector2 fromPoint, out Vector2 toPoint)
        {
            fromPoint.Y = AbsoluteTop;
            toPoint.Y = AbsoluteBottom;
            fromPoint.X = toPoint.X = AbsoluteLeft;
        }

        public void GetRightEdge(out Vector2 fromPoint, out Vector2 toPoint)
        {
            fromPoint.Y = AbsoluteTop;
            toPoint.Y = AbsoluteBottom;
            fromPoint.X = toPoint.X = AbsoluteRight;
        }
        #endregion Get edges

        #region Checking against other colliders
        public override bool Collide(Vector2 point)
        {
            return Collision.Collide.RectangleToPoint(AbsoluteLeft, AbsoluteTop, Width, Height, point);
        }

        public override bool Collide(Rectangle rectangle)
        {
            return AbsoluteRight > rectangle.Left && AbsoluteBottom > rectangle.Top 
                && AbsoluteLeft < rectangle.Right && AbsoluteTop < rectangle.Bottom;
        }

        public override bool Collide(Vector2 from, Vector2 to)
        {
            return Collision.Collide.RectangleToLine(AbsoluteLeft, AbsoluteTop, Width, Height, from, to);
        }

        public override bool Collide(Hitbox hitbox)
        {
            return Intersects(hitbox);
        }

        public override bool Collide(Grid grid)
        {
            return grid.Collide(Bounds);
        }

        public override bool Collide(Circle circle)
        {
            return Collision.Collide.RectangleToCircle(AbsoluteLeft, AbsoluteTop, Width, Height, circle.AbsolutePosition, circle.Radius);
        }

        public override bool Collide(ColliderList list)
        {
            return list.Collide(this);
        }
        #endregion Checking against other colliders
    }
}
