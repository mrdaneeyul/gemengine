﻿using GemEngine.ECS;
using GemEngine.Utility;
using Microsoft.Xna.Framework;
using System;
using System.Linq;

namespace GemEngine.Collision
{
    public class ColliderList : Collider
    {
        public Collider[] Colliders { get; private set; }

        public ColliderList(params Collider[] colliders)
        {
            if (colliders.Any(c => c == null))
                throw new Exception("Cannot add a null Collider to a ColliderList.");
            Colliders = colliders;
        }

        public void Add(params Collider[] toAdd)
        {
#if DEBUG
            foreach (var collider in toAdd)
            {
                if (Colliders.Contains(collider))
                    throw new Exception("Adding a Collider to a ColliderList that already contains it!");
                else if (collider == null)
                    throw new Exception("Cannot add a null Collider to a ColliderList.");
            }
#endif
            var newColliders = new Collider[Colliders.Length + toAdd.Length];
            for (var i = 0; i < Colliders.Length; i++)
                newColliders[i] = Colliders[i];
            for (var i = 0; i < toAdd.Length; i++)
            {
                newColliders[i + Colliders.Length] = toAdd[i];
                toAdd[i].Added(Entity);
            }
            Colliders = newColliders;
        }

        public void Remove(params Collider[] toRemove)
        {
#if DEBUG
            foreach (var collider in toRemove)
            {
                if (!Colliders.Contains(collider))
                    throw new Exception("Removing a Collider from a ColliderList that does not contain it!");
                else if (collider == null)
                    throw new Exception("Cannot remove a null Collider from a ColliderList.");
            }
#endif
            var newColliders = new Collider[Colliders.Length - toRemove.Length];
            var at = 0;
            foreach (var collider in Colliders)
            {
                if (!toRemove.Contains(collider))
                {
                    newColliders[at] = collider;
                    at++;
                }
            }
            Colliders = newColliders;
        }

        internal override void Added(Entity entity)
        {
            base.Added(entity);
            foreach (var collider in Colliders)
                collider.Added(entity);
        }

        internal override void Removed()
        {
            base.Removed();
            foreach (var collider in Colliders)
                collider.Removed();
        }

        public override float Width
        {
            get { return Right - Left; }
            set { throw new NotImplementedException(); }
        }

        public override float Height
        {
            get { return Bottom - Top; }
            set { throw new NotImplementedException(); }
        }

        public override float Left
        {
            get
            {
                var left = Colliders[0].Left;
                for (var i = 1; i < Colliders.Length; i++)
                    if (Colliders[i].Left < left)
                        left = Colliders[i].Left;
                return left;
            }
            set
            {
                var changeX = value - Left;
                foreach (var collider in Colliders)
                    Position.X += changeX;
            }
        }

        public override float Right
        {
            get
            {
                var right = Colliders[0].Right;
                for (var i = 1; i < Colliders.Length; i++)
                    if (Colliders[i].Right > right)
                        right = Colliders[i].Right;
                return right;
            }
            set
            {
                var changeX = value - Right;
                foreach (var collider in Colliders)
                    Position.X += changeX;
            }
        }

        public override float Top
        {
            get
            {
                var top = Colliders[0].Top;
                for (var i = 1; i < Colliders.Length; i++)
                    if (Colliders[i].Top < top)
                        top = Colliders[i].Top;
                return top;
            }
            set
            {
                var changeY = value - Top;
                foreach (var c in Colliders)
                    Position.Y += changeY;
            }
        }

        public override float Bottom
        {
            get
            {
                var bottom = Colliders[0].Bottom;
                for (var i = 1; i < Colliders.Length; i++)
                    if (Colliders[i].Bottom > bottom)
                        bottom = Colliders[i].Bottom;
                return bottom;
            }
            set
            {
                var changeY = value - Bottom;
                foreach (var collider in Colliders)
                    Position.Y += changeY;
            }
        }

        public override Collider Clone()
        {
            var clones = new Collider[Colliders.Length];
            for (var i = 0; i < Colliders.Length; i++)
                clones[i] = Colliders[i].Clone();
            return new ColliderList(clones);
        }

        public override void Render(Camera camera, Color color)
        {
            foreach (var collider in Colliders)
                collider.Render(camera, color);
        }

        #region Check against other colliders
        public override bool Collide(Vector2 point)
        {
            foreach (var collider in Colliders)
                if (collider.Collide(point))
                    return true;
            return false;
        }

        public override bool Collide(Rectangle rectangle)
        {
            foreach (var collider in Colliders)
                if (collider.Collide(rectangle))
                    return true;
            return false;
        }

        public override bool Collide(Vector2 from, Vector2 to)
        {
            foreach (var collider in Colliders)
                if (collider.Collide(from, to))
                    return true;
            return false;
        }

        public override bool Collide(Hitbox hitbox)
        {
            foreach (var collider in Colliders)
                if (collider.Collide(hitbox))
                    return true;
            return false;
        }

        public override bool Collide(Grid grid)
        {
            foreach (var collider in Colliders)
                if (collider.Collide(grid))
                    return true;
            return false;
        }

        public override bool Collide(Circle circle)
        {
            foreach (var collider in Colliders)
                if (collider.Collide(circle))
                    return true;
            return false;
        }

        public override bool Collide(ColliderList list)
        {
            foreach (var collider in Colliders)
                if (collider.Collide(list))
                    return true;
            return false;
        }
        #endregion Check against other colliders
    }
}
