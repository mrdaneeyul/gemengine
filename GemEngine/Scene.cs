﻿using GemEngine.ECS;
using GemEngine.Graphics.Render;
using GemEngine.Utility;
using Microsoft.Xna.Framework;
using System;
using System.Collections;
using System.Collections.Generic;

namespace GemEngine
{
    public class Scene : IEnumerable<Entity>, IEnumerable
    {
        public bool IsPaused;
        public float TimeActive;
        public float RawTimeActive;
        public bool IsFocused { get; private set; }
        public EntityList Entities { get; private set; }
        public TagLists TagLists { get; private set; }
        public RendererList Renderers { get; private set; }
        public Entity HelperEntity { get; private set; }
        public Tracker Tracker { get; private set; }

        private Dictionary<int, double> actualDepthLookup;

        public event Action OnEndOfFrame;

        public Scene()
        {
            Tracker = new Tracker();
            Entities = new EntityList(this);
            TagLists = new TagLists();
            Renderers = new RendererList(this);

            actualDepthLookup = new Dictionary<int, double>();

            HelperEntity = new Entity();
            Entities.Add(HelperEntity);
        }

        public virtual void Begin()
        {
            IsFocused = true;
            foreach (var entity in Entities)
                entity.SceneBegin(this);
        }

        public virtual void End()
        {
            IsFocused = false;
            foreach (var entity in Entities)
                entity.SceneEnd(this);
        }

        public virtual void BeforeUpdate()
        {
            if (!IsPaused)
                TimeActive += Engine.DeltaTime;
            RawTimeActive += Engine.RawDeltaTime;

            Entities.UpdateLists();
            TagLists.UpdateLists();
            Renderers.UpdateLists();
        }

        public virtual void Update()
        {
            if (IsPaused) return;
            Entities.Update();
            Renderers.Update();
        }

        public virtual void AfterUpdate()
        {
            if (OnEndOfFrame == null) return;
            OnEndOfFrame();
            Renderers.Update();
        }

        public virtual void BeforeRender()
        {
            Renderers.BeforeRender();
        }

        public virtual void Render()
        {
            Renderers.Render();
        }

        public virtual void AfterRender()
        {
            Renderers.AfterRender();
        }

        public virtual void HandleGraphicsReset()
        {
            Entities.HandleGraphicsReset();
        }

        public virtual void HandleGraphicsCreate()
        {
            Entities.HandleGraphicsCreate();
        }

        public virtual void GainFocus() { }

        public virtual void LoseFocus() { }

        #region Interval
        /// <summary>
        /// Returns whether the scene timer has passed the given time interval since the last frame. Ex: given 2.0f, this will return true once every 2 seconds
        /// </summary>
        /// <param name="interval">The time interval to check for</param>
        public bool IsOnInterval(float interval)
        {
            return (int)((TimeActive - Engine.DeltaTime) / interval) < (int)(TimeActive / interval);
        }

        /// <summary>
        /// Returns whether the scene timer has passed the given time interval since the last frame. Ex: given 2.0f, this will return true once every 2 seconds
        /// This overload will subtract an offset from the time active
        /// </summary>
        /// <param name="interval">The time interval to check for</param>
        /// <param name="offset">The extra offset to allow</param>
        public bool IsOnInterval(float interval, float offset)
        {
            return Math.Floor((TimeActive - offset - Engine.DeltaTime) / interval) < Math.Floor((TimeActive - offset) / interval);
        }

        /// <summary>
        /// Returns whether the scene timer is still between intervals
        /// </summary>
        /// <param name="interval">The upcoming interval</param>
        public bool IsBetweenIntervals(float interval)
        {
            return Calculate.IsBetweenIntervals(TimeActive, interval);
        }

        /// <summary>
        /// Returns whether the scene timer has passed the given raw time interval since the last frame. Ex: given 2.0f, this will return true once every 2 seconds
        /// </summary>
        /// <param name="interval">The raw time interval to check for</param>
        public bool IsOnRawInterval(float interval)
        {
            return (int)((RawTimeActive - Engine.RawDeltaTime) / interval) < (int)(RawTimeActive / interval);
        }

        /// <summary>
        /// Returns whether the scene timer has passed the given raw time interval since the last frame. Ex: given 2.0f, this will return true once every 2 seconds
        /// This overload will subtract an offset from the time active
        /// </summary>
        /// <param name="interval">The raw time interval to check for</param>
        /// <param name="offset">The extra offset to allow</param>
        public bool IsOnRawInterval(float interval, float offset)
        {
            return Math.Floor((RawTimeActive - offset - Engine.RawDeltaTime) / interval) < Math.Floor((RawTimeActive - offset) / interval);
        }

        /// <summary>
        /// Returns whether the scene timer is still between raw intervals
        /// </summary>
        /// <param name="interval">The upcoming interval</param>
        public bool IsBetweenRawIntervals(float interval)
        {
            return Calculate.IsBetweenIntervals(RawTimeActive, interval);
        }
        #endregion Interval

        #region Collisions via tags
        /// <summary>
        /// Check for a collision at a point with any entity with the specified tag
        /// </summary>
        /// <param name="point">The point to check the collision at</param>
        /// <param name="tag">The tag of the entities to check collisions for</param>
        public bool CollideCheck(Vector2 point, int tag)
        {
            var list = TagLists[tag];
            for (var i = 0; i < list.Count; i++)
                if (list[i].IsCollidable && list[i].CollidePoint(point))
                    return true;
            return false;
        }

        /// <summary>
        /// Checks for a collision at a line starting at the from point and ending at the to point for any entity with the specified tag
        /// </summary>
        /// <param name="fromPoint">The starting point of the collision line</param>
        /// <param name="toPoint">The ending point of the collision line</param>
        /// <param name="tag">The tag of the entities to check collisions for</param>
        public bool CollideCheck(Vector2 fromPoint, Vector2 toPoint, int tag)
        {
            var list = TagLists[tag];
            for (var i = 0; i < list.Count; i++)
                if (list[i].IsCollidable && list[i].CollideLine(fromPoint, toPoint))
                    return true;
            return false;
        }

        /// <summary>
        /// Checks for a collision on the specified rectangle for any entity with the specified tag
        /// </summary>
        /// <param name="rectangle">The rectangle to check collisions on</param>
        /// <param name="tag">The tag of the entities to check collisions for</param>
        /// <returns></returns>
        public bool CollideCheck(Rectangle rectangle, int tag)
        {
            var list = TagLists[tag];
            for (var i = 0; i < list.Count; i++)
                if (list[i].IsCollidable && list[i].CollideRectangle(rectangle))
                    return true;
            return false;
        }

        /// <summary>
        /// Return the first entity that has a collision at the specified point
        /// </summary>
        /// <param name="point">The point to check the collision at</param>
        /// <param name="tag">The tag of the entities to check collisions for</param>
        public Entity CollideGetFirst(Vector2 point, int tag)
        {
            var list = TagLists[tag];
            for (var i = 0; i < list.Count; i++)
                if (list[i].IsCollidable && list[i].CollidePoint(point))
                    return list[i];
            return null;
        }

        /// <summary>
        /// Return the first entity that has a collision on the specified line
        /// </summary>
        /// <param name="fromPoint">The starting point of the collision line</param>
        /// <param name="toPoint">The ending point of the collision line</param>
        /// <param name="tag">The tag of the entities to check collisions for</param>
        public Entity CollideGetFirst(Vector2 fromPoint, Vector2 toPoint, int tag)
        {
            var list = TagLists[tag];
            for (var i = 0; i < list.Count; i++)
                if (list[i].IsCollidable && list[i].CollideLine(fromPoint, toPoint))
                    return list[i];
            return null;
        }

        /// <summary>
        /// Return the first entity that has a collision on the specified rectangle
        /// </summary>
        /// <param name="rectangle">The rectangle to check collisions on</param>
        /// <param name="tag">The tag of the entities to check collisions for</param>
        public Entity CollideGetFirst(Rectangle rectangle, int tag)
        {
            var list = TagLists[tag];
            for (var i = 0; i < list.Count; i++)
                if (list[i].IsCollidable && list[i].CollideRectangle(rectangle))
                    return list[i];
            return null;
        }

        /// <summary>
        /// Check for a collision at the specified point with any entity with the specified tag and
        /// add it/them to the list of entities (hits) that have a collision
        /// </summary>
        /// <param name="point">The point to check the collision at</param>
        /// <param name="tag">The tag of the entities to check collisions for</param>
        /// <param name="hits">The list of entities that have a collision</param>
        public void CollideInto(Vector2 point, int tag, List<Entity> hits)
        {
            var list = TagLists[tag];
            for (var i = 0; i < list.Count; i++)
                if (list[i].IsCollidable && list[i].CollidePoint(point))
                    hits.Add(list[i]);
        }

        /// <summary>
        /// Check for a collision on the specified line with any entity with the specified tag and
        /// add it/them to the list of entities (hits) that have a collision
        /// </summary>
        /// <param name="fromPoint">The starting point of the collision line</param>
        /// <param name="toPoint">The ending point of the collision line</param>
        /// <param name="tag">The tag of the entities to check collisions for</param>
        /// <param name="hits">The list of entities that have a collision</param>
        public void CollideInto(Vector2 fromPoint, Vector2 toPoint, int tag, List<Entity> hits)
        {
            var list = TagLists[tag];
            for (var i = 0; i < list.Count; i++)
                if (list[i].IsCollidable && list[i].CollideLine(fromPoint, toPoint))
                    hits.Add(list[i]);
        }

        /// <summary>
        /// Check for a collision on the specified rectangle with any entity with the specified tag and
        /// add it/them to the list of entities (hits) that have a collision
        /// </summary>
        /// <param name="rectangle">The rectangle to check collisions on</param>
        /// <param name="tag">The tag of the entities to check collisions for</param>
        /// <param name="hits">The list of entities that have a collision</param>
        public void CollideInto(Rectangle rectangle, int tag, List<Entity> hits)
        {
            var list = TagLists[tag];
            for (var i = 0; i < list.Count; i++)
                if (list[i].IsCollidable && list[i].CollideRectangle(rectangle))
                    hits.Add(list[i]);
        }

        public List<Entity> CollideGetAll(Vector2 point, int tag)
        {
            var list = new List<Entity>();
            CollideInto(point, tag, list);
            return list;
        }

        public List<Entity> CollideGetAll(Vector2 fromPoint, Vector2 toPoint, int tag)
        {
            var list = new List<Entity>();
            CollideInto(fromPoint, toPoint, tag, list);
            return list;
        }

        public List<Entity> CollideGetAll(Rectangle rectangle, int tag)
        {
            var list = new List<Entity>();
            CollideInto(rectangle, tag, list);
            return list;
        }

        public void CollideDo(Vector2 point, int tag, Action<Entity> action)
        {
            var list = TagLists[tag];
            for (var i = 0; i < list.Count; i++)
                if (list[i].IsCollidable && list[i].CollidePoint(point))
                    action(list[i]);
        }

        public void CollideDo(Vector2 fromPoint, Vector2 toPoint, int tag, Action<Entity> action)
        {
            var list = TagLists[tag];
            for (var i = 0; i < list.Count; i++)
                if (list[i].IsCollidable && list[i].CollideLine(fromPoint, toPoint))
                    action(list[i]);
        }

        public void CollideDo(Rectangle rectangle, int tag, Action<Entity> action)
        {
            var list = TagLists[tag];
            for (var i = 0; i < list.Count; i++)
                if (list[i].IsCollidable && list[i].CollideRectangle(rectangle))
                    action(list[i]);
        }

        public Vector2 LineWalkCheck(Vector2 fromPoint, Vector2 toPoint, int tag, float precision)
        {
            var add = toPoint - fromPoint;
            add.Normalize();
            add *= precision;

            var amount = (int)Math.Floor((fromPoint - toPoint).Length() / precision);
            var previous = fromPoint;
            var at = fromPoint + add;

            for (var i = 0; i <= amount; i++)
            {
                if (CollideCheck(at, tag)) return previous;
                previous = at;
                at += add;
            }
            return toPoint;
        }
        #endregion Collisions via tags

        #region Collisions via tracked list entities
        public bool CollideCheck<T>(Vector2 point) where T : Entity
        {
            var list = Tracker.Entities[typeof(T)];
            for (var i = 0; i < list.Count; i++)
                if (list[i].IsCollidable && list[i].CollidePoint(point))
                    return true;
            return false;
        }

        public bool CollideCheck<T>(Vector2 fromPoint, Vector2 toPoint) where T : Entity
        {
            var list = Tracker.Entities[typeof(T)];
            for (var i = 0; i < list.Count; i++)
                if (list[i].IsCollidable && list[i].CollideLine(fromPoint, toPoint))
                    return true;
            return false;
        }
        
        public bool CollideCheck<T>(Rectangle rectangle) where T : Entity
        {
            var list = Tracker.Entities[typeof(T)];
            for (var i = 0; i < list.Count; i++)
                if (list[i].IsCollidable && list[i].CollideRectangle(rectangle))
                    return true;
            return false;
        }

        public T CollideGetFirst<T>(Vector2 point) where T : Entity
        {
            var list = Tracker.Entities[typeof(T)];
            for (var i = 0; i < list.Count; i++)
                if (list[i].IsCollidable && list[i].CollidePoint(point))
                    return list[i] as T;
            return null;
        }

        public T CollideGetFirst<T>(Vector2 fromPoint, Vector2 toPoint) where T : Entity
        {
            var list = Tracker.Entities[typeof(T)];
            for (var i = 0; i < list.Count; i++)
                if (list[i].IsCollidable && list[i].CollideLine(fromPoint, toPoint))
                    return list[i] as T;
            return null;
        }

        public T CollideGetFirst<T>(Rectangle rectangle) where T : Entity
        {
            var list = Tracker.Entities[typeof(T)];
            for (var i = 0; i < list.Count; i++)
                if (list[i].IsCollidable && list[i].CollideRectangle(rectangle))
                    return list[i] as T;
            return null;
        }

        public void CollideInto<T>(Vector2 point, List<Entity> hits) where T : Entity
        {
            var list = Tracker.Entities[typeof(T)];
            for (var i = 0; i < list.Count; i++)
                if (list[i].IsCollidable && list[i].CollidePoint(point))
                    hits.Add(list[i]);
        }

        public void CollideInto<T>(Vector2 fromPoint, Vector2 toPoint, List<Entity> hits) where T : Entity
        {
            var list = Tracker.Entities[typeof(T)];
            for (var i = 0; i < list.Count; i++)
                if (list[i].IsCollidable && list[i].CollideLine(fromPoint, toPoint))
                    hits.Add(list[i]);
        }

        public void CollideInto<T>(Rectangle rectangle, List<Entity> hits) where T : Entity
        {
            var list = Tracker.Entities[typeof(T)];
            for (var i = 0; i < list.Count; i++)
                if (list[i].IsCollidable && list[i].CollideRectangle(rectangle))
                    hits.Add(list[i]);
        }

        public void CollideInto<T>(Vector2 point, List<T> hits) where T : Entity
        {
            var list = Tracker.Entities[typeof(T)];
            for (var i = 0; i < list.Count; i++)
                if (list[i].IsCollidable && list[i].CollidePoint(point))
                    hits.Add(list[i] as T);
        }

        public void CollideInto<T>(Vector2 fromPoint, Vector2 toPoint, List<T> hits) where T : Entity
        {
            var list = Tracker.Entities[typeof(T)];
            for (var i = 0; i < list.Count; i++)
                if (list[i].IsCollidable && list[i].CollideLine(fromPoint, toPoint))
                    hits.Add(list[i] as T);
        }

        public void CollideInto<T>(Rectangle rectangle, List<T> hits) where T : Entity
        {
            var list = Tracker.Entities[typeof(T)];
            for (var i = 0; i < list.Count; i++)
                if (list[i].IsCollidable && list[i].CollideRectangle(rectangle))
                    hits.Add(list[i] as T);
        }

        public List<T> CollideGetAll<T>(Vector2 point) where T : Entity
        {
            var list = new List<T>();
            CollideInto(point, list);
            return list;
        }

        public List<T> CollideGetAll<T>(Vector2 fromPoint, Vector2 toPoint) where T : Entity
        {
            var list = new List<T>();
            CollideInto(fromPoint, toPoint, list);
            return list;
        }

        public List<T> CollideGetAll<T>(Rectangle rectangle) where T : Entity
        {
            var list = new List<T>();
            CollideInto(rectangle, list);
            return list;
        }

        public void CollideDo<T>(Vector2 point, Action<T> action) where T : Entity
        {
            var list = Tracker.Entities[typeof(T)];
            for (var i = 0; i < list.Count; i++)
                if (list[i].IsCollidable && list[i].CollidePoint(point))
                    action(list[i] as T);
        }

        public void CollideDo<T>(Vector2 fromPoint, Vector2 toPoint, Action<T> action) where T : Entity
        {
            var list = Tracker.Entities[typeof(T)];
            for (var i = 0; i < list.Count; i++)
                if (list[i].IsCollidable && list[i].CollideLine(fromPoint, toPoint))
                    action(list[i] as T);
        }

        public void CollideDo<T>(Rectangle rectangle, Action<T> action) where T : Entity
        {
            var list = Tracker.Entities[typeof(T)];
            for (var i = 0; i < list.Count; i++)
                if (list[i].IsCollidable && list[i].CollideRectangle(rectangle))
                    action(list[i] as T);
        }

        public Vector2 LineWalkCheck<T>(Vector2 fromPoint, Vector2 toPoint, float precision) where T : Entity
        {
            var add = toPoint - fromPoint;
            add.Normalize();
            add *= precision;

            var amount = (int)Math.Floor((fromPoint - toPoint).Length() / precision);
            var previous = fromPoint;
            var at = fromPoint + add;

            for (var i = 0; i <= amount; i++)
            {
                if (CollideCheck<T>(at)) return previous;
                previous = at;
                at += add;
            }
            return toPoint;
        }
        #endregion Collisions via tracked list entities

        #region Collisions via tracked list components
        public bool CollideCheckByComponent<T>(Vector2 point) where T : Component
        {
            var list = Tracker.Components[typeof(T)];
            for (var i = 0; i < list.Count; i++)
                if (list[i].Entity.IsCollidable && list[i].Entity.CollidePoint(point))
                    return true;
            return false;
        }

        public bool CollideCheckByComponent<T>(Vector2 fromPoint, Vector2 toPoint) where T : Component
        {
            var list = Tracker.Components[typeof(T)];
            for (var i = 0; i < list.Count; i++)
                if (list[i].Entity.IsCollidable && list[i].Entity.CollideLine(fromPoint, toPoint))
                    return true;
            return false;
        }

        public bool CollideCheckByComponent<T>(Rectangle rectangle) where T : Component
        {
            var list = Tracker.Components[typeof(T)];
            for (var i = 0; i < list.Count; i++)
                if (list[i].Entity.IsCollidable && list[i].Entity.CollideRectangle(rectangle))
                    return true;
            return false;
        }

        public T CollideGetFirstByComponent<T>(Vector2 point) where T : Component
        {
            var list = Tracker.Components[typeof(T)];
            for (var i = 0; i < list.Count; i++)
                if (list[i].Entity.IsCollidable && list[i].Entity.CollidePoint(point))
                    return list[i] as T;
            return null;
        }

        public T CollideGetFirstByComponent<T>(Vector2 fromPoint, Vector2 toPoint) where T : Component
        {
            var list = Tracker.Components[typeof(T)];
            for (var i = 0; i < list.Count; i++)
                if (list[i].Entity.IsCollidable && list[i].Entity.CollideLine(fromPoint, toPoint))
                    return list[i] as T;
            return null;
        }

        public T CollideGetFirstByComponent<T>(Rectangle rectangle) where T : Component
        {
            var list = Tracker.Components[typeof(T)];
            for (var i = 0; i < list.Count; i++)
                if (list[i].Entity.IsCollidable && list[i].Entity.CollideRectangle(rectangle))
                    return list[i] as T;
            return null;
        }

        public void CollideIntoByComponent<T>(Vector2 point, List<Component> hits) where T : Component
        {
            var list = Tracker.Components[typeof(T)];
            for (var i = 0; i < list.Count; i++)
                if (list[i].Entity.IsCollidable && list[i].Entity.CollidePoint(point))
                    hits.Add(list[i]);
        }

        public void CollideIntoByComponent<T>(Vector2 fromPoint, Vector2 toPoint, List<Component> hits) where T : Component
        {
            var list = Tracker.Components[typeof(T)];
            for (var i = 0; i < list.Count; i++)
                if (list[i].Entity.IsCollidable && list[i].Entity.CollideLine(fromPoint, toPoint))
                    hits.Add(list[i]);
        }

        public void CollideIntoByComponent<T>(Rectangle rectangle, List<Component> hits) where T : Component
        {
            var list = Tracker.Components[typeof(T)];
            for (var i = 0; i < list.Count; i++)
                if (list[i].Entity.IsCollidable && list[i].Entity.CollideRectangle(rectangle))
                    hits.Add(list[i]);
        }

        public void CollideIntoByComponent<T>(Vector2 point, List<T> hits) where T : Component
        {
            var list = Tracker.Components[typeof(T)];
            for (var i = 0; i < list.Count; i++)
                if (list[i].Entity.IsCollidable && list[i].Entity.CollidePoint(point))
                    hits.Add(list[i] as T);
        }

        public void CollideIntoByComponent<T>(Vector2 fromPoint, Vector2 toPoint, List<T> hits) where T : Component
        {
            var list = Tracker.Components[typeof(T)];
            for (var i = 0; i < list.Count; i++)
                if (list[i].Entity.IsCollidable && list[i].Entity.CollideLine(fromPoint, toPoint))
                    hits.Add(list[i] as T);
        }

        public void CollideIntoByComponent<T>(Rectangle rectangle, List<T> hits) where T : Component
        {
            var list = Tracker.Components[typeof(T)];
            for (var i = 0; i < list.Count; i++)
                if (list[i].Entity.IsCollidable && list[i].Entity.CollideRectangle(rectangle))
                    hits.Add(list[i] as T);
        }

        public List<T> CollideGetAllByComponent<T>(Vector2 point) where T : Component
        {
            var list = new List<T>();
            CollideIntoByComponent<T>(point, list);
            return list;
        }

        public List<T> CollideGetAllByComponent<T>(Vector2 fromPoint, Vector2 toPoint) where T : Component
        {
            var list = new List<T>();
            CollideIntoByComponent<T>(fromPoint, toPoint, list);
            return list;
        }

        public List<T> CollideGetAllByComponent<T>(Rectangle rectangle) where T : Component
        {
            var list = new List<T>();
            CollideIntoByComponent<T>(rectangle, list);
            return list;
        }

        public void CollideDoByComponent<T>(Vector2 point, Action<T> action) where T : Component
        {
            var list = Tracker.Components[typeof(T)];
            for (var i = 0; i < list.Count; i++)
                if (list[i].Entity.IsCollidable && list[i].Entity.CollidePoint(point))
                    action(list[i] as T);
        }

        public void CollideDoByComponent<T>(Vector2 fromPoint, Vector2 toPoint, Action<T> action) where T : Component
        {
            var list = Tracker.Components[typeof(T)];
            for (var i = 0; i < list.Count; i++)
                if (list[i].Entity.IsCollidable && list[i].Entity.CollideLine(fromPoint, toPoint))
                    action(list[i] as T);
        }

        public void CollideDoByComponent<T>(Rectangle rectangle, Action<T> action) where T : Component
        {
            var list = Tracker.Components[typeof(T)];
            for (var i = 0; i < list.Count; i++)
                if (list[i].Entity.IsCollidable && list[i].Entity.CollideRectangle(rectangle))
                    action(list[i] as T);
        }

        public Vector2 LineWalkCheckByCompont<T>(Vector2 fromPoint, Vector2 toPoint, float precision) where T : Component
        {
            var add = toPoint - fromPoint;
            add.Normalize();
            add *= precision;

            var amount = (int)Math.Floor((fromPoint - toPoint).Length() / precision);
            var previous = fromPoint;
            var at = fromPoint + add;

            for (var i = 0; i < amount; i++)
            {
                if (CollideCheckByComponent<T>(at)) return previous;
                previous = at;
                at += add;
            }
            return toPoint;
        }
        #endregion Collisions via tracked list components

        #region Utility
        internal void SetActualDepth(Entity entity)
        {
            const double THETA = 0.000001f;

            double add;
            if (actualDepthLookup.TryGetValue(entity.depth, out add))
                actualDepthLookup[entity.depth] += THETA;
            else
                actualDepthLookup.Add(entity.depth, THETA);
            entity.actualDepth = entity.depth - add;

            Entities.MarkUnsorted();
            for (var i = 0; i < BitTag.TotalTags; i++)
                if (entity.TagCheck(1 << i))
                    TagLists.MarkUnsorted(i);
        }
        #endregion Utility

        #region Entity shortcuts
        /// <summary>
        /// Shortcut to call Engine.Pooler.Create, add the entity to this scene, and return it. Entity type must be marked as pooled
        /// </summary>
        /// <typeparam name="T">Pooled entity type to create</typeparam>
        public T CreateAndAdd<T>() where T : Entity, new()
        {
            var entity = Engine.Pooler.Create<T>();
            Add(entity);
            return entity;
        }

        /// <summary>
        /// Quick access to entire tag lists of Entities. Result will never be null
        /// </summary>
        /// <param name="tag">The tag list to fetch</param>
        public List<Entity> this[BitTag tag]
        {
            get { return TagLists[tag.ID]; }
        }

        /// <summary>
        /// Shortcut function for adding an Entity to the Scene's Entities list
        /// </summary>
        /// <param name="entity">The Entity to add</param>
        public void Add(Entity entity)
        {
            Entities.Add(entity);
        }

        /// <summary>
        /// Shortcut function for removing an Entity from the Scene's Entities list
        /// </summary>
        /// <param name="entity">The Entity to remove</param>
        public void Remove(Entity entity)
        {
            Entities.Remove(entity);
        }

        /// <summary>
        /// Shortcut function for adding a set of Entities from the Scene's Entities list
        /// </summary>
        /// <param name="entities">The Entities to add</param>
        public void Add(IEnumerable<Entity> entities)
        {
            Entities.Add(entities);
        }

        /// <summary>
        /// Shortcut function for removing a set of Entities from the Scene's Entities list
        /// </summary>
        /// <param name="entities">The Entities to remove</param>
        public void Remove(IEnumerable<Entity> entities)
        {
            Entities.Remove(entities);
        }

        /// <summary>
        /// Shortcut function for adding a set of Entities from the Scene's Entities list
        /// </summary>
        /// <param name="entities">The Entities to add</param>
        public void Add(params Entity[] entities)
        {
            Entities.Add(entities);
        }

        /// <summary>
        /// Shortcut function for removing a set of Entities from the Scene's Entities list
        /// </summary>
        /// <param name="entities">The Entities to remove</param>
        public void Remove(params Entity[] entities)
        {
            Entities.Remove(entities);
        }

        /// <summary>
        /// Allows you to iterate through all Entities in the Scene
        /// </summary>
        public IEnumerator<Entity> GetEnumerator()
        {
            return Entities.GetEnumerator();
        }

        /// <summary>
        /// Allows you to iterate through all Entities in the Scene
        /// </summary>
        IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public List<Entity> GetEntitiesByTagMask(int mask)
        {
            var list = new List<Entity>();
            foreach (var entity in Entities)
                if ((entity.Tag & mask) != 0)
                    list.Add(entity);
            return list;
        }

        public List<Entity> GetEntitiesExcludingTagMask(int mask)
        {
            var list = new List<Entity>();
            foreach (var entity in Entities)
                if ((entity.Tag & mask) == 0)
                    list.Add(entity);
            return list;
        }
        #endregion Entity shortcuts

        #region Renderer shortcuts
        /// <summary>
        /// Shortcut function to add a Renderer to the Renderer list
        /// </summary>
        /// <param name="renderer">The Renderer to add</param>
        public void Add(Renderer renderer)
        {
            Renderers.Add(renderer);
        }

        /// <summary>
        /// Shortcut function to remove a Renderer from the Renderer list
        /// </summary>
        /// <param name="renderer">The Renderer to remove</param>
        public void Remove(Renderer renderer)
        {
            Renderers.Remove(renderer);
        }
        #endregion Renderer shortcuts
    }
}
