﻿using GemEngine.ECS;
using GemEngine.Input;
using GemEngine.Utility;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.IO;
using System.Reflection;
using System.Runtime;

namespace GemEngine
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Engine : Game
    {
        public string Title;
        public Version Version;

        public static Engine Instance { get; private set; }
        public static GraphicsDeviceManager Graphics { get; private set; }
        public static DebugConsole DebugConsole { get; private set; }
        public static Pooler Pooler { get; private set; }
        public static Action OverloadGameLoop;

        #region Screen Size
        public static int Width { get; private set; }
        public static int Height { get; private set; }
        public static int ViewWidth { get; private set; }
        public static int ViewHeight { get; private set; }
        public static int ViewPadding
        {
            get { return viewPadding; }
            set
            {
                viewPadding = value;
                Instance.UpdateView();
            }
        }
        private static int viewPadding;
        #endregion Screen Size

        #region Timing
        public static float DeltaTime { get; private set; }
        public static float RawDeltaTime { get; private set; }
        public static float TimeRate = 1f;
        public static float FreezeTimer;
        public static int FPS;
        private TimeSpan counterElapsed = TimeSpan.Zero;
        private int fpsCounter = 0;
        #endregion Timing

        #region Content Directory
#if !CONSOLE
        private static string assemblyDirectory = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
#endif
        public static string ContentDirectory
        {
#if PS4
            get { return Path.Combine("/app0/", Instance.Content.RootDirectory; }
#elif NSWITCH
            get { return Path.Combine("rom:/", Instance.Content.RootDirectory; }
#elif XBOXONE
            get { return Instance.Content.RootDirectory; }
#else
            get { return Path.Combine(assemblyDirectory, Instance.Content.RootDirectory); }
#endif
        }
        #endregion Content Directory

        #region Utility
        public static Color ClearColor;
        public static bool ShouldExitOnEscapeKeypress;
        #endregion Utility

        #region Scene
        private Scene scene;
        private Scene nextScene;
        #endregion Scene

        private static bool isResizing;

        /// <summary>
        /// Constructor for the engine. Your Program.cs file (or whatever class holds Main()) should use this to run the game.
        /// </summary>
        /// <param name="width">The base width of the game</param>
        /// <param name="height">The base height of the game</param>
        /// <param name="windowWidth">The width of the game window</param>
        /// <param name="windowHeight">The height of the game window</param>
        /// <param name="windowTitle">The title displayed on the window, as well as the title of the game</param>
        /// <param name="shouldStartFullscreen">Whether or not to start the game in fullscreen (this option is only used on non-console versions of the game)</param>
        public Engine(int width, int height, int windowWidth, int windowHeight, string windowTitle, bool shouldStartFullscreen)
        {
            Instance = this;
            
            Title = Window.Title = windowTitle;
            Width = width;
            Height = height;
            ClearColor = Color.Black;

            #region Setup graphics
            Graphics = new GraphicsDeviceManager(this);
            Graphics.DeviceReset += OnGraphicsReset;
            Graphics.DeviceCreated += OnGraphicsCreate;
            Graphics.SynchronizeWithVerticalRetrace = true;
            Graphics.PreferMultiSampling = false;
            Graphics.GraphicsProfile = GraphicsProfile.HiDef;
            Graphics.PreferredBackBufferFormat = SurfaceFormat.Color;
            Graphics.PreferredDepthStencilFormat = DepthFormat.Depth24Stencil8;
            Graphics.ApplyChanges();
            #endregion Setup graphics

#if PS4 || XBOXONE
            Graphics.PreferredBackBufferWidth = 1920;
            Graphics.PreferredBackBufferHeight = 1080;
#else
            Window.AllowUserResizing = true;
            Window.ClientSizeChanged += OnClientSizeChanged;

            if (shouldStartFullscreen)
            {
                Graphics.PreferredBackBufferWidth = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
                Graphics.PreferredBackBufferHeight = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;
                Graphics.IsFullScreen = true;
            }
            else
            {
                Graphics.PreferredBackBufferWidth = windowWidth;
                Graphics.PreferredBackBufferHeight = windowHeight;
                Graphics.IsFullScreen = false;
            }
#endif

            Content.RootDirectory = @"Content";

            IsMouseVisible = false;
            IsFixedTimeStep = false;
            ShouldExitOnEscapeKeypress = true;

            GCSettings.LatencyMode = GCLatencyMode.SustainedLowLatency;
        }

#if !CONSOLE
        protected virtual void OnClientSizeChanged(object sender, EventArgs args)
        {
            if (Window.ClientBounds.Width <= 0 || Window.ClientBounds.Height <= 0 || isResizing)
                return;

            isResizing = true;
            Graphics.PreferredBackBufferWidth = Window.ClientBounds.Width;
            Graphics.PreferredBackBufferHeight = Window.ClientBounds.Height;
            isResizing = false;
        }
#endif

        protected virtual void OnGraphicsReset(object sender, EventArgs args)
        {
            UpdateView();
            scene?.HandleGraphicsReset();
            if (nextScene != scene)
                nextScene?.HandleGraphicsReset();
        }

        protected virtual void OnGraphicsCreate(object sender, EventArgs e)
        {
            UpdateView();
            scene?.HandleGraphicsCreate();
            if (nextScene != scene)
                nextScene?.HandleGraphicsCreate();
        }

        protected override void OnActivated(object sender, EventArgs args)
        {
            base.OnActivated(sender, args);
            scene?.GainFocus();
        }

        protected override void OnDeactivated(object sender, EventArgs args)
        {
            base.OnDeactivated(sender, args);
            scene?.LoseFocus();
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            base.Initialize();

            InputManager.Initialize();
            Tracker.Initialize();
            Pooler = new Pooler();
            DebugConsole = new DebugConsole();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            base.LoadContent();
            GemEngine.Graphics.Draw.Initialize(GraphicsDevice);
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            RawDeltaTime = (float)gameTime.ElapsedGameTime.TotalSeconds;
            DeltaTime = RawDeltaTime * TimeRate;

            InputManager.Update();

#if !CONSOLE
            if (ShouldExitOnEscapeKeypress && InputManager.Keyboard.IsPressed(Keys.Escape))
            {
                Exit();
                return;
            }
#endif
            if (OverloadGameLoop != null)
            {
                OverloadGameLoop();
                base.Update(gameTime);
                return;
            }

            //Update current scene
            if (FreezeTimer > 0)
                FreezeTimer = Math.Max(FreezeTimer - RawDeltaTime, 0);
            else
            {
                scene?.BeforeUpdate();
                scene?.Update();
                scene?.AfterUpdate();
            }

            //Debug console
            if (DebugConsole.IsOpen)
                DebugConsole.UpdateOpen();
            else if (DebugConsole.IsEnabled)
                DebugConsole.UpdateClosed();

            //Changing scenes
            if (scene != nextScene)
            {
                var lastScene = scene;
                scene?.End();
                scene = nextScene;
                OnSceneTransition();
                scene?.Begin();
            }

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            RenderCore();

            base.Draw(gameTime);
            if (DebugConsole.IsOpen)
                DebugConsole.Render();

            //Frame counter
            fpsCounter++;
            counterElapsed += gameTime.ElapsedGameTime;
            if (counterElapsed >= TimeSpan.FromSeconds(1))
            {
#if DEBUG
                Window.Title = $"{Title} {fpsCounter.ToString()} fps - {(GC.GetTotalMemory(false) / 1048576f).ToString("F")} MB";
#endif
                FPS = fpsCounter;
                fpsCounter = 0;
                counterElapsed -= TimeSpan.FromSeconds(1);
            }
        }

        /// <summary>
        /// Override if you want to change the core rendering functionality of GemEngine.
        /// By default this simply sets the render target to null, clears the scene, and renders the current scene.
        /// </summary>
        protected virtual void RenderCore()
        {
            //TODO: This is where we might include the Nez pixel perfect render
            scene?.BeforeRender();

            GraphicsDevice.SetRenderTarget(null);
            GraphicsDevice.Viewport = Viewport;
            GraphicsDevice.Clear(ClearColor);

            scene?.Render();
            scene?.AfterRender();
        }

        protected override void OnExiting(object sender, EventArgs args)
        {
            base.OnExiting(sender, args);
            InputManager.Shutdown();
        }

        public void RunWithLogging()
        {
            try { Run(); }
            catch(Exception e)
            {
                ErrorLog.Write(e);
                ErrorLog.Open();
            }
        }

        #region Scene
        /// <summary>
        /// Called after a scene ends and before the next scene begins
        /// </summary>
        protected virtual void OnSceneTransition()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();

            TimeRate = 1f;
        }

        public static Scene Scene
        {
            get { return Instance.scene; }
            set { Instance.nextScene = value; }
        }
        #endregion Scene

        #region Screen
        public static Viewport Viewport { get; private set; }
        public static Matrix ScreenMatrix { get; set; }

        public static void SetWindowed(int width, int height)
        {
#if !CONSOLE
            if (width <= 0 || height <= 0)
                return;
            isResizing = true;
            Graphics.PreferredBackBufferWidth = width;
            Graphics.PreferredBackBufferHeight = height;
            Graphics.IsFullScreen = false;
            Graphics.ApplyChanges();
            Console.WriteLine($"WINDOW-{width}x{height}");
            isResizing = false;
#endif
        }

        public static void SetFullscreen()
        {
#if !CONSOLE
            isResizing = true;
            Graphics.PreferredBackBufferWidth = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
            Graphics.PreferredBackBufferHeight = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;
            Graphics.IsFullScreen = true;
            Graphics.ApplyChanges();
            Console.WriteLine("FULLSCREEN");
            isResizing = false;
#endif
        }

        private void UpdateView()
        {
            var screenWidth = (float)GraphicsDevice.PresentationParameters.BackBufferWidth;
            var screenHeight = (float)GraphicsDevice.PresentationParameters.BackBufferHeight;

            //get view size
            if (screenWidth / Width > screenHeight / Height)
            {
                ViewWidth = (int)(screenHeight / Height * Width);
                ViewHeight = (int)screenHeight;
            }
            else
            {
                ViewWidth = (int)screenWidth;
                ViewHeight = (int)(screenWidth / Width * Height);
            }

            //apply view padding
            var aspect = ViewHeight / (float)ViewWidth;
            ViewWidth -= ViewPadding * 2;
            ViewHeight -= (int)(aspect * ViewPadding * 2);

            //update screen matrix
            ScreenMatrix = Matrix.CreateScale(ViewWidth / (float)Width);

            //update viewport
            Viewport = new Viewport
            {
                X = (int)(screenWidth / 2 - ViewWidth / 2),
                Y = (int)(screenHeight / 2 - ViewHeight / 2),
                Width = ViewWidth,
                Height = ViewHeight,
                MinDepth = 0,
                MaxDepth = 0
            };
        }
        #endregion Screen
    }
}
